const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;

const {
  PROJECT_ID,
} = require('./../config');

module.exports = (req, res, next) => {
  const { params: { secret } } = req;
  const { filename } = res.locals;

  if (secret) {
    if (filename) {
      req.url = `${PROJECT_ID}/${secret}_${filename}`;
    } else {
      req.url = `${PROJECT_ID}/${secret}`;
    }
  }
  next();
};
