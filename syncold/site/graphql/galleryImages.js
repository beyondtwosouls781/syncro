const graphQlFetch = require('./graphql').graphQlFetch;

const templateQuery = (req) => {
  return graphQlFetch({ req, query: 'allImagesOfMainGalleryQuery' })
    .then(({ data, errors, extensions }) => {
      return {
        data: {
          mainGallery: data ? data.allImages[0] : [],
        },
        errors,
        extensions,
      };
    });
};

module.exports = templateQuery;
