const { graphQlFetch } = require('./graphql');

const accountQuery = (req) => {
  return graphQlFetch({ req, query: 'loggedInUser' })
    .then(({ data, errors, extensions }) => {
      return {
        data,
        errors,
        extensions,
      };
    });
};

module.exports = accountQuery;
