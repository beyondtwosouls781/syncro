const winston = require('winston');
const logsPath = `${__dirname}/../logs`;

let level = 'info';
let format = winston.format.simple();

if (process.env.NODE_ENV === 'development') {
  level = 'debug';
  format = winston.format.json();
}

const logger = winston.createLogger({
  level,
  format,
  transports: [
    new winston.transports.File({
      filename: `${logsPath}/combined.log`,
      level: 'info',
      timestamp: true,
    }),
    new winston.transports.File({
      filename: `${logsPath}/errors.log`,
      level: 'error',
      timestamp: true,
    }),
  ],
  exceptionHandlers: [
    new winston.transports.File({
      filename: `${logsPath}/exceptions.log`,
    }),
  ],
});

module.exports = logger;
