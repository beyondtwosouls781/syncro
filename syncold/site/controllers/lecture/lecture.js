const redis = require('./../../db/redis');
const { graphQlFetch } = require('./../../graphql');
const { ROOT_TOKEN } = require('./../../config');

const {
  productParams,
  additionalDataOfEvents,
} = require('./../course/functions');

const checkLectureAvailableForBuy = ({ events, course, cycle }) => {
  if (course && course.length > 0) {
    if (course[0].buyFull) return false;
  }
  if (cycle && cycle.length > 0) {
    if (cycle[0].buyFull) return false;
  }
  return Boolean(events.length);
};

const lectureByIdQuery = (req, lecture) => {
  const { id, tags } = lecture;
  return graphQlFetch({
    req: Object.assign({}, req, { user: { token: ROOT_TOKEN } }),
    query: 'lectureByIdQuery',
    variables: {
      id,
      lecturesIds: [id],
      tagsIds: tags ? tags.map(tag => tag.id) : [],
      date: new Date(),
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      const { Lecture, allEvents, allLectures } = data;
      const { recommendedLectures } = Lecture;
      const additionalDataForLecture = additionalDataOfEvents(allEvents);

      const LectureWithAdditionalData = Object.assign(
        {},
        Lecture,
        {
          discounts: data.allDiscounts.slice(),
        },
        additionalDataForLecture,
      );
      const dataWithAdditionalData = Object.assign(
        {},
        data,
        {
          Lecture: Object.assign(
            LectureWithAdditionalData,
            {
              isAvailableForBuy: checkLectureAvailableForBuy(LectureWithAdditionalData),
              product: productParams(LectureWithAdditionalData),
            },
          ),
          recomendation: recommendedLectures.concat(allLectures).slice(0, 3),
        },
      );
      const currentPageData = Object.assign(
        {},
        dataWithAdditionalData,
      );
      return currentPageData;
      // return null;
    });
};

module.exports = (req, lecture) => {
  return Promise.all([
    redis.batch([
      ['get', 'template'],
      ['get', 'howWork:lecture'],
      ['get', 'gallery:maingallery'],
    ]).execAsync(),
    lectureByIdQuery(req, lecture),
  ])
    .then((result) => {
      const [[template, howWork, gallery], data] = result;
      return Object.assign(
        {},
        data,
        {
          HowWorkPage: JSON.parse(howWork),
          mainGallery: JSON.parse(gallery),
          templateData: JSON.parse(template),
        },
        { ___typename: 'Lecture' },
      );
    });
};
