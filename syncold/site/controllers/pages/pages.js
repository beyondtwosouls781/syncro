const { graphQlFetch } = require('./../../graphql');

const simplePageQuery = (req) => {
  const { page } = req.params;
  return graphQlFetch({
    req,
    query: 'simpleContentQuery',
    variables: {
      type: page,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.SimpleContent;
    });
};

exports.agreement = (req) => {
  return simplePageQuery(req);
};

exports.termOfUse = (req) => {
  return simplePageQuery(req);
};
