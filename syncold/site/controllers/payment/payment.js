const crypto = require('crypto');

const redis = require('./../../db/redis');
const { graphQlFetch } = require('./../../graphql');
const acquiringFetch = require('./../../acquiring');
const { ROOT_TOKEN } = require('./../../config');

const updatePaymentWithoutAcquiring = (id) => {
  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'updatePaymentStatusMutation',
    variables: {
      id,
      status: 'PAID',
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return { PaymentURL: '/account', ErrorCode: '0' };
    });
};

const checkAcquiringToken = (id, body) => {
  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'acquiringOfPaymentIdQuery',
    variables: {
      id,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      const obj = Object.assign({}, body, { Password: data.Payment.acquiring.password });
      const token = obj.Token;
      delete obj.Token;

      const value = Object.keys(obj)
        .sort()
        .map(key => obj[key])
        .join('');
      const sha256 = crypto.createHash('sha256');
      const hash = sha256.update(value).digest('hex');

      return hash === token;
    });
};

const checkOrderEventsQuery = (order) => {
  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'checkOrderEventsQuery',
    variables: {
      order,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      const { checkOrderEvents } = data;
      const resultOfChecking = JSON.parse(checkOrderEvents.resultOfChecking);
      const resultOfCalcOrder = JSON.parse(checkOrderEvents.resultOfCalcOrder);

      const {
        EntityNoPublic,
        allEventsNoTicketsAvailable,
        PromocodeWithErrors,
        PromocodesWithErrors,
        DiscountsWithErrors,
      } = resultOfChecking;

      const output = {
        EntityNoPublic: EntityNoPublic ? {
          id: EntityNoPublic.id,
          title: EntityNoPublic.title,
        } : null,
        allEventsNoTicketsAvailable: allEventsNoTicketsAvailable ? allEventsNoTicketsAvailable
          .map(({ id, date, lecture: { title } }) => ({ id, date, title })) : null,
        PromocodeWithErrors: PromocodeWithErrors ? {
          id: PromocodeWithErrors.id,
          title: PromocodeWithErrors.title,
        } : null,
        PromocodesWithErrors: PromocodesWithErrors ? PromocodesWithErrors
          .map(({ id, title }) => ({ id, title })) : null,
        DiscountsWithErrors: DiscountsWithErrors ? DiscountsWithErrors
          .map(({ id, title }) => ({ id, title })) : null,
      };

      if (
        EntityNoPublic
        || allEventsNoTicketsAvailable
        || PromocodeWithErrors
        || PromocodesWithErrors
        || DiscountsWithErrors
      ) {
        return {
          resultOfChecking: output,
        };
      }

      const {
        order: {
          orderPrice,
          discountsPrice,
          discountsRate,
          totalPrice,
        },
      } = order;

      if (
        orderPrice !== resultOfCalcOrder.orderPrice
        // || discountsPrice !== resultOfCalcOrder.discountsPrice
        // || discountsRate !== resultOfCalcOrder.discountsRate
        || totalPrice !== resultOfCalcOrder.totalPrice
      ) {
        return {
          resultOfCalcOrder,
        };
      }

      return {
        resultOfChecking: null,
        resultOfCalcOrder: null,
      };
    });
};

const checkOrderCertAndSubQuery = (order) => {
  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'checkOrderCertAndSubQuery',
    variables: {
      order: JSON.stringify(order),
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      const { checkOrderCertAndSub } = data;
      const resultOfChecking = JSON.parse(checkOrderCertAndSub.resultOfChecking);
      const resultOfCalcOrder = JSON.parse(checkOrderCertAndSub.resultOfCalcOrder);

      const {
        EntityNoPublic,
        PromocodeWithErrors,
        PromocodesWithErrors,
        DiscountsWithErrors,
      } = resultOfChecking;

      const output = {
        EntityNoPublic: EntityNoPublic ? {
          id: EntityNoPublic.id,
          title: EntityNoPublic.title,
        } : null,
        PromocodeWithErrors: PromocodeWithErrors ? {
          id: PromocodeWithErrors.id,
          title: PromocodeWithErrors.title,
        } : null,
        PromocodesWithErrors: PromocodesWithErrors ? PromocodesWithErrors
          .map(({ id, title }) => ({ id, title })) : null,
        DiscountsWithErrors: DiscountsWithErrors ? DiscountsWithErrors
          .map(({ id, title }) => ({ id, title })) : null,
      };

      if (
        EntityNoPublic
        || PromocodeWithErrors
        || PromocodesWithErrors
        || DiscountsWithErrors
      ) {
        return {
          resultOfChecking: output,
        };
      }

      const {
        order: {
          orderPrice,
          discountsPrice,
          discountsRate,
          totalPrice,
        },
      } = order;

      if (
        orderPrice !== resultOfCalcOrder.orderPrice
        // || discountsPrice !== resultOfCalcOrder.discountsPrice
        // || discountsRate !== resultOfCalcOrder.discountsRate
        || totalPrice !== resultOfCalcOrder.totalPrice
      ) {
        return {
          resultOfCalcOrder,
        };
      }

      return {
        resultOfChecking: null,
        resultOfCalcOrder: null,
      };
    });
};

const checkOrderEvents = (data) => {
  return checkOrderEventsQuery(data)
    .then((result) => {
      if (result.resultOfChecking || result.resultOfCalcOrder) {
        let error = null;
        if (result.resultOfChecking) {
          error = new Error('resultOfChecking');
        }
        if (result.resultOfCalcOrder) {
          error = new Error('resultOfCalcOrder');
        }
        error.checkOrderEventsQuery = result;
        return Promise.reject(error);
      }
      return Promise.resolve();
    });
};

const checkCertAndSubs = (data) => {
  return checkOrderCertAndSubQuery(data)
    .then((result) => {
      if (result.resultOfChecking || result.resultOfCalcOrder) {
        let error = null;
        if (result.resultOfChecking) {
          error = new Error('resultOfChecking');
        }
        if (result.resultOfCalcOrder) {
          error = new Error('resultOfCalcOrder');
        }
        error.checkOrderCertAndSubQuery = result;
        return Promise.reject(error);
      }
      return Promise.resolve();
    });
};

const orderPayment = (data) => {
  return checkOrderEvents(data)
    .then(() => {
      if (data.order.totalPrice === 0) return updatePaymentWithoutAcquiring(data.paymentId);
      return acquiringFetch.init(data)
    })
    .then(({ PaymentURL, ErrorCode }) => {
      if (ErrorCode !== '0') return Promise.reject(new Error(`Acquiring ${ErrorCode}`))
      return PaymentURL;
    });
};

const certPayment = (data) => {
  return checkCertAndSubs(data)
    .then(() => {
      if (data.order.totalPrice === 0) return updatePaymentWithoutAcquiring(data.paymentId);
      return acquiringFetch.init(data)
    })
    .then(({ PaymentURL, ErrorCode }) => {
      if (ErrorCode !== '0') return Promise.reject(new Error(`Acquiring ${ErrorCode}`))
      return PaymentURL;
    });
};


const subPayment = (data) => {
  return checkCertAndSubs(data)
    .then(() => {
      if (data.order.totalPrice === 0) return updatePaymentWithoutAcquiring(data.paymentId);
      return acquiringFetch.init(data)
    })
    .then(({ PaymentURL, ErrorCode }) => {
      if (ErrorCode !== '0') return Promise.reject(new Error(`Acquiring ${ErrorCode}`))
      return PaymentURL;
    });
};

module.exports = (req) => {

};

module.exports.checkAcquiringToken = checkAcquiringToken;

module.exports.statusCallback = (req) => {
  const {
    Success,
    Status,
    OrderId,
    DATA,
    Token,
  } = req.body;

  let payment = null;

  if (!(Status === 'AUTHORIZED' || Status === 'CONFIRMED')) return undefined;

  return checkAcquiringToken(OrderId, req.body)
    .then((check) => {
      if (!check) return Promise.reject(new Error('Hash fail'));

      return graphQlFetch({
        req: {
          user: { token: ROOT_TOKEN },
        },
        query: 'paymentSuccsesMutation',
        variables: {
          id: OrderId,
          response: req.body,
          status: 'PAID',
        },
      });
    })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.updatePayment;
    })
    .then((result) => {
      payment = result;
      return redis.delAsync(`payment-data:${payment.id}`);
    })
    .then(() => {
      return payment;
    });
};

module.exports.success = (req) => {
  return Promise.all([
    redis.getAsync('template'),
  ])
    .then((result) => {
      const [template, data] = result;
      return Object.assign(
        {},
        // data,
        {
          templateData: JSON.parse(template),
        },
      );
    });
};

module.exports.fail = (req) => {
  return Promise.all([
    redis.getAsync('template'),
  ])
    .then((result) => {
      const [template, data] = result;
      return Object.assign(
        {},
        // data,
        {
          templateData: JSON.parse(template),
        },
      );
    });
};

module.exports.checkOrderEvents = checkOrderEvents;

module.exports.checkOrderEventsQuery = checkOrderEventsQuery;

module.exports.orderPayment = orderPayment;

module.exports.certPayment = certPayment;

module.exports.subPayment = subPayment;

module.exports.order = (req) => {
  const { id } = req.body;
  return redis.getAsync(`payment-data:${id}`)
    .then((reply) => {
      if (!reply) return Promise.reject();
      const data = JSON.parse(reply);
      return orderPayment(data);
    });
};

module.exports.certificate = (req) => {
  const { id } = req.body;
  return redis.getAsync(`payment-data:${id}`)
    .then((reply) => {
      if (!reply) return Promise.reject();
      const data = JSON.parse(reply);
      return certPayment(data);
    });
};

module.exports.subscription = (req) => {
  const { id } = req.body;
  return redis.getAsync(`payment-data:${id}`)
    .then((reply) => {
      if (!reply) return Promise.reject();
      const data = JSON.parse(reply);
      return subPayment(data);
    });
};
