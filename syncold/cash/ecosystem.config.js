module.exports = {
  apps: [
    {
      name: 'cash',
      script: './bin/www',
      watch: false,
      env: {
        PORT: 60004,
        NODE_ENV: 'development',
        LOCATION_ENV: 'dev',
      },
      env_production: {
        PORT: 60004,
        NODE_ENV: 'production',
        LOCATION_ENV: 'prod',
      },
    },
  ],
};
