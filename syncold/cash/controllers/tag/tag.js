const redis = require('./../../db/redis');

const redisBatchUpdate = (data) => {
  const { id, alias: { alias } } = data;

  return redis.getAsync(`alias:${alias}`)
    .then((reply) => {
      const oldValue = JSON.parse(reply);
      const newValue = Object.assign(
        {},
        oldValue,
        {
          tag: {
            id,
          },
        },
      );

      const commands = [];
      commands.push(
        ['set', `alias:${alias}`, JSON.stringify(newValue)],
        ['set', `tag:${id}`, JSON.stringify(data)],
      );

      return redis.batch(commands).execAsync();
    });
};

const redisBatchSet = (data) => {
  const { id } = data;
  return redis.batch([
    ['set', `tag:${id}`, JSON.stringify(data)],
  ]).execAsync();
};

const redisBatchDelete = (previousValues) => {
  const { id } = previousValues;
  return redis.batch([
    ['del', `tag:${id}`],
  ]).execAsync();
};

module.exports = (mutation, previousValues, node) => {
  switch (mutation) {
    case 'CREATED': {
      return redisBatchSet(node);
    }
    case 'UPDATED': {
      return redisBatchUpdate(node);
    }
    case 'DELETED': {
      return redisBatchDelete(previousValues);
    }
    default: {
      return redisBatchSet(node);
    }
  }
};
