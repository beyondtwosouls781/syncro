const fs = require('fs');
const path = require('path');
const { createApolloFetch } = require('apollo-fetch');

const { SIMPLE_API_ENDPOINT, ROOT_TOKEN } = require('./../config');

const apolloFetch = createApolloFetch({ uri: SIMPLE_API_ENDPOINT });

apolloFetch.use(({ request, options }, next) => {
  if (!options.headers) {
    options.headers = {};
  }

  options.headers['Authorization'] = `Bearer ${ROOT_TOKEN}`;

  next();
});

let queries = {};
let mutations = {};

const readFilesSync = (dir) => {
  const output = {};
  const filesPath = fs.readdirSync(`${__dirname}/${dir}`);
  filesPath.forEach((filepath) => {
    const buff = fs.readFileSync(`${__dirname}/${dir}/${filepath}`);
    const filename = path.basename(filepath, '.graphql');
    output[filename] = buff.toString();
  });

  return output;
};

const readGraphQlFiles = () => {
  if (Object.keys(queries).length === 0) {
    queries = readFilesSync('queries');
  }
  if (Object.keys(mutations).length === 0) {
    mutations = readFilesSync('mutations');
  }
};

const graphQlFetch = ({
  query,
  variables = {},
  operationName = '',
}) => {
  if (!queries[query] && !mutations[query]) return Promise.reject(new Error('No query'));
  return apolloFetch({
    query: queries[query] || mutations[query],
    variables,
    operationName,
  });
};

module.exports = {
  readGraphQlFiles,
  graphQlFetch,
};
