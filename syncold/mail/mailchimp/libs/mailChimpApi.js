const fetch = require('node-fetch');
const md5 = require('./md5');

class MailChimpApi {
  constructor() {
    this.config = null;
    this.options = null;
    this.fetchWithAuthOptions = this.fetchWithAuthOptions.bind(this);
    this.fetchGET = this.fetchGET.bind(this);
    this.fetchPOST = this.fetchPOST.bind(this);
    this.fetchPUT = this.fetchPUT.bind(this);
    this.fetchPATCH = this.fetchPATCH.bind(this);
    this.fetchDELETE = this.fetchDELETE.bind(this);
  }
  genOptionsWithAuthBasic() {
    this.options = null;
    const config = this.config;
    const mailchimpAuthBasic = `user:${config.mailChimpAPIKey}`;
    const buffMailchimpAuthBasic = new Buffer(mailchimpAuthBasic);
    const base64MailchimpAuthBasic = buffMailchimpAuthBasic.toString('base64');

    this.options = {
      headers: {
        Authorization: `Basic ${base64MailchimpAuthBasic}`,
        'Content-Type': 'application/json',
      },
      compress: true,
    };
  }
  setConfig(config) {
    if (!config) throw new Error('config is not an object');
    this.config = config;
    this.genOptionsWithAuthBasic();
  }
  fetchWithAuthOptions(url, fields, options) {
    const uri = `${this.config.mailChimpHost}${url}`;

    if (fields) {
      return fetch(uri, Object.assign(
        {},
        this.options,
        options,
        {
          body: JSON.stringify(fields),
        },
      ))
        .then(res => res.json());
    }

    return fetch(uri, Object.assign({}, this.options, options))
      .then((res) => {
        const { status } = res;
        if (status !== 200 && status !== 204) {
          console.log(res.status, res.title);
          const err = new Error(res.title);
          err.status = res.status;
          return Promise.reject(err);
        }
        if (status === 204) return res.text();
        return res.json();
      });
  }
  fetchGET(url) {
    return this.fetchWithAuthOptions(url);
  }
  fetchPOST(url, fields) {
    return this.fetchWithAuthOptions(url, fields, { method: 'POST' });
  }
  fetchPATCH(url, fields) {
    return this.fetchWithAuthOptions(url, fields, { method: 'PATCH' });
  }
  fetchPUT(url, fields) {
    return this.fetchWithAuthOptions(url, fields, { method: 'PUT' });
  }
  fetchDELETE(url) {
    return this.fetchWithAuthOptions(url, null, { method: 'DELETE' });
  }
  lists() {
    const { fetchGET } = this;
    const url = '/lists';
    return fetchGET(url)
      .then(json => json.lists);
  }
  checkSubscriptionStatus(listId, email) {
    const { fetchGET } = this;
    const emailMD5Hash = md5(email);
    const url = `/lists/${listId}/members/${emailMD5Hash}`;
    return fetchGET(url);
  }
  subscribe(listId, { email, firstName, lastName, userId }) {
    const { fetchPOST } = this;
    const person = {
      email_address: email,
      status: 'subscribed',
      merge_fields: {
        FNAME: firstName || '',
        LNAME: lastName || '',
        USER_ID: userId || '',
      },
    };
    const url = `/lists/${listId}/members`;
    return fetchPOST(url, person);
  }
  updateSubscribe(listId, subscribedEmail, { email, firstName, lastName, userId }) {
    const { fetchPUT } = this;
    const emailMD5Hash = md5(email);
    const person = {
      email_address: email,
      status: 'subscribed',
      merge_fields: {
        FNAME: firstName || '',
        LNAME: lastName || '',
        USER_ID: userId || '',
        EMAIL: email,
      },
    };
    const url = `/lists/${listId}/members/${emailMD5Hash}`;
    return fetchPUT(url, person);
  }
  unsubscribe(listId, email) {
    const { fetchPUT } = this;
    const emailMD5Hash = md5(email);
    const url = `/lists/${listId}/members/${emailMD5Hash}`;
    return fetchPUT(url, { status: 'unsubscribed' });
  }
  deleteSubscribe(listId, email) {
    const { fetchDELETE } = this;
    const emailMD5Hash = md5(email);
    const url = `/lists/${listId}/members/${emailMD5Hash}`;
    return fetchDELETE(url);
  }
  // e-commerce
  stores() {
    const { fetchGET } = this;
    const url = '/ecommerce/stores';
    return fetchGET(url)
      .then(json => json.stores);
  }
  addNewStore(listId, input) {
    const { fetchPOST } = this;
    const {
      id,
      name,
      domain,
      isSyncing = false,
      email,
      currencyCode,
      moneyFormat,
      primaryLocale,
      timezone,
    } = input;

    const store = {
      list_id: listId,
      id,
      name,
      domain,
      is_syncing: isSyncing,
      email_address: email,
      currency_code: currencyCode,
      money_format: moneyFormat,
      primary_locale: primaryLocale,
      timezone,
    };
    const url = '/ecommerce/stores';
    return fetchPOST(url, store);
  }
  storeSyncingOn(storeId) {
    const { fetchPATCH } = this;
    const url = `/ecommerce/stores/${storeId}`;
    return fetchPATCH(url, { is_syncing: true });
  }
  storeSyncingOff(storeId) {
    const { fetchPATCH } = this;
    const url = `/ecommerce/stores/${storeId}`;
    return fetchPATCH(url, { is_syncing: false });
  }
  products(storeId) {
    const { fetchGET } = this;
    const url = `/ecommerce/stores/${storeId}/products`;
    return fetchGET(url)
      .then(json => json.products);
  }
  addNewProductToStore(storeId, product) {
    const { fetchPOST } = this;
    const url = `/ecommerce/stores/${storeId}/products`;
    return fetchPOST(url, product);
  }
  updateProduct(storeId, productId, product) {
    const { fetchPUT } = this;
    const url = `/ecommerce/stores/${storeId}/products/${productId}`;
    return fetchPOST(url, product);
  }
  carts(storeId) {
    const { fetchGET } = this;
    const url = `/ecommerce/stores/${storeId}/carts`;
    return fetchGET(url)
      .then(json => json.carts);
  }
  addNewCart(storeId, input) {
    const { fetchPOST } = this;
    const {
      id,
      customer,
      orderTotal,
      products,
      campaignId,
      checkoutUrl,
      currencyCode,
    } = input;
    const {
      id: customerId,
      email,
      firstName,
      lastName,
      optInStatus = true,
    } = customer;

    const cart = {
      id,
      customer: {
        id: customerId,
        email_address: email,
        first_name: firstName,
        last_name: lastName,
        opt_in_status: optInStatus,
      },
      currency_code: currencyCode,
      campaign_id: campaignId,
      checkout_url: checkoutUrl,
      order_total: orderTotal,
      lines: products,
    };

    const url = `/ecommerce/stores/${storeId}/carts`;
    return fetchPOST(url, cart);
  }
  updateCart(storeId, cartId, input) {
    const { fetchPUT } = this;
    const {
      customer,
      orderTotal,
      products,
      campaignId,
      checkoutUrl,
      currencyCode,
    } = input;

    const {
      id: customerId,
      email,
      firstName,
      lastName,
      optInStatus = true,
    } = customer;

    const cart = {
      id: cartId,
      customer: {
        id: customerId,
        email_address: email,
        first_name: firstName,
        last_name: lastName,
      },
      currency_code: currencyCode,
      campaign_id: campaignId,
      checkout_url: checkoutUrl,
      order_total: orderTotal,
      lines: products,
    };

    const url = `/ecommerce/stores/${storeId}/carts/${cartId}`;
    return fetchPUT(url, cart);
  }
  deleteCart(storeId, cartId) {
    const { fetchDELETE } = this;
    const url = `/ecommerce/stores/${storeId}/carts/${cartId}`;
    return fetchDELETE(url);
  }
  // automations
  automations() {
    const { fetchGET } = this;
    const url = '/automations';
    return fetchGET(url)
      .then(json => json.automations);
  }
  // Campaigns
  campaigns() {
    const { fetchGET } = this;
    const url = '/campaigns';
    return fetchGET(url)
      .then(json => json.campaigns);
  }
  campaignFolders() {
    const { fetchGET } = this;
    const url = '/campaign-folders';
    return fetchGET(url)
      .then(json => json.folders);
  }
}

module.exports = MailChimpApi;
