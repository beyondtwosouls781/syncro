// const settingsQuery = require('./../graphql/settingsQuery');
const redisGetSettings = require('./../db/redisGetSettings');

const sendTemplate = require('./sendTemplate');

module.exports = (input, template) => {
  if (!template || !template.subject || !template.tags) return Promise.reject(new Error('No template params found'));
  if (!Array.isArray(input) && (!input.email || !input.firstName || !input.lastName)) return Promise.reject(new Error('No data input found'));

  return redisGetSettings(input, template)
    .then(settings => sendTemplate(input, template, settings));
};
