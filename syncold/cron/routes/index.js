const express = require('express');
const router = express.Router();

const { pool } = require('./../cron');
const controller = require('./../controllers/event');

/* GET home page. */
router.get('/', (req, res, next) => {
  res.send('OK');
});

router.post('/event', (req, res, next) => {
  const { data: { Event } } = req.body;
  if (!Event) return next(new Error('Wrong param'));
  const { node, mutation, previousValues } = Event;
  try {
    const result = controller(node, mutation, previousValues);
    return res.json(result);
  } catch (err) {
    next(err);
  }
});

router.get('/status', (req, res, next) => {
  console.log('pool - ', pool);
  res.send('OK');
});

module.exports = router;
