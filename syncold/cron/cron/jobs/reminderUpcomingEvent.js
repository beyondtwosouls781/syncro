const { graphQlFetch } = require('./../../graphql');
const dateRange = require('./../../lib/dateRange');
const fetcPostJson = require('./../../lib/fetcPostJson');

const { MAIL_HOST } = require('./../../config');

exports.onTick = function onTick(callback) {
  console.log('tick - allEventsUpcomingQuery', new Date());
  this.stop();
  const date = dateRange(new Date());
  let allEvents = null;

  graphQlFetch({
    query: 'allEventsUpcomingQuery',
    variables: {
      // dateGt: date.start,
      // dateLt: date.end,
      // dateTicket: new Date(),
      // datePayment: new Date(),
      dateGt: new Date('2017-01-01'),
      dateLt: new Date('2019-01-01'),
      dateTicket: new Date(),
      datePayment: new Date(),
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      allEvents = data.allEvents;
      if (allEvents.length === 0) return Promise.resolve();

      return fetcPostJson(
        `${MAIL_HOST}/cron/upcoming`,
        {
          data: { allEvents },
        },
      );
    })
    .then((result) => {
      if (allEvents.length === 0) return Promise.resolve();

      return fetcPostJson(
        `${MAIL_HOST}/cron/non-payments`,
        {
          allEvents,
        },
      );
    })
    .then((result) => {
      // this.start();
    })
    .catch((err) => {
      console.error(err);
      this.start();
    });
};

exports.onComplete = () => {

};
