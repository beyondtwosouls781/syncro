const { pool } = require('./../../cron');

module.exports = (node, mutation, previousValues) => {

  switch (mutation) {
    case 'CREATED': {
      return pool.add(node);
    }
    case 'UPDATED': {
      return pool.update(previousValues, node);
    }
    case 'DELETED': {
      return pool.remove(previousValues);
    }
    default: {
      return poll.add(node);
    }
  }
};
