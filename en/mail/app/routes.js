const index = require('./../routes/index');
const user = require('./../routes/user');
const subscriptions = require('./../routes/subscriptions');
const certificate = require('./../routes/certificate');
const payment = require('./../routes/payment');
const order = require('./../routes/order');
const ticket = require('./../routes/ticket');
const subscribe = require('./../routes/subscribe');
const cron = require('./../routes/cron');

module.exports = (app) => {
  app.use('/', index);
  app.use('/user', user);
  app.use('/order', order);
  app.use('/subscriptions', subscriptions);
  app.use('/certificate', certificate);
  app.use('/payment', payment);
  app.use('/ticket', ticket);
  app.use('/subscribe', subscribe);
  app.use('/cron', cron);

  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handler
  app.use((err, req, res, next) => {
    console.error(err);
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send(err.message);
  });
};
