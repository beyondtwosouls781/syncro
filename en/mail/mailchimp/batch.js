const MailChimpApi = require('./libs/mailChimpApi');
const config = require('./config');

const PromiseMap = (items, iterator) => {
  let current = Promise.resolve();

  return Promise.all(items.map((item) => {
    current = current
      .then(() => iterator(item));
    return current;
  }));
};

const mailChimpApi = new MailChimpApi();
mailChimpApi.setConfig(config);

const deleteAllCarts = storeId => (
  mailChimpApi.carts(storeId)
    .then((carts) => {
      const ids = carts.map(({ id }) => id);
      return PromiseMap(ids, id => mailChimpApi.deleteCart(storeId, id));
    })
    .then((result) => {
      console.log(result);
    })
);

module.exports = { deleteAllCarts };
