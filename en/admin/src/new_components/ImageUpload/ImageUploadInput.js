import React, { Component } from 'react';
import Button from 'material-ui/Button';
import FileUpload from 'material-ui-icons/FileUpload';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';
import { compose } from 'react-apollo';

import withSnackbar from './../../new_hoc/withSnackbar';
import config from './../../config';
import { GC_AUTH_TOKEN } from './../../constants';

const { FILE_API_ENDPOINT, IMAGE_HOST } = config;

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  container: {
    width: '100%',
    margin: theme.spacing.unit,
  },
});

class ImageUploadInput extends Component {
  constructor(props) {
    super(props);
    const {
      value,
    } = props;

    this.state = {
      secret: value,
      res: null,
      isRequested: false,
      file: null,
    };
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {
      value,
    } = nextProps;
    this.setState({
      secret: value,
    });
  }

  handleButtonClick() {
  }

  handleChange(event) {
    const { files } = event.target;
    const name = Object.keys(files)
      .map(key => files[key].name);
      // .map(({ name }));

    this.setState({
      name,
      files,
      isRequested: true,
    }, this.uploadFile);
  }

  uploadFile = async () => {
    const { files } = this.state;
    const token = localStorage.getItem(GC_AUTH_TOKEN);
    const data = new FormData()
    data.append('data', files[0])

    try {
      const response = await fetch(
        FILE_API_ENDPOINT,
        {
          method: 'POST',
          body: data,
          headers: {
            Authorization: `Bearer ${token.replace(/\n/g, '')}`,
          },
        },
      );

      const file = await response.json();
      const { secret } = file;
      this.setState({
        isRequested: false,
        success: true,
        secret,
      }, () => {
        this.props.onLoaded(secret);
      });
    } catch (e) {
      this.setState({
        isRequested: false,
        success: false,
        secret: null,
      }, () => {
        this.props.snackbar(new Error('Ошибка. Файл не загружен'));
      });
    }
  }

  render() {
    const {
      classes,
      label,
      multiple,
      size,
    } = this.props;
    const {
      name,
      isRequested,
      secret,
    } = this.state;

    return (
      <div className={classes.container}>
        <Typography type="title" gutterBottom>
          {label}
        </Typography>
        {/* <Typography type="body2" gutterBottom>
          Файл: { name.join(', ') || 'не выбран' }
        </Typography> */}
        <Typography type="body2" gutterBottom>
          Оригиналный файл: { isRequested && 'Загрузка...'}
          {
            !isRequested && secret && secret !== 'no-photo' &&
            <a href={`${FILE_API_ENDPOINT}/${secret}`} target="_blank">{secret}</a>
          }
          {((!isRequested && !secret) || (secret === 'no-photo')) && 'не выбран'}
        </Typography>
        <Typography type="body2" gutterBottom>
          Кроп: { isRequested && 'Загрузка...'}
          {
            !isRequested && secret && secret !== 'no-photo' &&
            <a href={`${IMAGE_HOST}/${secret}/${size}`} target="_blank">{secret}/{size}</a>
          }
          {((!isRequested && !secret) || (secret === 'no-photo')) && 'не выбран'}
        </Typography>
        <div>
          <img src={`${IMAGE_HOST}/${secret}/${size}`} alt="" />
        </div>
        <Button
          className={classes.button}
          raised
          component="label"
          color="default"
          disabled={isRequested}
          onClick={this.handleButtonClick}
        >
          {'Загрузить'}
          <FileUpload className={classes.rightIcon} />
          <input
            onChange={this.handleChange}
            style={{ display: 'none' }}
            type="file"
            // multiple={multiple}
          />
        </Button>
      </div>
    );
  }
}

ImageUploadInput.defaultProps = {
  value: [],
  onLoaded() {},
};

export default compose(withSnackbar(), withStyles(styles))(ImageUploadInput);
