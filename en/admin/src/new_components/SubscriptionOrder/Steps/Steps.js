import React from 'react';

import SelectUser from './../../Order/CreateOrder/Steps/SelectUser';
import SelectPaymentType from './../../Order/CreateOrder/Steps/SelectPaymentType';
import SelectProduct from './SelectProduct';
import OrderDiscounts from './../../OrderDiscounts';
import MutationOrder from './../MutationOrder/';
import OrderResult from './OrderResult';

const Steps = (props) => {
  const {
    activeStep,
  } = props;

  switch (activeStep) {
    case 0: {
      return <SelectUser {...props} />;
    }
    case 1: {
      return <SelectProduct {...props} />;
    }
    case 2: {
      return <OrderDiscounts {...props} type="subscription" />;
    }
    case 3: {
      return <SelectPaymentType {...props} />;
    }
    case 4: {
      return <MutationOrder {...props} />;
    }
    default: {
      return <SelectUser {...props} />;
    }
  }
};

export default Steps;
