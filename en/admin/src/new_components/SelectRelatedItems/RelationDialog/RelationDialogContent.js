import React, { Component } from 'react';
import {
  DialogContent,
} from 'material-ui/Dialog';

import EnhancedTable from './../../EnhancedTable';

import { formatInputDataArrayForTable } from './../../../new_utils';

class RelationDialogContent extends Component {
  constructor(props) {
    super(props);
    const {
      selected,
      gqlQueryName,
      queryResultObjectName,
      formatInputData,
    } = this.props;

    let data = [];
    if (this.props[gqlQueryName] && this.props[gqlQueryName][queryResultObjectName]) {
      data = formatInputData(this.props[gqlQueryName][queryResultObjectName]);
    }
    this.state = {
      selected,
      data,
      isRequested: data ? false : true,
    };
  }

  componentWillReceiveProps(nextProps) {
    const {
      gqlQueryName,
      queryResultObjectName,
      formatInputData,
      selected,
      queryFilterData,
    } = nextProps;

    let data = [];
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      data = formatInputData(nextProps[gqlQueryName][queryResultObjectName]);
    }

    if (selected.length > 0) {
      this.setState({
        selected,
        data,
        isRequested: false,
      });
    } else {
      this.setState({
        data,
        isRequested: false,
      });
    }
  }

  // handleFilterQuerySubmit(queryFilterData) {
  //   this.setState({
  //     isRequested: true,
  //     queryFilterData,
  //   }, this.refetch);
  // }


  // refetch = async () => {
  //   const { gqlQueryName, queryResultObjectName, modifyQueryFilter, refetchLoading, refetchDone, formatInputData } = this.props;
  //   const { queryFilterData } = this.state;
  //
  //   const result = await this.props[gqlQueryName].refetch({ filter: modifyQueryFilter(queryFilterData) });
  //   if (result.data) {
  //     const data = formatInputData(result.data[queryResultObjectName]);
  //     this.setState({
  //       data,
  //       isRequested: false,
  //     });
  //   } else {
  //     this.setState({
  //       isRequested: false,
  //     });
  //   }
  // }

  render() {
    const {
      tableTitle,
      columnData,
      selected,
      onSelect,
      multiple,
      all,
      filter,
      onFilterQuerySubmit,
      isRequested,
    } = this.props;

    const {
      queryFilterData,
      data,
    } = this.state;

    return (
      <div>
        <EnhancedTable
          selected={selected}
          onSelect={onSelect}
          tableTitle={tableTitle}
          columnData={columnData}
          data={formatInputDataArrayForTable(data)}
          multiple={multiple}
          all={all}
        />
      </div>
    );
  }
}

RelationDialogContent.defaultProps = {
  selected: [],
  formatInputData: data => data || [],
};

export default RelationDialogContent;
