import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';

import * as R from 'ramda';

import slugify from './../../../node_modules/transliteration/lib/browser/transliteration.min.js';
import { stateOfFields, stateOfData, validate } from './../../new_utils';
import BasicFields from './BasicFields';
import RelationFields from './RelationFields';
import BasicFormActions from './BasicFormActions';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  paper: {
    padding: 16,
    color: theme.palette.text.secondary,
  },
});

class BasicForm extends React.Component {
  constructor(props) {
    super(props);
    const {
      fields,
      data,
    } = props;
    if (data) {
      this.state = stateOfData(fields, data);
    } else {
      this.state = stateOfFields(fields);
    }

    this.handleReset = this.handleReset.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePreview = this.handlePreview.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { data, fields } = nextProps;
    if (data) {
      const newState = stateOfData(fields, data);
      this.setState(newState);
    }
  }

  handleChange = name => (event) => {
    const newState = Object.assign({}, this.state);
    let { value } = event.target;

    if (event.target.type === 'checkbox') {
      value = !newState[name];
    }

    if (name === 'title' && Object.prototype.hasOwnProperty.call(newState, 'alias')) {
      newState.alias = slugify(value);
    }

    if ((name === 'firstName' || name === 'lastName')
      && !Object.prototype.hasOwnProperty.call(newState, 'title')
      && Object.prototype.hasOwnProperty.call(newState, 'alias')
    ) {
      const str = (name === 'firstName') ? `${value} ${newState.lastName}` : `${newState.firstName} ${value}`;
      newState.alias = slugify(str);
    }

    if (newState.errors.length > 0) {
      newState.errors = validate(this.props.fields, newState);
    }

    newState[name] = value;
    // this.setState(newState, this.props.onChange(newState));
    this.setState(newState);
  }
  handleChangeNoEvent = name => (selectedIds, selected) => {
    const newState = Object.assign({}, this.state);
    newState[name] = selected || selectedIds;
    if (newState.errors.length > 0) {
      newState.errors = validate(this.props.fields, newState);
    }
    // this.setState(newState, this.props.onChange(newState));
    this.setState(newState);
  }
  handleReset() {
    const newState = stateOfFields(this.props.fields);
    // this.setState(newState, this.props.onChange(newState));
    this.setState(newState);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { onSubmit, fields } = this.props;
    const errors = validate(fields, this.state);

    if (errors.length > 0) {
      this.setState({
        errors,
      });
    } else {
      onSubmit(this.state);
    }
  }
  handlePreview(event) {
    event.preventDefault();
    const { onPreview, fields } = this.props;
    const errors = validate(fields, this.state);
    if (errors.length > 0) {
      this.setState({
        errors,
      });
    } else {
      onPreview(this.state);
    }
  }
  render() {
    const {
      classes,
      formTitle,
      fields,
      actions,
      disabledAllInputs,
      disabledAllActions,
      srcData,
      snackbar,
    } = this.props;
    const {
      errors,
    } = this.state;

    return (
      <Paper className={classes.paper}>
        {
          formTitle &&
          <Typography
            className={classes.title}
            type="subheading"
            color="inherit"
            noWrap
          >
            {formTitle}
          </Typography>
        }
        <div>
          <form
            className={classes.container}
            noValidate
            autoComplete="off"
            onSubmit={this.handleSubmit}
          >
            <BasicFields
              snackbar={snackbar}
              fields={fields}
              values={this.state}
              errors={errors}
              onChange={this.handleChange}
              onChangeNoEvent={this.handleChangeNoEvent}
              disabledAllInputs={disabledAllInputs}
              srcData={srcData}
            />
            <RelationFields
              snackbar={snackbar}
              fields={fields}
              values={this.state}
              errors={errors}
              onChange={this.handleChange}
              onChangeNoEvent={this.handleChangeNoEvent}
              disabledAllInputs={disabledAllInputs}
              srcData={srcData}
            />
            <div style={{ width: '100%' }}>
              <BasicFormActions
                actions={actions}
                disabledAllActions={disabledAllActions}
                onReset={this.handleReset}
                onSubmit={this.handleSubmit}
                onPreview={this.handlePreview}
              />
            </div>
          </form>
        </div>
      </Paper>
    );
  }
}

BasicForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

BasicForm.defaultProps = {
  fields: [],
  onChange() {},
  onSubmit() {},
  onPreview() {},
};

export default withStyles(styles)(BasicForm);
