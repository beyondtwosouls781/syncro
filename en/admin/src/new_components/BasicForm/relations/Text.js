import React from 'react';

import TextField from 'material-ui/TextField';

const Text = (props) => {
  const {
    label,
    type,
    className,
    value,
    id,
    disabled,
    required,
    error,
    helperText,
    onChange,
  } = props;

  return (
    <div>
      <TextField
        label={label}
        type={type}
        className={className}
        value={value}
        onChange={onChange(id)}
        margin="normal"
        disabled={disabled}
        required={required}
        error={error}
        helperText={helperText}
      />
      <div>Нет связанных компонентов</div>
    </div>
  );
};

export default Text;
