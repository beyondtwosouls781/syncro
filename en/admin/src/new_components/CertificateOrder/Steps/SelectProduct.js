import React, { Component } from 'react';
// import Typography from 'material-ui/Typography';

import SelectRelatedItems from './../../SelectRelatedItems';
import gql from './../../../new_pages/Catalog/gql';
import {
  ALL_CERTIFICATE_PRODUCTS_QUERY_SHORT,
} from './../../../constants';
import {
  certificatesShort as columnDataCertificates,
} from './../../../new_pages/Catalog/columnData';
import filter from './../../../new_pages/Catalog/fields/filterCertificatesShort';
import modifyQueryFilter from './../../../new_pages/Catalog/modifyQueryFilter';

const SelectProduct = (props) => {
  const {
    product,
    onSelectProduct,
  } = props;

  return (
    <div style={{ marginTop: 20 }} >
      <SelectRelatedItems
        srcData={product}
        relationTitle="Выберите тип сертификата"
        tableTitle="Сертификаты"
        gql={gql}
        gqlQueryName={ALL_CERTIFICATE_PRODUCTS_QUERY_SHORT}
        queryResultObjectName="allCertificateProducts"
        onChange={onSelectProduct}
        columnData={columnDataCertificates}
        filter={filter}
        modifyQueryFilter={modifyQueryFilter.certificates}
      />
    </div>
  );
};

export default SelectProduct;
