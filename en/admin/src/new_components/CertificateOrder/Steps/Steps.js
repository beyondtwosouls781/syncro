import React from 'react';

import SelectUser from './../../Order/CreateOrder/Steps/SelectUser';
import SelectPaymentType from './../../Order/CreateOrder/Steps/SelectPaymentType';
import SelectProduct from './SelectProduct';
import OrderDiscounts from './../../OrderDiscounts';
import SelectRecipientOfGift from './SelectRecipientOfGift';
import MutationOrder from './../MutationOrder/';

const Steps = (props) => {
  const {
    activeStep,
  } = props;

  switch (activeStep) {
    case 0: {
      return <SelectUser {...props} />;
    }
    case 1: {
      return <SelectProduct {...props} />;
    }
    case 2: {
      return <OrderDiscounts {...props} type="certificate" />;
    }
    case 3: {
      return <SelectRecipientOfGift {...props} />;
    }
    case 4: {
      return <SelectPaymentType {...props} />;
    }
    case 5: {
      return <MutationOrder {...props} />;
    }
    default: {
      return <SelectUser {...props} />;
    }
  }
};

export default Steps;
