import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import keycode from 'keycode';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import DeleteIcon from 'material-ui-icons/Delete';
import FilterListIcon from 'material-ui-icons/FilterList';

import EnhancedTableHead from './EnhancedTableHead';
import EnhancedTableToolbar from './EnhancedTableToolbarSecond';
import { sort } from './../../new_utils';

let counter = 0;
function createData(title, calories, fat, carbs, protein) {
  counter += 1;
  return { id: counter, title, calories, fat, carbs, protein };
}

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 800,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

class EnhancedTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    const {
      selected,
      page,
      rowsPerPage,
      order,
      orderBy,
      data,
    } = props;
    const sortedData = data.slice().sort((a, b) => (a.src.updatedAt < b.src.updatedAt ? -1 : 1));
    this.state = {
      order,
      orderBy,
      selected,
      data: sortedData,
      page,
      rowsPerPage,
    };
    this.isSelected = this.isSelected.bind(this);
  }
  componentWillMount() {
    const { all } = this.props;
    const { selected, data } = this.state;
    if (all && data.length > 0 && selected.length !== data.length) this.handleSelectAllClick(null, true);
  }
  componentWillReceiveProps(nextProps) {
    const { selected, data} = nextProps;

    const sortedData = data.slice().sort((a, b) => (a.src.updatedAt < b.src.updatedAt ? -1 : 1));
    this.setState({
      selected,
      data: sortedData,
    });
  }
  componentWillUpdate(nextProps, nextState) {
    const { all } = nextProps;
    const { selected, data } = nextState;
    if (all && data.length > 0 && selected.length !== data.length) this.handleSelectAllClick(null, true);
  }
  handleRequestSort = (event, property) => {
    const orderBy = property;
    const {
      data: dataState,
      order: orderState,
      orderBy: orderByState,
    } = this.state;

    let order = 'asc';

    if (orderByState === property && orderState === 'asc') order = 'desc';

    const data = sort(dataState, orderBy, order);

    this.setState({ data, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    const { multiple, all } = this.props;

    if (all) {
      const selected = this.state.data.slice();
      this.setState({ selected }, this.props.onSelect(selected));
    } else if (multiple && !all) {
      let selected = [];
      if (checked) {
        selected = this.state.data.slice();
      }
      this.setState({ selected }, this.props.onSelect(selected));
    }
  };

  handleKeyDown = (event, n) => {
    if (keycode(event) === 'space') {
      this.handleClick(event, n);
    }
  };

  handleClick = (event, n) => {
    const { selected } = this.state;
    const { multiple } = this.props;
    let newSelected = [];

    if (multiple) {
      let selectedIndex = -1;
      selected.forEach((item, index) => {
        if (item.id === n.id) selectedIndex = index;
      });
      if (selectedIndex === -1) {
        newSelected = newSelected.concat(selected, n);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(selected.slice(1));
      } else if (selectedIndex === selected.length - 1) {
        newSelected = newSelected.concat(selected.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      }
    } else {
      newSelected = [n];
    }
    this.setState({ selected: newSelected }, this.props.onSelect(newSelected));
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected(n) {
    const { all } = this.props;
    if (all) return true;
    return this.state.selected.some(({ id }) => n.id === id);
  }

  render() {
    const {
      classes,
      title,
      columnData,
      tableTitle,
      toolbarActions,
    } = this.props;

    const {
      data,
      order,
      orderBy,
      selected,
      rowsPerPage,
      page,
    } = this.state;

    let emptyRows = rowsPerPage - Math.min(rowsPerPage, (data.length - page) * rowsPerPage);
    if (data.length === 0) emptyRows = 0;

    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar
          numSelected={selected.length}
          title={tableTitle}
          toolbarActions={toolbarActions}
        />
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
              columnData={columnData}
            />
            <TableBody>
              {data.slice(page * rowsPerPage, (page * rowsPerPage) + rowsPerPage).map((n) => {
                const isSelected = this.isSelected(n);
                return (
                  <TableRow
                    hover
                    onClick={event => this.handleClick(event, n)}
                    onKeyDown={event => this.handleKeyDown(event, n)}
                    role="checkbox"
                    aria-checked={isSelected}
                    tabIndex={-1}
                    key={n.id}
                    selected={isSelected}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox checked={isSelected} />
                    </TableCell>
                    {
                      columnData.map((column) => {
                        const {
                          id,
                          label,
                          disablePadding,
                          numeric,
                        } = column;

                        return (
                          <TableCell
                            key={id}
                            padding={disablePadding ? 'none' : 'default'}
                            numeric={numeric || false}
                          >
                            {n[id]}
                          </TableCell>
                        );
                      })
                    }

                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  count={data.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </Paper>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

EnhancedTable.defaultProps = {
  selected: [],
  data: [],
  order: 'desc',
  orderBy: 'updatedAt',
  page: 0,
  rowsPerPage: 10,
  columnData: [],
  multiple: false,
};

export default withStyles(styles)(EnhancedTable);
