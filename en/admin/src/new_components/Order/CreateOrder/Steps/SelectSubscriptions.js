import React, { Component } from 'react';
import Typography from 'material-ui/Typography';

import SelectRelatedItems from './../../../SelectRelatedItems';

import gql from './../../../../new_pages/Subscriptions/gql';
import { ALL_SUSCRIPTIONS_OF_USER_QUERY } from './../../../../constants';
import columnDataShort from './../../../../new_pages/Subscriptions/columnDataShort';
import fields from './fields';
import modifyQueryFilter from './modifyQueryFilter';
import modifyQueryOptions from './modifyQueryOptions';

const SelectCertificateOrSubscription = (props) => {
  const {
    participants,
    subscriptions,
    onSelectSubscriptions,
  } = props;
  if (participants.length > 0) {
    return (
      <div style={{ marginTop: 20 }} >
        <Typography type="title" gutterBottom>
          Абонементы
        </Typography>
        {
          participants.map(({ id, firstName, lastName }) => (
            <SelectRelatedItems
              key={id}
              srcData={subscriptions[id]}
              relationTitle={`${firstName} ${lastName}`}
              tableTitle="Абонементы"
              gql={gql}
              gqlQueryName={ALL_SUSCRIPTIONS_OF_USER_QUERY}
              queryResultObjectName="allSubscriptions"
              onChange={onSelectSubscriptions(id)}
              columnData={columnDataShort}
              modifyQueryFilter={modifyQueryFilter.subscriptions({ id })}
            />
          ))
        }
      </div>
    );
  }

  return (
    <div style={{ marginTop: 20, marginBottom: 20 }}>
      <Typography type="title" gutterBottom>
        Абонементы
      </Typography>
      <Typography type="body1" gutterBottom>
        Нет выбранных участников
      </Typography>
    </div>
  );
};

export default SelectCertificateOrSubscription;
