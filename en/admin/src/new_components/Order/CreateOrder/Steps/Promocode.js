import React, { Component } from 'react';

import BasicForm from './../../../BasicForm';


class Promocode extends Component {
  runQuery() {
    const {
      lectures,
      courses,
      cycles,
    } = this.props;

    this.props.client.query({
      query: gql`...`,
      variables: { },
    });
  }
  render() {
    const {
      isRequested,
      formData,
      data,
    } = this.state;
    const {
      formTitle,
      fields
    } = this.props;

    return (
      <BasicForm
        formTitle={formTitle}
        fields={fields}
        data={formData || data}
        srcData={srcData}
        disabledAllInputs={isRequested}
        disabledAllActions={isRequested}
        actions={['send']}
        onSubmit={this.handleSubmit}
      />
    );
  }

}

export default Promocode;
