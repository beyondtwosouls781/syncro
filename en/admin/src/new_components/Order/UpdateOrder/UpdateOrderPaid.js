import React, { Component } from 'react';
import Order from './../CreateOrder';

class UpdateOrderPaid extends Component {
  constructor(props) {
    super(props);
    const { order } = props;
    this.state = {
      order,
    };
  }
  componentWillReceiveProps(nextProps) {
    const { order } = nextProps;
    this.setState({
      order,
    });
  }
  render() {
    const { order } = this.state;
    return (
      <div>Not Paid</div>
    );
  }

}

export default UpdateOrderPaid;
