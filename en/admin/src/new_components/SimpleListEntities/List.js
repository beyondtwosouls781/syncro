import React, { Component } from 'react';

import FilterOfQuery from './../FilterOfQuery';
import BasicTable from './../BasicTable';
import EnhancedTableForList from './../EnhancedTableForList';
import Actions from './../Actions';
import DialogDeleteItem from './../DialogDeleteItem';

import { formatInputDataArrayForTable } from './../../new_utils';

class List extends Component {
  constructor(props) {
    super(props);

    const {
      gqlQueryName,
      gqlMutationName,
      queryResultObjectName,
    } = props;

    let data = [];

    if (props[gqlQueryName] && props[gqlQueryName][queryResultObjectName]) {
      data = formatInputDataArrayForTable(props[gqlQueryName][queryResultObjectName]);
    }

    this.state = {
      data,
      isRequested: true,
      itemToDelete: null,
      formFilterData: null,
      gqlMutationNameResult: `${gqlMutationName}Result`,
      gqlMutationNameError: `${gqlMutationName}Error`,
      gqlMutationNameLoading: `${gqlMutationName}Loading`,
      gqlMutationNameReset: `${gqlMutationName}Reset`,
    };
    this.handleFilterQuerySubmit = this.handleFilterQuerySubmit.bind(this);
    this.handleView = this.handleView.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancelDelete = this.handleCancelDelete.bind(this);
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
    this.refetch = this.refetch.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const {
      gqlMutationNameResult,
      gqlMutationNameError,
      gqlMutationNameLoading,
      gqlMutationNameReset,
    } = this.state;

    const {
      gqlQueryName,
      queryResultObjectName,
      resulMutatiomtMessageSuccses,
      mutationResultObjectName,
      resulMutationMessageSuccses,
      modifyQueryFilter,
    } = nextProps;

    let isRequested = false
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName].loading) {
      isRequested = true;
    }
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName].error) {
      isRequested = true;
      this.props.snackbar(nextProps[gqlQueryName].error);
      this.setState({
        isRequested: false,
      });
      return false;
    }
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName].queryResultObjectName) {
      isRequested = false;
    }
    if (nextProps[gqlMutationNameLoading]) {
      isRequested = true;
    }
    if (nextProps[gqlMutationNameResult]) {
      const { id } = nextProps[gqlMutationNameResult].data[mutationResultObjectName];
      this.props.snackbar({ message: `${resulMutationMessageSuccses} - id: ${id}`, name: 'Success' });
      this.setState({
        isRequested: false,
        itemToDelete: null,
      });
      this.refetch();
      return false;
    }
    if (nextProps[gqlMutationNameError]) {
      this.props.snackbar(nextProps[gqlMutationNameError]);
      this.props[gqlMutationNameReset]();
      this.setState({
        isRequested: false,
      });
      return false;
    }

    let data = [];
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      data = formatInputDataArrayForTable(nextProps[gqlQueryName][queryResultObjectName]);
    }

    this.setState({
      isRequested,
      data,
    });
  }

  refetch = async () => {
    const { gqlQueryName, queryResultObjectName, modifyQueryFilter, refetchLoading, refetchDone } = this.props;
    const { formFilterData } = this.state;

    const result = await this.props[gqlQueryName].refetch({ filter: modifyQueryFilter(formFilterData) });
    if (result.data) {
      const data = formatInputDataArrayForTable(result.data[queryResultObjectName]);
      this.setState({
        data,
        isRequested: false,
      });
    } else {
      this.setState({
        isRequested: false,
      });
    }

  }

  handleFilterQuerySubmit(formFilterData) {
    this.setState({
      isRequested: true,
      formFilterData,
    }, this.refetch);
  }
  handleEdit(n) {
    const {
      linkForUpdateMaterial,
      history,
      replaceMutationAndViewMaterialLink,
    } = this.props;
    let link = linkForUpdateMaterial;
    if (replaceMutationAndViewMaterialLink) {
      link = replaceMutationAndViewMaterialLink(n, link);
    }
    history.push(`${link}/${n.id}`);
  }
  handleView(n) {
    const {
      linkForViewMaterial,
      history,
      replaceMutationAndViewMaterialLink,
    } = this.props;
    let link = linkForViewMaterial;
    if (replaceMutationAndViewMaterialLink) {
      link = replaceMutationAndViewMaterialLink(n, link);
    }

    history.push(`${link}/${n.id}`);
  }
  handleDelete(n) {

    this.setState({
      itemToDelete: n,
    });
  }
  handleCancelDelete() {
    this.setState({
      itemToDelete: null,
    });
  }
  handleDeleteConfirm = async () => {
    const { itemToDelete } = this.state;
    const { gqlMutationName } = this.props;
    this.props[gqlMutationName]({
      variables: {
        id: itemToDelete.id,
      },
    });
  }
  render() {
    const {
      columnData,
      gqlQueryName,
      queryResultObjectName,
      fields,
      actionsButtons,
    } = this.props;
    const {
      isRequested,
      itemToDelete,
      data,
    } = this.state;

    return (
      <div style={{ marginBottom: 20 }}>
        <Actions
          items={actionsButtons}
        />
        <FilterOfQuery
          formTitle="Фильтр"
          fields={fields.filter}
          isRequested={isRequested}
          onSubmit={this.handleFilterQuerySubmit}
        />
        <EnhancedTableForList
          data={data}
          columnData={columnData}
          isRequested={isRequested}
          onView={this.handleView}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
          showDeleteAction
        />
        <DialogDeleteItem
          open={itemToDelete ? true : false}
          title={itemToDelete ? itemToDelete.title : ''}
          onCancel={this.handleCancelDelete}
          onConfirm={this.handleDeleteConfirm}
          disabled={isRequested}
        />
      </div>
    );
  }
}

export default List;
