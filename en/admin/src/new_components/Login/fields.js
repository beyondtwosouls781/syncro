const password = [
  {
    id: 'password',
    label: 'Пароль',
    required: true,
    type: 'password',
    fullWidth: true,
  },
];

const email = [
  {
    id: 'email',
    label: 'E-mail',
    required: true,
    type: 'text',
    autoFocus: true,
    fullWidth: true,
  },
];

const signIn = email.concat(password);

const resetPass = password.concat([
  {
    id: 'repassword',
    type: 'password',
    label: 'Повторите пароль',
    required: true,
  },
]);

export default {
  signIn,
  resetPassEmail: email,
  resetPass,
};
