// const authToken = localStorage.getItem(GC_AUTH_TOKEN);
// if (!authToken || (account && !account.id)) {
//   localStorage.removeItem(GC_AUTH_TOKEN);
//   return (
//     <Login {...props} />
//   );
// }

import React, { Component } from 'react';

import Dialog, {
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from 'material-ui/Dialog';

import Form from './Form';
import fields from './fields';
import withProgress from './../../new_hoc/withProgress';

import {
  LOGGED_IN_USER_WITH_ROLE,
  AUTHENTICATE_USER_MUTATION,
  USER_RESET_PASS_SEND_EMAIL_MUTATION,
} from './../../constants';

class DialogInner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeForm: 'signIn',
    };
    this.handleChangeFormType = this.handleChangeFormType.bind(this);
  }
  handleChangeFormType(formType) {
    this.setState({
      activeForm: formType,
    });
  }
  render() {
    const { authToken, account, onSubmitSignIn, onSubmitResetPass, onSubmitResetPassSendEmail } = this.props;
    const { activeForm } = this.state;

    let onSubmit = null;

    switch(activeForm) {
      case 'resetPassEmail': {
        onSubmit = onSubmitResetPassSendEmail;
        break;
      }
      case 'resetPass': {
        onSubmit = onSubmitResetPass;
        break;
      }
      default: {
        onSubmit = onSubmitSignIn;
      }
    }
    const currentFields = fields[activeForm];

    return (
      <div>
        <DialogTitle id="auth-dialog-title">
          {
            activeForm === 'signIn' &&
            `Аутентификация`
          }
          {
            (activeForm === 'resetPassEmail' || activeForm === 'resetPass') &&
            `Сброс пароля`
          }
        </DialogTitle>
        {
          !authToken || (account && !account.id) ?
          (
            <Form
              {...this.props}
              type={activeForm}
              onSubmit={onSubmit}
              onChangeFormType={this.handleChangeFormType}
              fields={currentFields}
            />
          ) :
          (
            <DialogContent>
              <DialogContentText>
                Проверка прав доступа
              </DialogContentText>
            </DialogContent>
          )
        }
      </div>
    );
  }
}

export default withProgress([
  LOGGED_IN_USER_WITH_ROLE,
  AUTHENTICATE_USER_MUTATION,
  USER_RESET_PASS_SEND_EMAIL_MUTATION,
])(DialogInner);
