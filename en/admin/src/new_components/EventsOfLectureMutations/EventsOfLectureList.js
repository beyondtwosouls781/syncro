import React, { Component } from 'react';

import { formatInputDataArrayForTable } from './../../new_utils';

import EventsList from './../EventOfLectureNoMutations/EventsList';

const EventsOfLectureList = (props) => {
  const {
    gqlQueryName,
    queryResultObjectName,
    gqlMutationName,
    onEdit,
    onDelete,
  } = props;

  let srcData = null;
  let data = null;

  if (props[gqlQueryName] && props[gqlQueryName][queryResultObjectName]) {
    srcData = props[gqlQueryName][queryResultObjectName];
    data = formatInputDataArrayForTable(srcData);
  }

  return (
    <EventsList
      data={data || []}
      srcData={srcData || []}
      onEdit={onEdit}
      onDelete={onDelete}
    />
  );
};

export default EventsOfLectureList;
