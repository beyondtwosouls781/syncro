import React, { Component } from 'react';
import Button from 'material-ui/Button';
import { graphql, compose } from 'react-apollo';

import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';

import DialogDeleteItem from './../DialogDeleteItem';

class DeleteEventDialog extends Component {
  constructor(props) {
    super(props);
    const {
      gqlMutationName,
    } = props;

    this.state = {
      isRequested: false,
      isSuccess: false,
    };
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
    this.delete = this.delete.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    const {
      gqlMutationName,
      mutationResultObjectName,
    } = nextProps;
    const gqlMutationDeleteResult = `${gqlMutationName}Result`;
    const gqlMutationDeleteError = `${gqlMutationName}Error`;

    if (nextProps[gqlMutationDeleteResult]) {
      const { id } = nextProps[gqlMutationDeleteResult].data[mutationResultObjectName];
      this.props.snackbar({ message: `Событие удалено - id: ${id}`, name: 'Success' });
      this.setState({
        id: null,
        isRequested: false,
        isSuccess: true,
      });
    }
    if (nextProps[gqlMutationDeleteError]) {
      this.props.snackbar(nextProps[gqlMutationDeleteError]);
      this.setState({
        isRequested: false,
      });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.isSuccess) {
      this.props.onConfirm();
    }
  }
  handleDeleteConfirm() {
    this.setState({
      id: this.props.data.id,
      isRequested: true,
    }, this.delete);
  }
  delete() {
    const {
      gqlMutationName,
    } = this.props;

    this.props[gqlMutationName]({
      variables: {
        id: this.state.id,
      },
    });
  }
  render() {
    const {
      open,
      itemToDelete,
      title,
      data,
      isMutation,
      onCancel,
    } = this.props;
    const {
      isRequested
    } = this.state;

    return (
      <div>
        <DialogDeleteItem
          open={open}
          title={title}
          onCancel={onCancel}
          onConfirm={this.handleDeleteConfirm}
          disabled={isRequested}
        />
      </div>
    );
  }
}

// const DeleteEventDialog = (props) => {
//
// };

DeleteEventDialog.defaultProps = {
  onClose() {

  },
  onSubmit() {

  },
  open: false,
};

export default DeleteEventDialog;
