import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';

import { EDIT_ALIAS_MUTATION } from './../../constants';
import aliasGql from './../../new_pages/Alias/gql';

import { formatInputDataForForm, formatOutputFormData } from './../../new_utils';

import BasicForm from './../BasicForm';

class FormEntity extends Component {
  constructor(props) {
    super(props);
    const {
      gqlQueryName,
      gqlMutationName,
      queryResultObjectName,
      data,
    } = props;

    let formatedData = null;
    let srcData = null;
    if (data) {
      formatedData = data;
    }
    if (props[gqlQueryName] && props[gqlQueryName][queryResultObjectName]) {
      srcData = props[gqlQueryName][queryResultObjectName]
      formatedData = formatInputDataForForm(srcData);
    }

    this.state = {
      data: formatedData,
      srcData,
      isRequested: gqlQueryName ? true : false,
      formData: null,
      gqlMutationNameResult: `${gqlMutationName}Result`,
      gqlMutationNameError: `${gqlMutationName}Error`,
      gqlMutationNameLoading: `${gqlMutationName}Loading`,
      gqlMutationNameReset: `${gqlMutationName}Reset`,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    // this.confirm = this.confirm.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {
      gqlMutationNameResult,
      gqlMutationNameError,
      gqlMutationNameLoading,
      gqlMutationNameReset,
    } = this.state;

    const {
      gqlQueryName,
      queryResultObjectName,
      mutationResultObjectName,
      resultMutationMessageSuccses,
      modifyQueryFilter,
      data,
      onSubmitMutation,
    } = nextProps;

    let isRequested = false;
    let formatedData = {};
    let srcData = {};
    if (data) {
      formatedData = data;
    }
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName].loading) {
      isRequested = true;
    }
    if (nextProps[gqlQueryName] && nextProps[gqlQueryName].error) {
      isRequested = true;
      this.props.snackbar(nextProps[gqlQueryName].error);
      this.setState({
        isRequested: false,
      });
      return false;
    }

    if (nextProps[gqlQueryName] && nextProps[gqlQueryName][queryResultObjectName]) {
      isRequested = false;
      srcData = nextProps[gqlQueryName][queryResultObjectName];
      formatedData = formatInputDataForForm(srcData);
    }

    if (nextProps[gqlMutationNameLoading]) {
      isRequested = true;
    }

    if (nextProps[gqlMutationNameResult]) {
      const result = nextProps[gqlMutationNameResult].data[mutationResultObjectName];
      this.props.snackbar({ message: `${resultMutationMessageSuccses} - id: ${result.id}`, name: 'Success' });
      this.setState({
        isRequested: false,
      }, () => {
        if (onSubmitMutation instanceof Function) {
          onSubmitMutation(result);
        }
      });

      return false;
    }

    if (nextProps[gqlMutationNameError]) {
      this.props.snackbar(nextProps[gqlMutationNameError]);
      this.props[gqlMutationNameReset]();
      this.setState({
        isRequested: false,
      });
      return false;
    }

    this.setState({
      isRequested,
      data: formatedData,
      srcData,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      gqlMutationNameResult,
    } = this.state;
    const {
      mutationResultObjectName,
      onSuccessMutation,
    } = this.props;

    if (this.props[gqlMutationNameResult]
      && this.props[gqlMutationNameResult].data
      && onSuccessMutation instanceof Function
    ) {
      onSuccessMutation(this.props[gqlMutationNameResult].data[mutationResultObjectName], this.props);
    }
  }

  handleSubmit(formData) {
    this.setState({
      isRequested: true,
      formData,
    }, () => {
      const { dontSavePassResultTo } = this.props;
      if (dontSavePassResultTo instanceof Function) {
        dontSavePassResultTo(formData);
      } else {
        this.confirm();
      }
    });
  }

  confirm() {
    const { formData } = this.state;
    const {
      gqlMutationName,
      fields,
      gqlQueryName,
      queryResultObjectName,
      client,
    } = this.props;
    const data = formatOutputFormData(formData, fields);

    if (
      data.alias
      && data.alias.alias
      && gqlQueryName
      && queryResultObjectName
      && this.props[gqlQueryName][queryResultObjectName].alias.id
    ) {
      const aliasId = this.props[gqlQueryName][queryResultObjectName].alias.id;
      this.props.client.mutate({
        mutation: aliasGql[EDIT_ALIAS_MUTATION],
        variables: {
          id: aliasId,
          alias: data.alias.alias,
        }
      })
        .then(({ errors }) => {
          this.props[gqlMutationName]({
            variables: data,
          });
        })
        .catch((err) => {
          this.props.snackbar(err);
        });

    } else {
      this.props[gqlMutationName]({
        variables: data,
      });
    }
  }

  render() {
    const {
      columnData,
      gqlQueryName,
      queryResultObjectName,
      fields,
      formTitle,
      actions,
      snackbar,
    } = this.props;

    const {
      isRequested,
      data,
      formData,
      srcData,
    } = this.state;

    return (
      <div style={{ marginBottom: 20 }}>
        <BasicForm
          snackbar={snackbar}
          formTitle={formTitle}
          fields={fields}
          data={formData || data}
          srcData={srcData}
          disabledAllInputs={isRequested}
          disabledAllActions={isRequested}
          actions={actions || ['undo', 'save']}
          onSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

export default withApollo(FormEntity);
