import React, { Component } from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';

import fields from './fields';
import columnData from './columnData';
import formatEventDataForForm from './formatEventDataForForm';

import FormEntity from './../MutationEntities/FormEntity';

const EventMutationDialog = (props) => {
  const {
    open,
    onClose,
    onSubmit,
    onSubmitMutation,
    lecturers,
    location,
    price,
    quantityOfTickets,
    lectureId,
    curator,
    data,
    isMutation,
  } = props;

  let title = '';
  let currentFileds = null;

  if (data && data.id) {
    title = 'Редактирование мероприятия';
    currentFileds = fields.update;
  } else {
    title = 'Создание нового мероприятия';
    currentFileds = fields.create;
  }

  return (
    <div>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Можно задать лектора, локацию, цену, количество билетов для конкретной даты.
          </DialogContentText>
          <FormEntity
            columnData={columnData}
            fields={currentFileds}
            dontSavePassResultTo={onSubmit}
            data={
              Object.assign(
                {},
                {
                  lecturers,
                  location,
                  curator,
                  price,
                  quantityOfTickets,
                  lectureId,
                  date: '',
                  time: '',
                },
                formatEventDataForForm(data),
              )
            }
          />

        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Отменить
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

EventMutationDialog.defaultProps = {
  onClose() {

  },
  onSubmit() {

  },
  open: false,
};

export default EventMutationDialog;
