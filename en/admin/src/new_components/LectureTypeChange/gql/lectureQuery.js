import gql from 'graphql-tag';

export default gql`
  query lectureQuery($id: ID!) {
    Lecture(
      id: $id,
    ) {
      id
      lectureType,
    }
  }
`;
