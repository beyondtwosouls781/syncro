import React, { Component } from 'react';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Slide from 'material-ui/transitions/Slide';

import DialogContentInner from './DialogContentInner';

const Transition = props => <Slide direction="up" {...props} />;

class MutationEntityInDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
    this.handleShowDialog = this.handleShowDialog.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleSuccsessMutation = this.handleSuccsessMutation.bind(this);
  }
  handleShowDialog() {
    this.setState({
      open: true,
    });
  }
  handleCloseDialog() {
    this.setState({
      open: false,
    });
  }
  handleSuccsessMutation() {
    this.setState({
      open: false,
    });
  }
  render() {
    const {
      title,
      subTitle,
      gql,
      gqlMutationName,
      mutationResultObjectName,
      resultMutationMessageSuccses,
      fields,
      formTitle,
      modifyMutationOptions,
    } = this.props;

    const {
      open,
    } = this.state;

    return (
      <div style={{ marginTop: 40 }}>
        {
          title &&
          <Typography type="title" gutterBottom>
            {title}
          </Typography>
        }
        {
          subTitle &&
          <Typography type="body1" gutterBottom>
            {subTitle}
          </Typography>
        }
        <Button
          raised
          dense
          color="primary"
          onClick={this.handleShowDialog}
        >
          Создать
        </Button>
        <Dialog
          fullScreen
          open={open}
          onClose={this.handleCloseDialog}
          transition={Transition}
        >
          <DialogTitle>{title || ''}</DialogTitle>
          <DialogContent>
            <DialogContentInner
              gql={gql}
              gqlMutationName={gqlMutationName}
              mutationResultObjectName={mutationResultObjectName}
              resultMutationMessageSuccses={resultMutationMessageSuccses}
              fields={fields}
              formTitle={formTitle}
              onSuccessMutation={this.handleSuccsessMutation}
              modifyMutationOptions={modifyMutationOptions}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCloseDialog} color="primary">
              Отменить
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default MutationEntityInDialog;
