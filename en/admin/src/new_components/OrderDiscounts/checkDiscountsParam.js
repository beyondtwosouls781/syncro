const checkValid = (lectures, product, discount) => {
  const { tags } = product || lectures[0].src || [];
  const tagsIds = tags.map(({ id }) => id);
  let result = false;

  if (discount.tags && discount.tags.length > 0) {
    result = discount.tags.some(({ id }) => tagsIds.includes(id));
    if (result) return true;
  }

  if (product && product.lectures.length === lectures.length) {
    // if (product._)
    if (discount.courses && discount.courses.length > 0) {
      result = discount.courses.some(({ id }) => id === product.id);
      if (result) return true;
    }
    if (discount.cycles && discount.cycles.length > 0) {
      result = discount.cycles.some(({ id }) => id === product.id);
      if (result) return true;
    }
  } else {
    const lecturesIds = lectures.map(({ id }) => id);
    result = discount.lectures.some(({ id }) => lecturesIds.includes(id));
  }

  return result;
};

const checkDiscountsParam = (props, { events }, discounts) => {
  const {
    courses,
    cycles,
    lectures,
    participants,
    certificates,
    subscriptions,
  } = props;

  return discounts.map((item) => {
    const discount = item.src ? item.src : item;
    const {
      buyingLecturesCount,
      participantsCount,
      lectures: discountLectures,
      courses: discoutCourses,
      cycles: discountCycles,
      tags: discountTags,
      certificates,
      subscriptions,
    } = discount;

    let participantsCountValid = false;
    let buyingLecturesCountValid = false;
    let productCheckValid = true;
    let mainProduct = null;

    if (
      buyingLecturesCount === -1
      || (participants.length >= buyingLecturesCount)
    ) participantsCountValid = true;

    if (
      buyingLecturesCount === -1
      || (events.length > 0
      && (events.length * participants.length) >= buyingLecturesCount)
    ) buyingLecturesCountValid = true;

    if (courses && courses.length > 0) mainProduct = courses[0].src;
    if (cycles && cycles.length > 0) mainProduct = cycles[0].src;


    switch (true) {
      case discount.allProducts: {
        productCheckValid = true;
        break;
      }
      case (
        (courses && courses.length > 0)
        || (cycles && cycles.length > 0)
        || (lectures && lectures.length > 0)
      ): {
        productCheckValid = checkValid(lectures, mainProduct, discount);
        break;
      }

      default: {
        productCheckValid = true;
      }
    }

    return Object.assign(
      {},
      discount,
      {
        valid: Boolean(buyingLecturesCount && participantsCountValid && productCheckValid),
      },
    );
  });
};

export default checkDiscountsParam;
