import React, { Component } from 'react';
// import Typography from 'material-ui/Typography';

import SelectRelatedItems from './../SelectRelatedItems';
// import gql from './../../new_pages/Discounts/gql';
import gql from './gql';
import {
  // ALL_DISCOUNTS_RELAIONS_QUERY,
  ALL_DISCOUNTS_OF_CERTIFICATE,
  ALL_DISCOUNTS_OF_SUBSCRIPTION,
  ALL_DISCOUNTS_OF_LECTURES,
} from './../../constants';
import columnDataShort from './../../new_pages/Discounts/columnDataShort';
import fields from './../../new_pages/Discounts/fields';
// import modifyQueryFilter from './../../new_pages/Discounts/modifyQueryFilter';
import modifyQueryFilter from './modifyQueryFilter';
import validateDiscounts from './validateDiscounts';

const SelectAdditionalDiscounts = (props) => {
  const {
    additionalDiscounts,
    onSelectAdditionalDiscounts,
    type,
  } = props;

  let gqlQueryName = '';
  let currentModifyQueryFilter = null;

  switch (type) {
    case 'certificate': {
      gqlQueryName = ALL_DISCOUNTS_OF_CERTIFICATE;
      currentModifyQueryFilter = modifyQueryFilter.certificate;
      break;
    }
    case 'subscription': {
      gqlQueryName = ALL_DISCOUNTS_OF_SUBSCRIPTION;
      currentModifyQueryFilter = modifyQueryFilter.subscription;
      break;
    }
    default: {
      gqlQueryName = ALL_DISCOUNTS_OF_LECTURES;
      currentModifyQueryFilter = modifyQueryFilter.lectures;
    }
  }

  return (
    <div style={{ marginTop: 20 }} >
      <SelectRelatedItems
        srcData={additionalDiscounts}
        relationTitle="Выбрать дополнительную скидку"
        tableTitle="Скидки"
        gql={gql}
        // gqlQueryName={ALL_DISCOUNTS_RELAIONS_QUERY}
        gqlQueryName={gqlQueryName}
        queryResultObjectName="allDiscounts"
        onChange={(selectedIds, selected) => { onSelectAdditionalDiscounts(selectedIds, validateDiscounts(props, selected)) }}
        columnData={columnDataShort}
        filter={fields.filterShort}
        // modifyQueryFilter={modifyQueryFilter}
        modifyQueryFilter={currentModifyQueryFilter(props)}
      />
    </div>
  );
};

export default SelectAdditionalDiscounts;
