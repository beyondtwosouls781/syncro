const XLSX = require('xlsx');

const sheetToWorkbook = (sheet, opts) => {
	const n = opts && opts.sheet ? opts.sheet : 'Sheet1';
	const sheets = {};
	sheets[n] = sheet;
	return { SheetNames: [n], Sheets: sheets };
};

const aoaToWorkbook = (data, opts) => sheetToWorkbook(XLSX.utils.aoa_to_sheet(data, opts), opts);

module.exports = (aoa, filename, done) => aoaToWorkbook(aoa, { cellDates: true });
