const fields = [
  {
    id: 'id',
    label: 'id',
    type: 'multiline',
    required: true,
    disabled: true,
  },
  {
    id: 'lectureType',
    label: 'Тип лекции',
    type: 'select',
    required: true,
    enumValues: [
      {
        value: 'SINGLE_LECTURE',
        label: 'Одиночная лекция',
      },
      {
        value: 'LECTURE_FOR_COURSE',
        label: 'Лекция для курса',
      },
      {
        value: 'LECTURE_FOR_CYCLE',
        label: 'Лекция для цикла',
      }
    ]
  }
];

export default fields;
