import {
  LECTURE_QUERY,
  EDIT_LECTURE_MUTATION,
} from './../../../constants';

import lectureQuery from './lectureQuery';
import updateLectureMutation from './updateLectureTypeMutation';

const gql = {
  [LECTURE_QUERY]: lectureQuery,
  [EDIT_LECTURE_MUTATION]: updateLectureMutation,
};

export default gql;
