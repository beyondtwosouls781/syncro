import React, { Component } from 'react';
import Button from 'material-ui/Button';
import { graphql, compose } from 'react-apollo';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';

import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';

import DialogDeleteItem from './../DialogDeleteItem';

import {
  DELETE_EVENT_MUTATION,
} from './../../constants';

import gql from './gql';

const gqlMutationDeleteResult = `${DELETE_EVENT_MUTATION}Result`;
const gqlMutationDeleteError = `${DELETE_EVENT_MUTATION}Error`;

class DeleteEventDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      isRequested: false,
      isSuccess: false,
    };
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
    this.delete = this.delete.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[gqlMutationDeleteResult]) {
      const id = nextProps[gqlMutationDeleteResult].data.deleteEvent.id
      this.props.snackbar({ message: `Событие удалено - id: ${id}`, name: 'Success' });
      this.setState({
        id: null,
        isRequested: false,
        isSuccess: true,
      });
    }
    if (nextProps[gqlMutationDeleteError]) {
      this.props.snackbar(nextProps[gqlMutationDeleteError]);
      this.setState({
        isRequested: false,
      });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.isSuccess) {
      this.props.onConfirm();
    }
  }
  handleDeleteConfirm() {
    this.setState({
      id: this.props.data.id,
      isRequested: true,
    }, this.delete);
  }
  delete() {
    this.props[DELETE_EVENT_MUTATION]({
      variables: {
        id: this.state.id,
      },
    });
  }
  render() {
    const {
      open,
      itemToDelete,
      title,
      data,
      isMutation,
      onCancel,
    } = this.props;
    const {
      isRequested
    } = this.state;

    return (
      <div>
        <DialogDeleteItem
          open={open}
          title={title}
          onCancel={onCancel}
          onConfirm={this.handleDeleteConfirm}
          disabled={isRequested}
        />
      </div>
    );
  }
}

// const DeleteEventDialog = (props) => {
//
// };

DeleteEventDialog.defaultProps = {
  onClose() {

  },
  onSubmit() {

  },
  open: false,
};

export default compose(
  graphql(gql[DELETE_EVENT_MUTATION], { name: DELETE_EVENT_MUTATION }),
  withStateMutation({ name: DELETE_EVENT_MUTATION }),
  withSnackbar(),
)(DeleteEventDialog);
