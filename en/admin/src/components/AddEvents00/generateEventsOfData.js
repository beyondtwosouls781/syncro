import moment from 'moment';

export default events => events.map((event) => {

  if (event.time) {
    return event;
  }
  const {
    id,
    date,
    lecture,
    lecturers,
    location,
    curator,
    price,
    quantityOfTickets,
  } = event;
  const eventDate = moment(date);
  return {
    id,
    date: eventDate.format('YYYY-MM-DD'),
    time: eventDate.format('HH:mm'),
    lectureId: lecture.id,
    lecturersIds: lecturers.map(lecturer => lecturer.id),
    lecturersIds_title: lecturers.map(lecturer => `${lecturer.firstName} ${lecturer.lastName}`),
    locationId: location.id,
    locationId_title: [location.title],
    curatorId: curator.id,
    curatorId_title: `${curator.firstName} ${curator.lastName}`,
    price,
    quantityOfTickets,
  };
});
