import React from 'react';

import ExtTableBase from './../ExtTableBase';
import {
  lecturesAll as columnDataLectures,
  courses as columnDataCourses,
  cycles as columnDataCycles,
} from './../../pages/Catalog/columnData';

import Tags from './../../pages/Tags/Tags';
import Lecturers from './../../pages/Lecturers/Lecturers';
import Locations from './../../pages/Locations/Locations';
import Catalog from './../../pages/Catalog/Catalog';
// import Events from './../../pages/Events/Events';
import AddEvents from './../AddEvents';
import SelectAllQueryShort from './../SelectAllQueryShort';

const ExtFormRelationAddingItem = (props) => {
  const {
    field,
    values,
    mutationType,
    onSelect,
    onChangeNoEvents,
  } = props;
  const {
    label,
    relation,
    id,
    multiple,
  } = field;

  switch (relation) {
    case 'events': {
      return (
        <div>
          <AddEvents
            data={values}
            onChange={onChangeNoEvents(id)}
            onSelect={onSelect(id)}
            lectureId={values.lectureId || values.id}
            lecturersIds={values.lecturersIds}
            locationId={values.locationId}
            price={values.price}
            quantityOfTickets={values.quantityOfTickets}
            events={values.eventsIds}
            field={field}
          />
        </div>
      );
    }
    case 'curator-select': {
      return (
        <div>
          <SelectAllQueryShort
            typeMaterial="curator"
            typeView="select"
            onChange={onChangeNoEvents(id)}
            ids={values[id]}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'lecturers-select': {
      return (
        <div>
          <SelectAllQueryShort
            typeMaterial="lecturers"
            typeView="select"
            onChange={onChangeNoEvents(id)}
            ids={values[id]}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'locations-select': {
      return (
        <div>
          <SelectAllQueryShort
            typeMaterial="locations"
            typeView="select"
            onChange={onChangeNoEvents(id)}
            ids={values[id]}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'tags-select': {
      return (
        <div>
          <SelectAllQueryShort
            typeMaterial="tags"
            typeView="select"
            onChange={onChangeNoEvents(id)}
            ids={values[id]}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'discounts-select': {
      return (
        <div>
          <SelectAllQueryShort
            typeMaterial="discounts"
            typeView="select"
            onChange={onChangeNoEvents(id)}
            ids={values[id]}
            multiple={multiple}
          />
        </div>
      );
    }
    case 'acquiring-select': {
      return (
        <div>
          <SelectAllQueryShort
            typeMaterial="acquiring"
            typeView="select"
            onChange={onChangeNoEvents(id)}
            ids={values[id]}
          />
        </div>
      );
    }
    case 'lectures': {
      return (
        <div>
          <Catalog
            {...props}
            title={''}
            typeMaterial={'lecture'}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'courses': {
      return (
        <div>
          <Catalog
            {...props}
            title={''}
            typeMaterial={'course'}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'cycles': {
      return (
        <div>
          <Catalog
            {...props}
            title={''}
            typeMaterial={'cycle'}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'subscriptions': {
      return (
        <div>
          <Catalog
            {...props}
            title={''}
            typeMaterial={'subscription'}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'certificates': {
      return (
        <div>
          <Catalog
            {...props}
            title={''}
            typeMaterial={'certificate'}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'tags': {
      return (
        <div>
          <Tags
            {...props}
            title={''}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'lecturers': {
      return (
        <div>
          <Lecturers
            {...props}
            title={''}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'locations': {
      return (
        <div>
          <Locations
            {...props}
            title={''}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'recommendedCourses': {
      return (
        <div>
          <Catalog
            {...props}
            title={''}
            typeMaterial={'course'}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'recommendedCycles': {
      return (
        <div>
          <Catalog
            {...props}
            title={''}
            typeMaterial={'cycle'}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    case 'recommendedLectures': {
      return (
        <div>
          <Catalog
            {...props}
            title={''}
            typeMaterial={'lecture'}
            showActions={false}
            onSelect={onSelect(id)}
            selected={values[id]}
          />
        </div>
      );
    }
    default: {
      return (
        <div>Нет связанных компонентов</div>
      );
    }
  }
};

ExtFormRelationAddingItem.defaultProps = {
  onChange() {},
  onChangeNoEvents() {},
  onSubmit() {},
  onSelect() {},
};

export default ExtFormRelationAddingItem;
