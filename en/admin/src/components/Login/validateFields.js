import validator from 'validator';

const hasOwnProperty = (obj, property) => Object.prototype.hasOwnProperty.call(obj, property);

const locale = 'ru-RU';

const validateFields = (input) => {
  const errors = [];
  const email = input.email;
  const password = input.password;
  const phone = input.phone;
  const firstName = input.firstName;
  const lastName = input.lastName;

  if (email && (validator.isEmpty(email) || !validator.isEmail(email))) {
    errors.push('email');
  }

  if (password && (validator.isEmpty(password) || !validator.isLength(password, { min: 8 }))) {
    errors.push('password');
  }

  if (phone && (validator.isEmpty(phone) || !validator.isMobilePhone(phone, locale))) {
    errors.push('phone');
  }

  if (firstName && (validator.isEmpty(firstName) || !validator.isAlpha(firstName, locale))) {
    errors.push('firstName');
  }

  if (lastName && (validator.isEmpty(lastName) || !validator.isAlpha(lastName, locale))) {
    errors.push('lastName');
  }

  return errors;
};

export default validateFields;
