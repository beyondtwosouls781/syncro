import React from 'react';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';

import { AUTHENTICATE_USER_MUTATION } from './../../constants';
import withProgress from './../../hoc/withProgress';

const DialogInner = ({ fields, open, classes, onChange, onSubmit, disabled }) => (
  <form
    noValidate
    autoComplete="off"
    onSubmit={onSubmit}
  >
    <DialogTitle>Авторизация</DialogTitle>
    <DialogContent>
      <TextField
        autoFocus
        margin="dense"
        id="email"
        label="Email"
        className={classes.textField}
        type="email"
        fullWidth
        onChange={onChange('email')}
        value={fields.email}
        error={fields.errors.indexOf('email') !== -1}
        disabled={disabled}
      />
      <TextField
        id="password"
        label="Пароль"
        className={classes.textField}
        type="password"
        autoComplete="current-password"
        margin="normal"
        fullWidth
        onChange={onChange('password')}
        value={fields.password}
        error={fields.errors.indexOf('password') !== -1}
        disabled={disabled}
      />
    </DialogContent>
    <DialogActions>
      <Button
        type="submit"
        onClick={onSubmit}
        color="primary"
        disabled={disabled}
      >
        Войти
      </Button>
    </DialogActions>
  </form>
);

DialogInner.displayName = 'DialogInner';
const DialogInnerWithProgress = withProgress(DialogInner, [AUTHENTICATE_USER_MUTATION]);

const SignInDialog = props => (
  <Dialog open={props.open}>
    <DialogInnerWithProgress
      {...props}
    />
  </Dialog>
);

SignInDialog.defaultProps = {
  classes: {},
  onChange: () => {},
};

SignInDialog.displayName = 'SignInDialog';

export default SignInDialog;
