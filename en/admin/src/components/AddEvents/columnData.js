import { format as formatUtil } from './../../utils';

const columnData = [
  {
    id: 'date',
    numeric: false,
    disablePadding: false,
    label: 'Дата',
    format: formatUtil.date,
  },
  {
    id: 'time',
    numeric: false,
    disablePadding: false,
    label: 'Время',
    // format: formatUtil.time,
  },
  {
    id: 'lecturers',
    numeric: false,
    disablePadding: false,
    label: 'Лекторы',
    // format: formatUtil.lecturersToSting,
    format: input => input.join('\n')
  },
  {
    id: 'location',
    numeric: false,
    disablePadding: false,
    label: 'Локация',
    // format: formatUtil.locationToSting,
    format: input => input.join('\n')
  },
  {
    id: 'curator',
    numeric: false,
    disablePadding: false,
    label: 'Координатор',
  },
  {
    id: 'price',
    numeric: false,
    disablePadding: false,
    label: 'Цена'
  },
  {
    id: 'quantityOfTickets',
    numeric: false,
    disablePadding: false,
    label: 'Количество билетов'
  }
];
export default columnData;
