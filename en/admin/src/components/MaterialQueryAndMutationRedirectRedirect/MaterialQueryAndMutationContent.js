import React from 'react';
import withProgress from './../../hoc/withProgress';

import MutationMaterial from './../MutationMaterial';

const ProgressComponent = props => <div />;

const MaterialQueryAndMutationContent = (props) => {
  const {
    gqlAllQueryName,
    gqlAllQueryItemsName,
    fields,
  } = props;
  const data = props[gqlAllQueryName][gqlAllQueryItemsName];
  const Progress = withProgress([gqlAllQueryName])(ProgressComponent);
  if (Array.isArray(data)) {
    if (data.length > 0) {
      return (
        <MutationMaterial
          {...props}
          fields={fields.update}
          resulMutatiomtMessageSuccses={'Обновлено'}
        />
      );
    }
    return (
      <MutationMaterial
        {...props}
        gqlQueryName={null}
        fields={fields.create}
        resulMutatiomtMessageSuccses={'Создано'}
      />
    );
  }

  return (
    <Progress {...props} />
  );
  // return (
  //   <div>
  //     Content
  //   </div>
  // );
};

export default MaterialQueryAndMutationContent;
