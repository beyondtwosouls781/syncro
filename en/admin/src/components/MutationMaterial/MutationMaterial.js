import React from 'react';

import MutationMaterialContent from './MutationMaterialContent';
import MaterialMutationContainer from './../../containers/MaterialMutationContainer';

const MutationMaterial = (props) => {
  return (
    <div>
      <MaterialMutationContainer
        {...props}
        component={MutationMaterialContent}
      />
    </div>
  );
};

export default MutationMaterial;
