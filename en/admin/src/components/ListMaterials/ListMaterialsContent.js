import React, { Component } from 'react';

import FilterQuery from './../FilterQuery';
import ExtTableBase from './../ExtTableBase';
import withProgress from './../../hoc/withProgress';
import DialogDeleteItem from './../DialogDeleteItem';

import withSnackbar from './../../hoc/withSnackbar';

const isRequest = (props) => {
  if (props[props.gqlAllQueryName].loading) {
    return true;
  }
  return false;
};

class ListMaterialsContent extends Component {
  constructor(props) {
    super(props);
    const { gqlMutationDelete } = this.props;
    this.state = {
      gqlMutationDeleteResult: `${gqlMutationDelete}Result`,
      gqlMutationDeleteError: `${gqlMutationDelete}Error`,
      gqlMutationDeleteLoading: `${gqlMutationDelete}Loading`,
      gqlMutationDeleteReset: `${gqlMutationDelete}Reset`,
      itemToDelete: null,
      isRequired: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleCancelDelete = this.handleCancelDelete.bind(this);
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const {
      gqlMutationDeleteResult,
      gqlMutationDeleteError,
      gqlMutationDeleteLoading,
      gqlMutationDeleteReset,
    } = this.state;

    const {
      resulMutatiomtMessageSuccses,
      gqlMutationDeleteItemName,
      gqlAllQueryName,
    } = this.props;

    if (nextProps[gqlMutationDeleteResult]) {
      const id = nextProps[gqlMutationDeleteResult].data[gqlMutationDeleteItemName].id
      this.props.snackbar({ message: `${resulMutatiomtMessageSuccses} - id: ${id}`, name: 'Success' });
      this.setState({
        isRequested: false,
        itemToDelete: null,
      }, this.props[gqlAllQueryName].refetch);
      return false;
    }
    if (nextProps[gqlMutationDeleteError]) {
      this.props.snackbar(nextProps[gqlMutationDeleteError]);
      this.props[gqlMutationDeleteReset]();
      this.setState({
        isRequested: false,
      });
      return false;
    }
  }
  handleSubmit(fields) {
    const {
      gqlAllQueryName,
    } = this.props;
    this.props[gqlAllQueryName].refetch({ filter: this.props.filterModifier(fields, this.props.filterType) });
  }
  handleChange(fields) {

  }
  handleDelete(n) {
    this.setState({
      itemToDelete: n,
    });
  }
  handleCancelDelete() {
    this.setState({
      itemToDelete: null,
    });
  }
  handleDeleteConfirm = async () => {
    const { itemToDelete } = this.state;
    const { gqlMutationDelete } = this.props;
    this.props[gqlMutationDelete]({
      variables: {
        id: itemToDelete.id,
      },
    });
  }
  render() {
    const {
      fields,
      gqlAllQueryName,
      gqlAllQueryItemsName,
      gqlMutationDelete,
      showActions,
    } = this.props;
    const {
      itemToDelete,
      isRequested: isRequestedMutation,
    } = this.state;
    const filterFileds = fields.filter;
    const isRequested = isRequest(this.props);
    const data = this.props[gqlAllQueryName][gqlAllQueryItemsName];
    const Table = withProgress([gqlAllQueryName, gqlMutationDelete])(ExtTableBase);

    return (
      <div>
        <FilterQuery
          fields={filterFileds}
          isRequested={isRequested || isRequestedMutation}
          title={'Фильтр'}
          onChange={this.handleChange}
          onSubmit={this.handleSubmit}
        />
        <Table
          {...this.props}
          data={data}
          showActions={showActions}
          isRequested={isRequested || isRequestedMutation}
          onDelete={this.handleDelete}
        />
        <DialogDeleteItem
          open={itemToDelete ? true : false}
          title={itemToDelete ? itemToDelete.title : ''}
          onCancel={this.handleCancelDelete}
          onConfirm={this.handleDeleteConfirm}
          disabled={isRequested}
        />
      </div>
    );
  }
}

export default withSnackbar()(ListMaterialsContent);
