import gql from 'graphql-tag';

export default gql`
  query allLecturersQueryShort($filter:LecturerFilter) {
    allLecturers(filter: $filter) {
      id,
      firstName,
      lastName,
    }
  }
`;
