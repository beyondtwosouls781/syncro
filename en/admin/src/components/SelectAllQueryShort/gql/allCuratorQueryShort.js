import gql from 'graphql-tag';

export default gql`
  query allCuratorQueryShort {
    allUsers(filter: {
      role_in: [ADMIN, MODERATOR, CURATOR]
    }) {
      id
      
      firstName
      lastName
      email
    }
  }
`;
