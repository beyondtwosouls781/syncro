import gql from 'graphql-tag';

export default gql`
  query allAcquiringsQueryShort($filter:AcquiringFilter) {
    allAcquirings(filter: $filter) {
      id,
      title,
    }
  }
`;
