import gql from 'graphql-tag';

export default gql`
  query allDiscountsQueryShort($filter:DiscountFilter) {
    allDiscounts(filter: $filter) {
      id,
      title,
      rate,
      useCount,
    }
  }
`;
