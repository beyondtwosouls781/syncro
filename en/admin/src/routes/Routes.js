import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';

import { compose } from 'react-apollo';

import { GC_USER_ID } from './../constants';

import authAndPermission from './../new_hoc/authAndPermission';
import withAuthAndPermission from './../new_hoc/withAuthAndPermission';
import withWrapper from './../new_hoc/withWrapper';

import {
  Home,
  Login,
  NotFound,
  Materials,
  Others,
  Imports,
  Test,
} from './../pages';

import {
  Catalog,
  Content,
  Lecturers,
  Discounts,
  Certificates,
  Subscriptions,
  Locations,
  Tags,
  Events,
  Orders,
  Users,
  Reviews,
  Partners,
  Galleries,
  Acquirings,
  Messages,
  Settings,
  Payments,
  Schedule,
  Template,
} from './../new_pages';

const Routes = (props) => {
  // const userId = localStorage.getItem(GC_USER_ID);
  // if (!userId) return <Redirect to="/login" />;
  return (
    <Switch>
      <Route
        exact
        path="/"
        component={rest => authAndPermission({ ...props, ...rest })(<Home {...props} {...rest} />)}
      />
      <Route
        path="/login"
        component={rest => authAndPermission({ ...props, ...rest })(<Login {...props} {...rest} />)}
      />
      <Route
        path="/events"
        component={rest => authAndPermission({ ...props, ...rest })(<Events title="Мероприятия" {...props} {...rest} />)}
      />
      <Route
        path="/schedule"
        component={rest => authAndPermission({ ...props, ...rest })(<Schedule title="Расписание" {...props} {...rest} />)}
      />
      <Route
        path="/messages"
        component={rest => authAndPermission({ ...props, ...rest })(<Messages title="Уведомления" {...props} {...rest} />)}
      />
      <Route
        path="/catalog"
        component={rest => authAndPermission({ ...props, ...rest })(<Catalog title="Каталог" {...props} {...rest} />)}
      />
      <Route
        path="/content"
        component={rest => authAndPermission({ ...props, ...rest })(<Content title="Содержимое сайта" {...props} {...rest} />)}
      />
      <Route
        path="/reviews"
        component={rest => authAndPermission({ ...props, ...rest })(<Reviews title="Отзывы" {...props} {...rest} />)}
      />
      <Route
        path="/locations"
        component={rest => authAndPermission({ ...props, ...rest })(<Locations title="Локации" {...props} {...rest} />)}
      />
      <Route
        path="/certificates"
        component={rest => authAndPermission({ ...props, ...rest })(<Certificates title="Сертификаты" {...props} {...rest} />)}
      />
      <Route
        path="/subscriptions"
        component={rest => authAndPermission({ ...props, ...rest })(<Subscriptions title="Абонементы" {...props} {...rest} />)}
      />
      <Route
        path="/partners"
        component={rest => authAndPermission({ ...props, ...rest })(<Partners title="Партнеры" {...props} {...rest} />)}
      />
      <Route
        path="/users"
        component={rest => authAndPermission({ ...props, ...rest })(<Users title="Пользователи" {...props} {...rest} />)}
      />
      <Route
        path="/others"
        component={rest => authAndPermission({ ...props, ...rest })(<Others title="Прочее" {...props} {...rest} />)}
      />
      <Route
        path="/orders"
        component={rest => authAndPermission({ ...props, ...rest })(<Orders title="Заказы" {...props} {...rest} />)}
      />
      <Route
        path="/payments"
        component={rest => authAndPermission({ ...props, ...rest })(<Payments title="Платежи" {...props} {...rest} />)}
      />
      <Route
        path="/settings"
        component={rest => authAndPermission({ ...props, ...rest })(<Settings title="Настройки" {...props} {...rest} />)}
      />
      <Route
        path="/template"
        component={rest => authAndPermission({ ...props, ...rest })(<Template title="Шаблон" {...props} {...rest} />)}
      />

      <Route
        path="/tags"
        component={rest => authAndPermission({ ...props, ...rest })(<Tags title="Направления" {...props} {...rest} />)}
      />
      <Route
        path="/lecturers"
        component={rest => authAndPermission({ ...props, ...rest })(<Lecturers title="Лекторы" {...props} {...rest} />)}
      />
      <Route
        path="/discounts"
        component={rest => authAndPermission({ ...props, ...rest })(<Discounts title="Скидки" {...props} {...rest} />)}
      />
      <Route
        path="/acquirings"
        component={rest => authAndPermission({ ...props, ...rest })(<Acquirings title="Эквайринг" {...props} {...rest} />)}
      />
      <Route
        path="/galleries"
        component={rest => authAndPermission({ ...props, ...rest })(<Galleries title="Галереи" {...props} {...rest} />)}
      />

      <Route
        path="/imports/"
        component={rest => authAndPermission({ ...props, ...rest })(<Imports {...props} {...rest} />)}
      />

      <Route
        path="/test"
        component={rest => authAndPermission({ ...props, ...rest })(<Test {...props} {...rest} />)}
      />

      <Route
        path="/test/:id"
        component={rest => authAndPermission({ ...props, ...rest })(<Test {...props} {...rest} />)}
      />

      <Route component={rest => <NotFound {...props} {...rest} />} />
      {/* <Redirect to="/404" /> */}
    </Switch>
  );
};

export default compose(withWrapper, withAuthAndPermission)(Routes);
