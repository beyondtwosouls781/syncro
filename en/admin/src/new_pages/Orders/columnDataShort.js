const columnData = [
  {
    id: 'userFullNameEmail',
    label: 'Пользователь',
  },
  {
    id: 'discounts',
    label: 'Скидки',
  },
  {
    id: 'lecturesTitle',
    label: 'Лекции',
  },
  {
    id: 'dates',
    label: 'Даты',
  },
  // {
  //   id: 'paymentStatus',
  //   label: 'Оплата',
  // },
  {
    id: 'participants',
    label: 'Участники',
  },
  // {
  //   id: 'certificatesAndSubscriptions',
  //   label: 'Абонементы и сертификаты',
  // },
];
export default columnData;
