import gql from 'graphql-tag';

export default gql`
  query AllOrdersQuery($filter: OrderFilter, $first: Int, $skip: Int, $orderBy: OrderOrderBy) {
    allOrders(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      totalPrice
      events {
        id
        date
        lecture {
          id
          title
          course {
            id
            title
          }
          cycle {
            id
            title
          }
        }
      }
      participants {
        id
        firstName
        lastName
        email
      }
      payment {
        id
        type
        acquiring {
          id
          title
        }
      }
      user {
        id
        firstName
        lastName
        email
      }
    }
  }
`;
