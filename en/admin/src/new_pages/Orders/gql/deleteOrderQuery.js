import gql from 'graphql-tag';

export default gql`
  mutation deleteOrderMutation(
      $id: ID!
    ){
    deleteOrder(
      id: $id,
    ) {
      id
    }
  }
`;
