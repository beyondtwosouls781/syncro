import React from 'react';

import gql from './gql';

import {
  CREATE_ORDER_MUTATION,
  EDIT_ORDER_MUTATION,
  ORDER_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';
import certificatesFields from './fields';

import { CreateOrder as Order } from './../../new_components/Order';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const OrdersMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_ORDER_MUTATION;
    mutationResultObjectName = 'createOrder';
    resultMutationMessageSuccses = 'Заказ создан';
  } else {
    gqlMutationName = EDIT_ORDER_MUTATION;
    mutationResultObjectName = 'updateOrder';
    resultMutationMessageSuccses = 'Заказ обновлен';
    gqlQueryName = ORDER_QUERY;
    queryResultObjectName = 'Order';
  }

  return (
    <div>
      <Order
        {...props}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default OrdersMutation;
