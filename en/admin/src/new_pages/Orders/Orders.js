import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_ORDERS_QUERY,
  DELETE_ORDER_MUTATION,
} from './../../constants';

import ListEntities from './../../new_components/ListEntities';

import fields from './fields';
import actions from './actions';
import columnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';
import { tabsPayment } from './../../new_utils';

const Orders = (props) => {

  const gqlQueryName = ALL_ORDERS_QUERY;
  const queryResultObjectName = 'allOrders';
  const gqlMutationName = DELETE_ORDER_MUTATION;
  const mutationResultObjectName = 'deleteOrder';
  const resulMutationMessageSuccses = 'Заказ удален';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={actions}
        columnData={columnData}
        fields={fields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        resulMutationMessageSuccses={resulMutationMessageSuccses}
        modifyQueryFilter={modifyQueryFilter}
        tabs={tabsPayment}
      />
    </div>
  );
};

export default Orders;
