import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllDiscountsRelationsQuery($filter: DiscountFilter, $first: Int, $skip: Int, $orderBy: DiscountOrderBy) {
    allDiscounts(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...DiscountRelationsPayload
    }
  }
  ${fragments.discountReationsPayload}
`;
