import { PUBLIC } from './../../enum';

const create = [
  {
    id: 'title',
    label: 'Название',
    required: true,
    type: 'multiline',
  },
  {
    id: 'public',
    label: 'Опубликовано',
    defaultValue: false,
    type: 'checkbox',
    outputType: 'boolean',
  },
  {
    id: 'promocode',
    label: 'Промокод',
    type: 'multiline',
  },
  {
    id: 'rate',
    label: 'Величина скидки %',
    required: true,
    type: 'number',
    subType: 'fullWidth',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'validityPeriodFrom',
    label: 'Начало действия',
    required: true,
    type: 'date',
  },
  {
    id: 'validityPeriodTo',
    label: 'Окончания действия',
    required: true,
    type: 'date',
  },
  {
    id: 'useCount',
    label: 'Сколько раз можно использовать',
    helperText: '-1 - безлимитно',
    defaultValue: -1,
    type: 'number',
    outputType: 'number',
    subType: 'fullWidth',
    outputSubType: 'int',
  },
  {
    id: 'buyingLecturesCount',
    label: 'Количество лекций при которых начинает действовать скидка',
    defaultValue: 1,
    type: 'number',
    subType: 'fullWidth',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'participantsCount',
    label: 'Количество участников при которых начинает действовать скидка',
    defaultValue: 1,
    type: 'number',
    subType: 'fullWidth',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'allProducts',
    label: 'Действут на все материалы',
    type: 'checkbox',
    defaultValue: false,
    outputType: 'boolean',
  },
  {
    id: 'tags',
    label: 'Направления связанные со скидкой',
    defaultValue: [],
    disabled: true,
    relation: 'tags',
    type: 'relation',
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'lectures',
    label: 'Лекции связанные со скидкой',
    defaultValue: [],
    disabled: true,
    relation: 'lectures',
    type: 'relation',
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'courses',
    label: 'Курсы связанные со скидкой',
    defaultValue: [],
    disabled: true,
    relation: 'courses',
    type: 'relation',
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'cycles',
    label: 'Циклы связанные со скидкой',
    defaultValue: [],
    disabled: true,
    relation: 'cycles',
    type: 'relation',
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'subscriptionProducts',
    label: 'Типы Абонементов связанные со скидкой',
    defaultValue: [],
    disabled: true,
    relation: 'subscriptionProducts',
    type: 'relation',
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
  {
    id: 'certificateProducts',
    label: 'Типы Сертификатов связанные со скидкой',
    defaultValue: [],
    disabled: true,
    relation: 'certificateProducts',
    type: 'relation',
    outputType: 'relation',
    outputSubType: 'ids',
    multiple: true,
  },
];


const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const view = update.slice();
view.push(
  {
    id: 'unused',
    label: 'Неиспользовано',
  },
  {
    id: 'orders',
    label: 'Заказы связанные со скидкой',
    disabled: true,
    relation: 'orders',
    type: 'relation',
  },
);

const filter = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'promocode',
    label: 'Промокод',
  },
  {
    id: 'rate',
    label: 'Размер скидки',
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
];

const filterShort = filter.concat({
  id: 'public',
  label: 'Опубликовано',
  type: 'select',
  enum: PUBLIC,
  defaultValue: 'true',
});

export default {
  create,
  update,
  filter,
  filterShort,
  view,
};
