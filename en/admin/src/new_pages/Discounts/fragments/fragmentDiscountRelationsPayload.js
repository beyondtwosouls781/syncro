import gql from 'graphql-tag';

export default gql`
  fragment DiscountRelationsPayload on Discount {
    id
    title
    promocode
    rate
    allProducts
    buyingLecturesCount
    participantsCount
    useCount
    validityPeriodFrom
    validityPeriodTo
    certificateProducts {
      id
      title
    }
    subscriptionProducts {
      id
      title
    }
    lectures {
      id
      title
    }
    courses {
      id
      title
    }
    cycles {
      id
      title
    }
    tags {
      id
      title
    }
  }
`;
