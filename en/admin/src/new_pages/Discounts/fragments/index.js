import discountPayload from './fragmentDiscountPayload';
import discountShortPayload from './fragmentDiscountShortPayload';
import discountFullPayload from './fragmentDiscountFullPayload';
import discountReationsPayload from './fragmentDiscountRelationsPayload';
export default {
  discountPayload,
  discountShortPayload,
  discountFullPayload,
  discountReationsPayload,
};
