import { backHistory } from './../../new_utils';

export default (data, { history, match }) => {
  const { id } = data;
  const { url } = match;
  const updateUrl = url.replace(/create$/, `update/${id}`);
  history.push(updateUrl);
};
