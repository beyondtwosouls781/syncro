import {
  ALL_MAINPAGE_QUERY,
  CREATE_MAINPAGE_MUTATION,
  EDIT_MAINPAGE_MUTATION,
  MAINPAGE_QUERY,
} from './../../../../constants';

import allMainPageQuery from './allMainPageQuery';
import createMainPageMutation from './createMainPageMutation';
import updateMainPageMutation from './updateMainPageMutation';
import mainPageQuery from './mainPageQuery';

const gql = {
  [ALL_MAINPAGE_QUERY]: allMainPageQuery,
  [CREATE_MAINPAGE_MUTATION]: createMainPageMutation,
  [EDIT_MAINPAGE_MUTATION]: updateMainPageMutation,
  [MAINPAGE_QUERY]: mainPageQuery,
};

export default gql;
