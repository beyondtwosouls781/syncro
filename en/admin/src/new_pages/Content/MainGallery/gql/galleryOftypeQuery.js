import gql from 'graphql-tag';

export default gql`
  query GalleryOfTypeQuery($type: String!) {
    Gallery(
      type: $type,
    ) {
      id
    }
  }
`;
