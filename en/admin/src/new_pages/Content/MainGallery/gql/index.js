import {
  ALL_IMAGES_QUERY,
  DELETE_IMAGE_MUTATION,

  CREATE_IMAGE_MUTATION,
  UPDATE_IMAGE_MUTATION,
  IMAGE_QUERY,
  IMAGE_QUERY_WITH_RELATIONS,
  GALLERY_OF_TYPE_QUERY,
} from './../../../../constants';

import allImagesQuery from './allImagesQuery';
import deleteImageMutation from './deleteImageMutation';
import createImageMutation from './createImageMutation';
import updateImageMutation from './updateImageMutation';
import imageQuery from './imageQuery';
import imageQueryWithRelations from './imageQueryWithRelations';
import galleryOftypeQuery from './galleryOftypeQuery';

const gql = {
  [ALL_IMAGES_QUERY]: allImagesQuery,
  [DELETE_IMAGE_MUTATION]: deleteImageMutation,

  [CREATE_IMAGE_MUTATION]: createImageMutation,
  [UPDATE_IMAGE_MUTATION]: updateImageMutation,

  [IMAGE_QUERY]: imageQuery,
  [IMAGE_QUERY_WITH_RELATIONS]: imageQueryWithRelations,
  [GALLERY_OF_TYPE_QUERY]: galleryOftypeQuery,
};

export default gql;
