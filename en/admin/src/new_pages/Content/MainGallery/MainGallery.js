import React from 'react';

import gql from './gql';

import {
  ALL_IMAGES_QUERY,
  DELETE_IMAGE_MUTATION,
} from './../../../constants';

import ListEntities from './../../../new_components/ListEntities';

import mainGalleriesFields from './fields';
import mainGalleriesActions from './actions';
import mainGalleriesColumnData from './columnData';
import modifyQueryFilter from './modifyQueryFilter';

const MainGallery = (props) => {

  const gqlQueryName = ALL_IMAGES_QUERY;
  const queryResultObjectName = 'allImages';
  const gqlMutationName = DELETE_IMAGE_MUTATION;
  const mutationResultObjectName = 'deleteImage';
  const resulMutatiomtMessageSuccses = 'Изображение удалено';

  return (
    <div>
      <ListEntities
        {...props}
        actionsButtons={mainGalleriesActions}
        columnData={mainGalleriesColumnData}
        fields={mainGalleriesFields}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        modifyQueryFilter={modifyQueryFilter}
      />
    </div>
  );
};

export default MainGallery;
