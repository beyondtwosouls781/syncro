import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AboutPageQuery($id: ID!) {
    AboutPage(
      id: $id
    ) {
      ...AboutPagePayload
    }
  }
  ${fragments.aboutPagePayload}
`;
