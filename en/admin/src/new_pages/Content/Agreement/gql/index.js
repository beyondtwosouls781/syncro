import {
  AGREEMENT_QUERY,
  CREATE_AGREEMENT_MUTATION,
  EDIT_AGREEMENT_MUTATION,
} from './../../../../constants';

import agreementQuery from './agreementQuery';
import createAgreementMutation from './createAgreementMutation';
import updateAgreementMutation from './updateAgreementMutation';

const gql = {
  [AGREEMENT_QUERY]: agreementQuery,
  [CREATE_AGREEMENT_MUTATION]: createAgreementMutation,
  [EDIT_AGREEMENT_MUTATION]: updateAgreementMutation,
};

export default gql;
