import gql from 'graphql-tag';

export default gql`
  fragment SubscriptionPayload on Subscription {
    id
    createdAt
    updatedAt

    user {
      id
      firstName
      lastName
      email
    }
    draft
    title
    price
    lecturesCount
    unusedLectures
    start
    end
    code
    product {
      id
      title
      price
    }
    user {
      id
      firstName
      lastName
    }
    personsCount
    payment {
      id
      status
      type
      data
      additionalDiscounts {
        id
        title
        rate
        validityPeriodFrom,
        validityPeriodTo,
        useCount
        unused
        buyingLecturesCount
        participantsCount
        allProducts
        public
        certificateProducts {
          id
        }
      }
      discounts {
        id
        title
        rate
        validityPeriodFrom,
        validityPeriodTo,
        useCount
        unused
        buyingLecturesCount
        participantsCount
        allProducts
        public
        certificateProducts {
          id
        }
      }
      user {
        id
        firstName
        lastName
      }
    }
  }
`;
