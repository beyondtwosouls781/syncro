import gql from 'graphql-tag';

export default gql`
  fragment SubscriptionShortPayload on Subscription {
    id
    createdAt
    updatedAt

    user {
      id
      firstName
      lastName
    }
    draft
    title
    price
    lecturesCount
    unusedLectures
    start
    end
    personsCount
    code
    payment {
      id
      status
    }
  }
`;
