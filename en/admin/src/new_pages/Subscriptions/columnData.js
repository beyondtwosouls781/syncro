const columnData = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'userFullNameEmail',
    label: 'Пользователь',
  },
  {
    id: 'code',
    label: 'Промокод',
  },
  {
    id: 'start',
    label: 'Начало действия',
  },
  {
    id: 'end',
    label: 'Окончание действия',
  },
  {
    id: 'lecturesCount',
    label: 'Количество лекций',
  },
  {
    id: 'unusedLectures',
    label: 'Остаток лекций',
  },
  {
    id: 'payment',
    label: 'Оплата',
  },
];
export default columnData;
