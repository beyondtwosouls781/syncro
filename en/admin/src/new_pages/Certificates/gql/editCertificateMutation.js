import gql from 'graphql-tag';

export default gql`
  mutation UpdateCertificateMutation(
      $id: ID!,
      $userId: ID,
      $gift: Boolean!,
      $productId: ID!,

      $paymentId: ID!,
      $userIdPayment: ID!
      $totalPrice: Float!
      $orderPrice: Float!,
      $discountPrice: Float!,
      $type: PaymentsTypes!,
      $status: PaymentStatus!,
      $discountsIds: [ID!],
      $additionalDiscountsIds: [ID!],
      $data: Json!
    ){
    updateCertificate(
      id: $id,
      userId: $userId,
      gift: $gift
      productId: $productId
    ) {
      id,
    }
    updatePayment(
      id: $paymentId,
      orderPrice: $orderPrice,
      discountPrice: $discountPrice,
      totalPrice: $totalPrice,
      type: $type,
      userId: $userIdPayment,
      status: $status,
      discountsIds: $discountsIds,
      additionalDiscountsIds: $additionalDiscountsIds,
      data: $data,
    ) {
      id
    }
  }
`;
