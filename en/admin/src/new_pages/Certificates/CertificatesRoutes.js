import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Certificates from './Certificates';
import CertificatesMutation from './CertificatesMutation';
import CertificateMutationUpdate from './CertificateMutationUpdate';
import CertificateViewWithRelations from './CertificateViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../new_hoc/withWrapper';

const CertificatesRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Certificates
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <CertificatesMutation {...props} {...rest} mutationType="create" title={'Новый заказ'} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <CertificateMutationUpdate {...props} {...rest} mutationType="update" title={'Редактирование сертификатов'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <CertificateViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(CertificatesRoutes, { title: 'Сертификаты' });
export default CertificatesRoutes;
