import gql from 'graphql-tag';

export default gql`
  fragment CertificatePayload on Certificate {
    id
    createdAt
    updatedAt

    user {
      id
      firstName
      lastName
      email
    }
    draft
    gift
    title
    price
    lecturesCount
    unusedLectures
    start
    end
    discounts {
      id
      title
    }
    code
    product {
      id
      title
      public
      price
      lecturesCount
      months
      personsCount
      type
    }
    user {
      id
      firstName
      lastName
    }
    personsCount
    payment {
      id
      status
      type
      data
      additionalDiscounts {
        id
        title
        rate
        validityPeriodFrom,
        validityPeriodTo,
        useCount
        unused
        buyingLecturesCount
        participantsCount
        allProducts
        public
        certificateProducts {
          id
        }
      }
      discounts {
        id
        title
        rate
        validityPeriodFrom,
        validityPeriodTo,
        useCount
        unused
        buyingLecturesCount
        participantsCount
        allProducts
        public
        certificateProducts {
          id
        }
      }
      user {
        id
        firstName
        lastName
      }
    }
  }
`;
