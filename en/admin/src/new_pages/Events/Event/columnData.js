import CheckAttend from './CheckAttend';

const registeredParticipants = [
  {
    id: 'firstName',
    label: 'Имя',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
  },
  {
    id: 'email',
    label: 'E-mail',
  },
  {
    id: 'phone',
    label: 'Телефон',
  },
  {
    id: 'change',
    label: 'Перенос',
  },
  {
    id: 'paymentType',
    label: 'Тип оплаты',
  },
  {
    id: 'payment',
    label: 'Оплачен',
  },
  {
    id: 'orderId',
    label: 'ID заказа',
  },
];

const arrivingParticipants = [
  {
    id: 'component',
    component: CheckAttend,
  },
  {
    id: 'firstName',
    label: 'Имя',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
  },
  {
    id: 'email',
    label: 'E-mail',
  },
  {
    id: 'phone',
    label: 'Телефон',
  },
];

export default {
  registeredParticipants,
  arrivingParticipants,
};
