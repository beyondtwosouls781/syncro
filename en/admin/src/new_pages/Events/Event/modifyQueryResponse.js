const registeredParticipants = (input) => {
  let participants = [];
  const { orders } = input;
  if (orders && orders.length > 0) {
    participants = orders
      .map(({ id, participants: participantsOfOrder, payment }) => participantsOfOrder
        .map(participant => Object.assign({}, participant, { payment }, { order: { id } })))
      .reduce((prev, curr) => prev.concat(curr));
  }
  return participants;
};

export default {
  registeredParticipants,
};
