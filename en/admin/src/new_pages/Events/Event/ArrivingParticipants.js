import React, { Component } from 'react';
import Typography from 'material-ui/Typography';

import gql from './../gql';

import {
  TICKETS_OF_EVENT_QUERY,
} from './../../../constants';

import ListEntitiesQuery from './../../../new_components/ListEntitiesQuery';

import fields from './fields';
import columnData from './columnData';
import modifyQueryOptions from './modifyQueryOptions';
import modifyQueryFilter from './modifyQueryFilter';
import modifyQueryResponse from './modifyQueryResponse';
// import coloredRow from './coloredRow';

const ArrivingParticipants = (props) => {
  const gqlQueryName = TICKETS_OF_EVENT_QUERY;
  const queryResultObjectName = 'allTickets';

  return (
    <div>
      <ListEntitiesQuery
        {...props}
        columnData={columnData.arrivingParticipants}
        fields={fields.arrivingParticipants}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        modifyQueryFilter={modifyQueryFilter.arrivingParticipants}
        modifyQueryOptions={modifyQueryOptions.arrivingParticipants}
        // coloredRow={coloredRow.arrivingParticipants}
        // modifyQueryResponse={modifyQueryResponse.arrivingParticipants}
      />
    </div>
  );
};

export default ArrivingParticipants;
