import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';
import Checkbox from 'material-ui/Checkbox';

import withProgress from './../../../new_hoc/withProgress';
import withStateMutation from './../../../new_hoc/withStateMutation';
import withSnackbar from './../../../new_hoc/withSnackbar';

import {
  ATTENDED_USER_MUTATION,
} from './../../../constants';

import gql from './../gql';

const gqlMutationNameResult = `${ATTENDED_USER_MUTATION}Result`;
const gqlMutationNameError = `${ATTENDED_USER_MUTATION}Error`;
const gqlMutationNameLoading = `${ATTENDED_USER_MUTATION}Loading`;
const gqlMutationNameReset = `${ATTENDED_USER_MUTATION}Reset`;

class CheckAttend extends Component {
  constructor(props) {
    super(props);
    const {
      attended,
    } = props;
    this.state = {
      attended,
      isRequested: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.mutate = this.mutate.bind(this);
  }

  handleChange(value) {
    const { attended } = this.state;
    const { id } = this.props;
    this.setState({
      isRequested: true,
    }, () => {
      this.mutate(id, !attended);
    });
  }

  mutate(id, attended) {
    this.props[ATTENDED_USER_MUTATION](
      {
        variables: { id, attended },
      },
    )
      .then(({ data, errors }) => {
        const result = data.updateTicket;
        this.props.snackbar({ message: `${result.attended ? 'Отмечен' : 'Отметка снята'} - id: ${result.id}`, name: 'Success' });
      })
      .catch((err) => {
        this.props.snackbar(err);
        this.setState({
          isRequested: false,
        });
      });
  }

  render() {
    const {
      attended,
      isRequested,
    } = this.state;
    return (
      <Checkbox
        disabled={isRequested}
        checked={attended}
        onChange={this.handleChange}
      />
    );
  }
}

export default compose(
  graphql(gql[ATTENDED_USER_MUTATION], { name: ATTENDED_USER_MUTATION }),
  withProgress([ATTENDED_USER_MUTATION]),
  withSnackbar(),
)(CheckAttend);
