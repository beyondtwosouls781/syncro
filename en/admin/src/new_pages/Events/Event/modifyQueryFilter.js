import { appendProp } from './../../../new_utils';

const registeredParticipants = (props, input = {}) => {
  const participantsFilter = {};
  let orderFilter = {};

  if (input) {
    const {
      firstName,
      lastName,
      phone,
      email,
      paymentType,
      paymentStatus,
    } = input;
    if (firstName) participantsFilter.firstName_contains = firstName;
    if (lastName) participantsFilter.lastName_contains = lastName;
    if (email) participantsFilter.email_contains = email;
    if (phone) participantsFilter.phone_contains = phone;
    if (paymentStatus) {
      orderFilter = appendProp(orderFilter, 'payment');
      orderFilter.payment.status = paymentStatus;
    }
    // Типы оплаты: оплатил на месте, оплата через сайт, сертификат, абонемент, без оплаты, подарок.
    if (paymentType) {
      orderFilter = appendProp(orderFilter, 'payment');
      switch (paymentType) {
        case 'certificate': {
          orderFilter.payment.certificatesInPayments_some = {
            id_not: null,
          };
          break;
        }
        case 'subscription': {
          orderFilter.payment.subscriptionsInPayments_some = {
            id_not: null,
          };
          break;
        }
        case 'pending': {
          orderFilter.payment.status = 'PENDING';
          break;
        }
        case 'gift': {
          break;
        }
        default: {
          orderFilter.payment.type = paymentType;
        }
      }
    }
  }

  return { participantsFilter, orderFilter };
};

const arrivingParticipants = (props, input = {}) => {
  const { match } = props;
  const { id } = match.params;

  const filter = { event: { id }, user: {} };
  const {
    firstName,
    lastName,
    phone,
    email,
    paymentType,
    paymentStatus,
  } = input;
  if (firstName) filter.user.firstName_contains = firstName;
  if (lastName) filter.user.lastName_contains = lastName;
  if (email) filter.user.email_contains = email;
  if (phone) filter.user.phone_contains = phone;
  return { filter };
};

export default {
  registeredParticipants,
  arrivingParticipants,
};
