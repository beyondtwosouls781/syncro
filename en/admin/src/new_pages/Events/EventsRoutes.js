import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Events from './Events';
import Event from './Event';
import EventsViewWithRelations from './EventsViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../new_hoc/withWrapper';

const EventsRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Events
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <Event
              {...props}
              {...rest}
            />
          )}
        />
        {/* <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <EventsViewWithRelations
              {...props}
              {...rest}
            />
          )}
        /> */}
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(PaymentsRoutes, { title: 'Скидки' });
export default EventsRoutes;
