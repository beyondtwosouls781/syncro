import gql from 'graphql-tag';

export default gql`
  mutation CreateGalleryMutation(
      $title: String!,
      $type: String!,
    ){
    createGallery(
      title: $title,
      type: $type,
    ) {
      id
      createdAt
      updatedAt
      title
      type
    }
  }
`;
