import React from 'react';

import gql from './gql';

import {
  GALLERY_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const GalleryViewWithRelations = (props) => {
  const gqlQueryName = GALLERY_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Gallery';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default GalleryViewWithRelations;
