import gql from 'graphql-tag';

export default gql`
  query AllTemplatesQuery {
    allTemplates(
      first: 1,
    ) {
      id
      createdAt
      updatedAt

      scriptHead
      scriptBody
      styleHead
      contactTitle
      contactOfficeTitle
      contactOfficeAddress
      contactOfficePhone
      contactOfficeEmail
      contactMapCenter
      contactMapPlacemark

      metaKeywords
      metaDescription
      titleHead
    }
  }
`;
