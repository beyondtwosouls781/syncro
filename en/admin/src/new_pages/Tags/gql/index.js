import {
  CREATE_TAG_MUTATION,
  ALL_TAGS_QUERY,
  EDIT_TAG_MUTATION,
  TAG_QUERY,
  DELETE_TAG_MUTATION,
  TAG_QUERY_WITH_RELATIONS,
  ALL_TAGS_QUERY_SHORT,
} from './../../../constants';
import createTagMutation from './createTagMutation';
import allTagsQuery from './allTagsQuery';
import editTagMutation from './editTagMutation';
import tagQuery from './tagQuery';
import deleteTagMutation from './deleteTagMutation';
import tagQueryWithRelations from './tagQueryWithRelations';
import allTagsQueryShort from './allTagsQueryShort';

const gql = {
  [CREATE_TAG_MUTATION]: createTagMutation,
  [ALL_TAGS_QUERY]: allTagsQuery,
  [EDIT_TAG_MUTATION]: editTagMutation,
  [TAG_QUERY]: tagQuery,
  [DELETE_TAG_MUTATION]: deleteTagMutation,
  [TAG_QUERY_WITH_RELATIONS]: tagQueryWithRelations,
  [ALL_TAGS_QUERY_SHORT]: allTagsQueryShort,
};

export default gql;
