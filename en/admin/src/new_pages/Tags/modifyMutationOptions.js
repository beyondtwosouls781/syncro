export default () => {
  return (
    {
      refetchQueries: [
        'AllTagsQuery',
        'AllTagsQueryShort',
      ],
    }
  );
};
