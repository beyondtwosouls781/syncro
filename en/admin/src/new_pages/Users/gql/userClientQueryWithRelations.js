import gql from 'graphql-tag';

export default gql`
  query userClientQueryWithRelations($id: ID, $email: String) {
    User(
      id: $id,
      email: $email,
    ) {
      id
      createdAt
      updatedAt
      blocked
      email
      firstName
      lastName
      phone
      role
      socialNetwork
      blocked
      draft
      socUserId
      subscriptions {
       id
       title
       lecturesCount
       unusedLectures
       start
       end
      }
      certificates {
       id
       title
       lecturesCount
       unusedLectures
       start
       end
      }
      orders {
        id
        createdAt
        totalPrice
        events {
          date
          lecture {
            id
            title
            course {
              id
              title
            }
            cycle {
              id
              title
            }
          }
        }
        participants {
          id
          email
          firstName
          lastName
        }
        payment {
          id
          status
          totalPrice
          discountPrice
          orderPrice
          commission
          discounts {
            id
            title
          }
          additionalDiscounts {
            id
            title
          }
          certificatesInPayments {
            id
            title
          }
          subscriptionsInPayments {
            id
            title
          }
        }
      }
      tickets {
       id
       number
       createdAt
       event {
         id
         date
         lecture {
           id
           title
         }
       }
      }
      payments {
       id
       updatedAt
       status
       totalPrice
       discountPrice
       orderPrice
       commission
       certificate {
         id
         title
       }
       subscription {
         id
         title
       }
       order {
         id
       }
       discounts {
         id
         title
       }
       additionalDiscounts {
         id
         title
       }
       certificatesInPayments {
         id
         title
       }
       subscriptionsInPayments {
         id
         title
       }
      }
      ordersParticipants {
       id
       events {
         id
         date
         lecture {
           id
           title
         }
       }
      }
      reviews {
        id
        createdAt
        anons
        description
        public
        lecture {
          id
          title
        }
      }
    }
  }
`;
