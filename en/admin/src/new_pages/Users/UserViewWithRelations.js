import React from 'react';

import { format as formatUtil } from './../../utils';

import gql from './gql';

import {
  USER_CLIENT_QUERY_WITH_RELATIONS,
  USER_SYSTEM_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const UserViewWithRelations = (props) => {
  const {
    userRole,
  } = props;

  let gqlQueryName = null;
  let currentFields = null;

  switch (userRole) {
    case 'CLIENT': {
      gqlQueryName = USER_CLIENT_QUERY_WITH_RELATIONS;
      currentFields = fields.client;
      break;
    }
    default: {
      gqlQueryName = USER_SYSTEM_QUERY_WITH_RELATIONS;
      currentFields = fields.admin;
    }
  }
  const queryResultObjectName = 'User';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={currentFields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default UserViewWithRelations;
