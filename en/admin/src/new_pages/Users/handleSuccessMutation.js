export default (data, { history, location }) => {
  const { pathname } = location;
  switch (true) {
    case pathname.indexOf('administrator') !== -1: {
      history.push('/users/administrators');
      break;
    }
    case pathname.indexOf('moderator') !== -1: {
      history.push('/users/moderators');
      break;
    }
    case pathname.indexOf('curator') !== -1: {
      history.push('/users/curators');
      break;
    }
    default: {
      history.push('/users');
    }
  }
};
