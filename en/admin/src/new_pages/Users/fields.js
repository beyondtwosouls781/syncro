import { USER_ROLES } from './../../enum';

const createWithoutPassword = [
  {
    id: 'firstName',
    label: 'Имя',
    required: true,
  },
  {
    id: 'lastName',
    label: 'Фамилия',
    required: true,
  },
  {
    id: 'email',
    label: 'E-mail',
    required: true,
  },
  {
    id: 'phone',
    label: 'Телефон',
    required: true,
  },
];

const create = createWithoutPassword.concat([
  {
    id: 'password',
    label: 'Новый пароль',
    required: true,
    type: 'password',
    helperText: 'Минимальная длина 8 символов',
  },
  {
    id: 'repassword',
    type: 'password',
    label: 'Повторите пароль',
    required: true,
  },
]);

const update = create.map((field) => {
  const { id } = field;
  const newField = Object.assign({}, field);
  if (id === 'password' || id === 'repassword') {
    newField.required = false;
  }
  return newField;
});
update.push(
  {
    id: 'id',
    label: 'id',
    disabled: true,
    required: true,
    type: 'multiline',
  },
  {
    id: 'blocked',
    label: 'Заблокировать',
    defaultValue: false,
    type: 'checkbox',
    outputType: 'boolean',
  },
);

const view = update.filter(({ id }) => id !== 'password' && id !== 'repassword');

const filter = [
  {
    id: 'firstName',
    label: 'Имя',
  },
  {
    id: 'lastName',
    label: 'Фамилия',
  },
  {
    id: 'email',
    label: 'E-mail',
  },
  {
    id: 'phone',
    label: 'Телефон',
  },
  {
    id: 'createdAtFrom',
    label: 'Дата регистрации от',
    type: 'date',
  },
  {
    id: 'createdAtTo',
    label: 'Дата регистрации до',
    type: 'date',
  },
];


const client = {
  create: create.slice(),
  update: update.slice(),
  view: view.slice(),
  filter: filter.slice(),
  createWithoutPassword: createWithoutPassword.slice(),
};

const clientRole = {
  id: 'role',
  enumValues: USER_ROLES,
  label: 'Роль',
  defaultValue: 'CLIENT',
  required: true,
  disabled: true,
};

client.create.push(clientRole);
client.createWithoutPassword.push(clientRole);
client.update.push(clientRole);

client.view.push(
  {
    id: 'subscriptions',
    label: 'Абонементы',
    type: 'relation',
    relation: 'subscriptions',
  },
  {
    id: 'certificates',
    label: 'Сертификаты',
    type: 'relation',
    relation: 'certificates',
  },
  {
    id: 'payments',
    label: 'Платежи',
    type: 'relation',
    relation: 'payments',
  },
  {
    id: 'orders',
    label: 'Заказы',
    type: 'relation',
    relation: 'orders',
  },
  {
    id: 'tickets',
    label: 'Билеты',
    type: 'relation',
    relation: 'tickets',
  },
  {
    id: 'ordersParticipants',
    label: 'Участник',
    type: 'relation',
    relation: 'ordersParticipants',
  },
  {
    id: 'reviews',
    label: 'Отзывы',
    type: 'relation',
    relation: 'reviews',
  },
);

const admin = {
  create: create.slice(),
  update: update.slice(),
  view: view.slice(),
  filter: filter.slice(),
  createWithoutPassword: createWithoutPassword.slice(),
};

const adminRole = {
  id: 'role',
  enumValues: USER_ROLES,
  label: 'Роль',
  defaultValue: 'ADMIN',
  required: true,
  disabled: true,
};

admin.create.push(adminRole);
admin.createWithoutPassword.push(adminRole);
admin.update.push(adminRole);

admin.view.push(
  {
    id: 'lecturesCreated',
    label: 'Созданные лекции',
    type: 'relation',
    relation: 'lecturesCreated',
  },
  {
    id: 'coursesCreated',
    label: 'Созданные курсы',
    type: 'relation',
    relation: 'coursesCreated',
  },
  {
    id: 'cyclesCreated',
    label: 'Созданные циклы',
    type: 'relation',
    relation: 'cyclesCreated',
  },
  {
    id: 'curator',
    label: 'Координатор',
    type: 'relation',
    relation: 'curator',
  },
);

const moderator = {
  create: create.slice(),
  update: update.slice(),
  view: view.slice(),
  filter: filter.slice(),
  createWithoutPassword: createWithoutPassword.slice(),
};

const moderatorRole = {
  id: 'role',
  enumValues: USER_ROLES,
  label: 'Роль',
  defaultValue: 'MODERATOR',
  required: true,
  disabled: true,
};

moderator.create.push(moderatorRole);
moderator.update.push(moderatorRole);

const curator = {
  create: create.slice(),
  update: update.slice(),
  view: view.slice(),
  filter: filter.slice(),
  createWithoutPassword: createWithoutPassword.slice(),
};

const curatorRole = {
  id: 'role',
  enumValues: USER_ROLES,
  label: 'Роль',
  defaultValue: 'CURATOR',
  required: true,
  disabled: true,
};

curator.create.push(curatorRole);
curator.update.push(curatorRole);

export default {
  client,
  admin,
  moderator,
  curator,
  filter,
};
