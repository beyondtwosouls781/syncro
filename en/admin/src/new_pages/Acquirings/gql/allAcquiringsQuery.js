import gql from 'graphql-tag';

export default gql`
  query AllAcquiringsQuery($filter: AcquiringFilter, $first: Int, $skip: Int, $orderBy: AcquiringOrderBy) {
    allAcquirings(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      createdAt
      updatedAt

      commission
      password
      shopId
      title
      token
      currentAcquiring {
        id
      }

    }
    _allAcquiringsMeta {
      count
    }
  }
`;
