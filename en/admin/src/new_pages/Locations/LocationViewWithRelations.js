import React from 'react';

import { format as formatUtil } from './../../utils';

import gql from './gql';

import {
  LOCATION_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewEntityWithRelations from './../../new_components/ViewEntityWithRelations';

import fields from './fields';

const LocationViewWithRelations = (props) => {

  const gqlQueryName = LOCATION_QUERY_WITH_RELATIONS;
  const queryResultObjectName = 'Location';

  return (
    <div>
      <ViewEntityWithRelations
        {...props}
        fields={fields.view}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
      />
    </div>
  );
};

export default LocationViewWithRelations;
