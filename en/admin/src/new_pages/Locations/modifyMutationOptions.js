export default () => {
  return (
    {
      refetchQueries: [
        'AllLocationsQuery',
        'AllLocationsQueryShort',
      ],
    }
  );
};
