import gql from 'graphql-tag';

export default gql`
  query AllLocationsQueryShort($filter: LocationFilter, $first: Int, $skip: Int, $orderBy: LocationOrderBy) {
    allLocations(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id
      title
      metro
      address
    }
  }
`;
