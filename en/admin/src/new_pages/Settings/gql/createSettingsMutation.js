import gql from 'graphql-tag';

export default gql `
  mutation CreateSettingMutation(
    $contactOfficeAddress: String
    $contactOfficePhone: String
    $contactOfficeTitle: String
    $mailChimpAPIKey: String
    $mailChimpHost: String
    $mailChimpMainList: String
    $mailChimpNewSchedule: String
    $mailChimpReactivationList: String
    $senderEmail: String
    $senderName: String
    $mandrillAPIKey: String
    $mandrillHost: String
    $mandrillTemplateSlugCertificate: String
    $mandrillTemplateSlugCertificateAsGift: String
    $mandrillTemplateSlugConfirmEmail: String
    $mandrillTemplateSlugMaterialsOfLecture: String
    $mandrillTemplateSlugOptRegistration: String
    $mandrillTemplateSlugOrder: String
    $mandrillTemplateSlugOrderWithoutPayment: String
    $mandrillTemplateSlugPayment: String
    $mandrillTemplateSlugRegistration: String
    $mandrillTemplateSlugReminderOfLecture: String
    $mandrillTemplateSlugResetPass: String
    $mandrillTemplateSlugSubscription: String
    $mandrillTemplateSlugTicket: String
    $mandrillTemplateSlugUserBlock: String
    $mandrillTemplateSlugUserUnblock: String
    $smtpHost: String
    $smtpLogin: String
    $smtpPassword: String
    $acquiringId: ID
  ){
    createSetting(
      contactOfficeAddress: $contactOfficeAddress
      contactOfficePhone: $contactOfficePhone
      contactOfficeTitle: $contactOfficeTitle
      mailChimpAPIKey: $mailChimpAPIKey
      mailChimpHost: $mailChimpHost
      mailChimpMainList: $mailChimpMainList
      mailChimpNewSchedule: $mailChimpNewSchedule
      mailChimpReactivationList: $mailChimpReactivationList
      senderEmail: $senderEmail
      senderName: $senderName
      mandrillAPIKey: $mandrillAPIKey
      mandrillHost: $mandrillHost
      mandrillTemplateSlugCertificate: $mandrillTemplateSlugCertificate
      mandrillTemplateSlugCertificateAsGift: $mandrillTemplateSlugCertificateAsGift
      mandrillTemplateSlugConfirmEmail: $mandrillTemplateSlugConfirmEmail
      mandrillTemplateSlugMaterialsOfLecture: $mandrillTemplateSlugMaterialsOfLecture
      mandrillTemplateSlugOptRegistration: $mandrillTemplateSlugOptRegistration
      mandrillTemplateSlugOrder: $mandrillTemplateSlugOrder
      mandrillTemplateSlugOrderWithoutPayment: $mandrillTemplateSlugOrderWithoutPayment
      mandrillTemplateSlugPayment: $mandrillTemplateSlugPayment
      mandrillTemplateSlugRegistration: $mandrillTemplateSlugRegistration
      mandrillTemplateSlugReminderOfLecture: $mandrillTemplateSlugReminderOfLecture
      mandrillTemplateSlugResetPass: $mandrillTemplateSlugResetPass
      mandrillTemplateSlugSubscription: $mandrillTemplateSlugSubscription
      mandrillTemplateSlugTicket: $mandrillTemplateSlugTicket
      mandrillTemplateSlugUserBlock: $mandrillTemplateSlugUserBlock
      mandrillTemplateSlugUserUnblock: $mandrillTemplateSlugUserUnblock
      smtpHost: $smtpHost
      smtpLogin: $smtpLogin
      smtpPassword: $smtpPassword
      acquiringId: $acquiringId
    ) {
      id
      createdAt
      updatedAt

      acquiring {
        id
        title
      }
      contactOfficeAddress
      contactOfficePhone
      contactOfficeTitle
      mailChimpAPIKey
      mailChimpHost
      mailChimpMainList
      mailChimpNewSchedule
      mailChimpReactivationList
      mandrillAPIKey
      mandrillHost
      mandrillTemplateSlugCertificate
      mandrillTemplateSlugCertificateAsGift
      mandrillTemplateSlugConfirmEmail
      mandrillTemplateSlugMaterialsOfLecture
      mandrillTemplateSlugOptRegistration
      mandrillTemplateSlugOrder
      mandrillTemplateSlugOrderWithoutPayment
      mandrillTemplateSlugPayment
      mandrillTemplateSlugRegistration
      mandrillTemplateSlugReminderOfLecture
      mandrillTemplateSlugResetPass
      mandrillTemplateSlugSubscription
      mandrillTemplateSlugTicket
      smtpHost
      smtpLogin
      smtpPassword
    }
  }
`;
