import gql from 'graphql-tag';

export default gql`
  mutation UpdateAliasMutation(
      $id: ID!,
      $alias: String!,
    ){
    updateAlias(
      id: $id,
      alias: $alias,
    ) {
      id
      alias
    }
  }
`;
