import {
  updateLecture,
  updateLectureForCourse,
  updateLectureForCycle,
} from './updateLecture';

const extFileds = [
  {
    id: 'events',
    label: 'Даты проведения лекции',
    type: 'relation',
    relation: 'events',
  },
  {
    id: 'reviews',
    label: 'Отзывы',
    type: 'relation',
    relation: 'reviews',
  },
];

const viewLecture = updateLecture.concat(extFileds);

const viewLectureForCourse = updateLectureForCourse.concat(extFileds);

const viewLectureForCycle = updateLectureForCycle.concat(extFileds);

export default {};
export { viewLecture };
export { viewLectureForCourse };
export { viewLectureForCycle };
