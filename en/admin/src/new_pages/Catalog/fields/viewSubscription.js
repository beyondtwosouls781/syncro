import updateSubscription from './updateSubscription';

const viewSubscription = updateSubscription.slice();

export default viewSubscription;
