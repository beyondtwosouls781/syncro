import createSubscription from './createSubscription';

const idField = {
  id: 'id',
  label: 'id',
  type: 'multiline',
  required: true,
  disabled: true,
};

const updateSubscription = createSubscription.concat(idField);

export default updateSubscription;
