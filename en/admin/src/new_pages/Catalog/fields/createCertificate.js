import {
  SUB_PRODUCT_TYPES,
} from './../../../enum';
import { GC_USER_ID } from './../../../constants';

const userId = localStorage.getItem(GC_USER_ID);

const createCertificate = [
  // {
  //   id: 'author',
  //   label: 'Автор',
  //   type: 'author',
  //   disabled: true,
  //   hidden: true,
  //   defaultValue: userId,
  //   outputType: 'id',
  // },
  {
    id: 'type',
    label: 'Тип',
    type: 'enum',
    enum: SUB_PRODUCT_TYPES,
    defaultValue: 'CERTIFICATE',
    required: true,
    disabled: true,
  },
  {
    id: 'public',
    label: 'Опубликовать',
    type: 'checkbox',
    defaultValue: false,
    outputType: 'boolean',
  },
  {
    id: 'title',
    label: 'Название',
    required: true,
    type: 'multiline',
  },
  {
    id: 'lecturesCount',
    required: true,
    label: 'Количество лекций',
    helperText: '-1 - безлимит',
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'personsCount',
    label: 'Количество персон',
    type: 'number',
    defaultValue: '1',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'months',
    required: true,
    label: 'Срок действия(месяцев)',
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'price',
    required: true,
    label: 'Цена',
    type: 'number',
    outputType: 'number',
    outputSubType: 'int',
  },
  {
    id: 'prefix',
    required: true,
    label: 'Префикс',
    type: 'multiline',
  },
  {
    id: 'discounts',
    label: 'Скидки',
    relation: 'discounts',
    type: 'relation',
    disabled: true,
    multiple: true,
    defaultValue: [],
    outputType: 'relation',
    outputSubType: 'ids',
  },
];

export default createCertificate;
