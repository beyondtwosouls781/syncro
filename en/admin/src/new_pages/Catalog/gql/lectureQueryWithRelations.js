import gql from 'graphql-tag';

export default gql`
  query lectureQueryWithRelations($id: ID!) {
    Lecture(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      lectureType,
      title,
      subTitle,
      titleForCourse,
      titleForCycle,
      anons,
      anonsForCourse,
      anonsForCycle,
      public,
      description,
      themes,
      duration,

      img1240x349,
      img340x192,
      img340x340,
      metaDescription,
      metaKeywords,
      presentation,
      #
      author {
        id,

        firstName,
        lastName,
        email,
        phone,
        role,
      },
      #
      alias {
        id,
        alias,
      },
      #
      course {
        id,
        title,
      },
      #
      cycle {
        id,
        title,
      },
      #
      events {
        id,
        date,
        price,
        quantityOfTickets,
        tickets {
          id
        },
        lecture {
          id,
        }
        lecturers {
          id,
          firstName,
          lastName,
        },
        location {
          id,
          title,
          address,
          metro,
        },
      },
      #
      reviews {
        id
        createdAt
        anons
        description
        public
        user {
          id
          firstName,
          lastName,
        }
      },
      #
      tags {
        id,
        title,
        color,
        textColor,
      },
      #
      discounts {
        id,
        title,
        promocode,
        rate,
        allProducts,
        buyingLecturesCount,
        participantsCount,
        useCount,
        validityPeriodFrom,
        validityPeriodTo,
      }
      #
      metaTags {
        id,
        metaKeywords,
        metaDescription,
        title,
      },
      #
      recommendedCourses {
        id,
        title,
      },
      #
      recommendedCycles {
        id,
        title,
      },
      #
      recommendedLectures {
        id,
        title,
      },
      #
      inRecommendationsOfCourses {
        id,
        title,
      },
      #
      inRecommendationsOfCycles {
        id,
        title,
      },
      #
      inRecommendationsOfLectures {
        id,
        title,
      },
      #
    }
  }
`;
