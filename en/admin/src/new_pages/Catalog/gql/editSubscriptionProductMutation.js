import gql from 'graphql-tag';

export default gql `
  mutation UpdateSubscriptionProductMutation(
      $id: ID!,
      $lecturesCount: Int!,
      $months: Int!,
      $price: Float!,
      $title: String!,
      $prefix: String!,
      $personsCount: Int!,
      $image: String!,
      $type: SubProductTypes,
      $discountsIds: [ID!],
      $public: Boolean,
    ){
    updateSubscriptionProduct(
      id: $id,

      lecturesCount: $lecturesCount,
      months: $months,
      price: $price,
      prefix: $prefix,
      title: $title,
      type: $type,
      discountsIds: $discountsIds,
      personsCount: $personsCount,
      image: $image,
      public: $public,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lecturesCount,
      months,
      price,
      type,
      personsCount,
      prefix,
      public,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
    }
  }
`;
