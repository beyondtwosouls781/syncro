import gql from 'graphql-tag';

import fragments from './../fragments';

export default gql`
  query AllCertificateProductQuery($filter: CertificateProductFilter, $first: Int, $skip: Int, $orderBy: CertificateProductOrderBy) {
    allCertificateProducts(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      ...CertificateProductPayload
    }
  }
  ${fragments.certificateProductPayload}
`;
