import gql from 'graphql-tag';

export default gql`
  fragment CertificateProductShortPayload on CertificateProduct {
    id

    title
    lecturesCount
    months
    price
    prefix
    type
    personsCount
  }
`;
