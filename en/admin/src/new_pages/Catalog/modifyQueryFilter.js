import { LECTYRE_TYPES, PUBLIC } from './../../enum';
import { checkPublic, appendProp } from './../../new_utils';

const modifyQueryFilterLectures = (input = {}) => {
  let filter = {};
  filter = checkPublic(filter, input);

  filter.lectureType = input.lectureType || LECTYRE_TYPES[0].value;
  if (input.title) {
    filter.title_contains = input.title;
  }
  if (input.lecturersIds && input.lecturersIds.length > 0) {
    if (filter.events_some) {
      filter.events_some.lecturers_some = {
        id_in: input.lecturersIds,
      };
    } else {
      filter.events_some = {
        lecturers_some: {
          id_in: input.lecturersIds,
        },
      };
    }
  }
  if (input.locationsIds && input.locationsIds.length > 0) {
    if (filter.events_some) {
      filter.events_some.location = {
        id_in: input.locationsIds,
      };
    } else {
      filter.events_some = {
        location: {
          id_in: input.locationsIds,
        },
      };
    }
  }

  if (input.tagsIds && input.tagsIds.length > 0) {
    filter.tags_some = {
      id_in: input.tagsIds,
    };
  }

  return filter;
};

const modifyQueryFilterRecomendations = id => (input = {}) => {
  let filter = {};
  filter = checkPublic(filter, input);

  filter.lectureType = input.lectureType || LECTYRE_TYPES[0].value;
  if (input.title) {
    filter.title_contains = input.title;
  }
  if (input.lecturersIds && input.lecturersIds.length > 0) {
    if (filter.events_some) {
      filter.events_some.lecturers_some = {
        id_in: input.lecturersIds,
      };
    } else {
      filter.events_some = {
        lecturers_some: {
          id_in: input.lecturersIds,
        },
      };
    }
  }
  if (input.locationsIds && input.locationsIds.length > 0) {
    if (filter.events_some) {
      filter.events_some.location = {
        id_in: input.locationsIds,
      };
    } else {
      filter.events_some = {
        location: {
          id_in: input.locationsIds,
        },
      };
    }
  }

  if (input.tagsIds && input.tagsIds.length > 0) {
    filter.tags_some = {
      id_in: input.tagsIds,
    };
  }
  filter.id_not = id;

  return filter;
};

const modifyQueryFilterCourses = (input = {}) => {
  let filter = {};
  filter = checkPublic(filter, input);

  if (input.title) {
    filter.title_contains = input.title;
  }
  if (input.lecturersIds && input.lecturersIds.length > 0) {
    if (filter.lectures_some && filter.lectures_some.events_some) {
      filter.lectures_some.events_some.lecturers_some = {
        id_in: input.lecturersIds,
      };
    } else {
      filter.lectures_some = {
        events_some: {
          lecturers_some: {
            id_in: input.lecturersIds,
          },
        },
      };
    }
  }
  if (input.locationsIds && input.locationsIds.length > 0) {
    if (filter.lectures_some && filter.lectures_some.events_some) {
      filter.lectures_some.events_some.location = {
        id_in: input.locationsIds,
      };
    } else {
      filter.lectures_some = {
        events_some: {
          location: {
            id_in: input.locationsIds,
          },
        },
      };
    }
  }

  if (input.tagsIds && input.tagsIds.length > 0) {
    filter.tags_some = {
      id_in: input.tagsIds,
    };
  }

  return filter;
};

const modifyQueryFilterCycles = modifyQueryFilterCourses;

const modifyQueryFilterCertificates = (input = {}) => {
  let filter = {};
  filter = checkPublic(filter, input);
  if (input.title) filter.title_contains = input.tiitle;
  return filter;
};

const modifyQueryFilterSubscriptions = modifyQueryFilterCertificates;

const modifyQueryFilterLectureCatalog = (input = {}) => {
  let filter = {};
  const {
    tab,
    title,
    tagsIds,
    lecturersIds,
    locationsIds,
    lectureType,
  } = input;

  if (tab) {
    switch (tab.id) {
      case 'expired': {
        filter = appendProp(filter, 'events_every');
        filter.events_every.date_lte = new Date();
        filter.public = true;
        break;
      }
      case 'not_public': {
        filter.public = false;
        break;
      }
      default: {
        filter = appendProp(filter, 'events_some');
        filter.public = true;
        filter.events_some = {
          date_gte: new Date(),
        };
      }
    }
  } else {
    filter = appendProp(filter, 'events_some');
    filter.public = true;
    filter.events_some = {
      date_gte: new Date(),
    };
  }
  if (title) filter.title_contains = title;
  if (tagsIds && tagsIds.length > 0) filter.tags_some = { id_in: tagsIds };
  if (lecturersIds && lecturersIds.length > 0) {
    filter = appendProp(filter, 'events_some');
    filter.events_some.lecturers_some = { id_in: lecturersIds };
  }
  if (locationsIds && locationsIds.length > 0) {
    filter = appendProp(filter, 'events_some');
    filter.events_some.location = {
      id_in: locationsIds,
    };
  }
  if (lectureType) filter.lectureType = lectureType;
  return filter;
};

const modifyQueryFilterCourseCatalog = (input = {}) => {
  let filter = {};
  const {
    tab,
    title,
    tagsIds,
    lecturersIds,
    locationsIds,
  } = input;

  if (tab) {
    switch (tab.id) {
      case 'expired': {
        filter = appendProp(filter, 'lectures_every', 'events_every');

        filter.lectures_every.events_every.date_lte = new Date();
        filter.public = true;
        break;
      }
      case 'not_public': {
        filter.public = false;
        break;
      }
      default: {
        filter = appendProp(filter, 'lectures_some', 'events_some');

        filter.public = true;
        filter.lectures_some.events_some = {
          date_gte: new Date(),
        };
      }
    }
  } else {
    filter = appendProp(filter, 'lectures_some', 'events_some');

    filter.public = true;
    filter.lectures_some.events_some = {
      date_gte: new Date(),
    };
  }
  if (title) filter.title_contains = title;
  if (tagsIds && tagsIds.length > 0) filter.tags_some = { id_in: tagsIds };
  if (lecturersIds && lecturersIds.length > 0) {
    filter = appendProp(filter, 'lectures_some', 'events_some');
    filter.lectures_some.events_some.lecturers_some = { id_in: lecturersIds };
  }
  if (locationsIds && locationsIds.length > 0) {
    filter = appendProp(filter, 'lectures_some', 'events_some');

    filter.lectures_some.events_some.location = {
      id_in: locationsIds,
    };
  }
  return filter;
};

const modifyQueryFilterCycleCatalog = modifyQueryFilterCourseCatalog;

const modifyQueryFilterSubscriptionCatalog = (input = {}) => {
  const filter = {};
  const {
    tab,
    title,
  } = input;

  if (tab && tab.id === 'not_public') {
    filter.public = false;
  } else {
    filter.public = true;
  }
  if (title) filter.title_contains = title;
  return filter;
};

const modifyQueryFilterCertificateCatalog = modifyQueryFilterSubscriptionCatalog;


export default {
  lectures: modifyQueryFilterLectures,
  recomendations: modifyQueryFilterRecomendations,
  courses: modifyQueryFilterCourses,
  cycles: modifyQueryFilterCycles,
  certificates: modifyQueryFilterCertificates,
  subscriptions: modifyQueryFilterSubscriptions,
  courseCatalog: modifyQueryFilterCourseCatalog,
  cycleCatalog: modifyQueryFilterCycleCatalog,
  subscriptionCatalog: modifyQueryFilterSubscriptionCatalog,
  certificateCatalog: modifyQueryFilterCertificateCatalog,
  lectureCatalog: modifyQueryFilterLectureCatalog,
};
