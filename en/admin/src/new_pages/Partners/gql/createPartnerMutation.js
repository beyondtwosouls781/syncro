import gql from 'graphql-tag';

export default gql`
  mutation CreatePartnerMutation(
      $image: String
      $link: String
      $title: String!
    ){
    createPartner(
      image: $image,
      link: $link,
      title: $title,
    ) {
      id
    }
  }
`;
