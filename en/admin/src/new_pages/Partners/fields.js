const create = [
  {
    id: 'title',
    label: 'Название',
    required: true,
  },
  {
    id: 'link',
    label: 'Ссылка',
    type: 'multiline'
  },
  {
    id: 'image',
    label: 'Изображение',
    type: 'image'
  },
];

const view = create.slice();

const update = create.concat({
  id: 'id',
  label: 'ID',
  required: true,
  disabled: true,
});

const filter = [
  {
    id: 'title',
    label: 'Название',
  },
  {
    id: 'link',
    label: 'Ссылка',
  },
];

export default {
  create,
  update,
  view,
  filter,
}
