export default [
  {
    label: 'Активные',
    id: 'active',
  },
  {
    label: 'Не модерированные',
    id: 'not_moderated',
  },
];
