export default () => {
  return (
    {
      refetchQueries: [
        'AllReviewsQuery',
      ],
    }
  );
};
