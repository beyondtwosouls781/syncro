import { format } from './../../utils';

const columnData = [
  {
    id: 'public',
    label: 'Опубликован',
  },
  {
    id: 'userFullName',
    label: 'Пользователь',
  },
  {
    id: 'anons',
    label: 'Анонс',
  },
  {
    id: 'description',
    label: 'Основной текст',
  },
  {
    id: 'createdAt',
    label: 'Дата создания',
  },
];
export default columnData;
