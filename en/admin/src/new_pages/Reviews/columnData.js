import { format } from './../../utils';

const columnData = [
  {
    id: 'userFullName',
    label: 'Пользователь',
  },
  {
    id: 'lectureTitle',
    label: 'Лекция',
  },
  {
    id: 'anons',
    label: 'Анонс',
  },
  {
    id: 'description',
    label: 'Основной текст',
  },
  {
    id: 'createdAt',
    label: 'Дата создания',
  },
];
export default columnData;
