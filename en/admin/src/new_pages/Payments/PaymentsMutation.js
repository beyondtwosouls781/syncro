import React from 'react';

import gql from './gql';

import {
  CREATE_PAYMENT_MUTATION,
  EDIT_PAYMENT_MUTATION,
  PAYMENT_QUERY,
} from './../../constants';

import MutationEntities from './../../new_components/MutationEntities';

import paymentsFields from './fields';

import handleSuccessMutation from './handleSuccessMutation';
import modifyMutationOptions from './modifyMutationOptions';

const PaymentsMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resultMutationMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let queryResultObjectName = null;

  if (mutationType === 'create') {
    gqlMutationName = CREATE_PAYMENT_MUTATION;
    fields = paymentsFields.create;
    mutationResultObjectName = 'createPayment';
    resultMutationMessageSuccses = 'Платеж создан';
  } else {
    gqlMutationName = EDIT_PAYMENT_MUTATION;
    fields = paymentsFields.update;
    mutationResultObjectName = 'updatePayment';
    resultMutationMessageSuccses = 'Платеж обновлен';
    gqlQueryName = PAYMENT_QUERY;
    queryResultObjectName = 'Payment';
  }

  return (
    <div>
      <MutationEntities
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        resultMutationMessageSuccses={resultMutationMessageSuccses}
        modifyMutationOptions={modifyMutationOptions}
        onSuccessMutation={handleSuccessMutation}
      />
    </div>
  );
};

export default PaymentsMutation;
