import gql from 'graphql-tag';

export default gql`
  query paymentQuery($id: ID) {
    Payment(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      public,
      promocode,
      rate,
      allProducts,
      buyingLecturesCount,
      participantsCount,
      useCount,
      validityPeriodFrom,
      validityPeriodTo,
      courses {
        id,
        title,
      }
      cycles {
        id,
        title,
      }
      lectures {
        id,
        title,
      }
      orders {
        id
      }
      tags {
        id,
        title,
      }
      _coursesMeta {
        count
      }
      _cyclesMeta {
        count
      },
      _lecturesMeta {
        count
      },
      _ordersMeta {
        count
      },
      _tagsMeta {
        count
      },
    }
  }
`;
