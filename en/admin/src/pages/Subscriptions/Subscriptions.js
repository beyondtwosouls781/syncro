import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_SUBSCRIPTIONS_QUERY,
  DELETE_SUBSCRIPTION_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import subscriptionsFields from './fields';
import subscriptionsActions from './actions';
import subscriptionsColumnData from './columnData';

import tabs from './tabs';

const Subscriptions = (props) => {

  const gqlAllQueryName = ALL_SUBSCRIPTIONS_QUERY;
  const gqlAllQueryItemsName = 'allSubscriptions';
  const gqlMutationDelete = DELETE_SUBSCRIPTION_MUTATION;
  const gqlMutationDeleteItemName = 'deleteSubscription';
  const resulMutatiomtMessageSuccses = 'Сертификат удален';

  return (
    <div>
      <ListMaterials
        {...props}
        tabs={tabs}
        actionsButtons={subscriptionsActions}
        columnData={subscriptionsColumnData}
        fields={subscriptionsFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showActions={props.showActions === undefined ? true : props.showActions}
        // filterModifier={filterModifier}
      />
    </div>
  );
};

export default Subscriptions;
