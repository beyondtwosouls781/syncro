import gql from 'graphql-tag';

export default gql`
  mutation CreateSubscriptionMutation(
      $userId: ID,
    ){
    createSubscription(
      userId: $userId,
    ) {
      id
    }
  }
`;
