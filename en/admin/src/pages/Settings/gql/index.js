import {
  ALL_SETTINGS_QUERY,
  SETTING_QUERY,
  CREATE_SETTINGS_MUTATION,
  EDIT_SETTINGS_MUTATION,
} from './../../../constants';

import allSettingsQuery from './allSettingsQuery';
import settingQuery from './settingQuery';
import createSettingsMutation from './createSettingsMutation';
import updateSettingsgMutation from './updateSettingsMutation';

const gql = {
  [ALL_SETTINGS_QUERY]: allSettingsQuery,
  [SETTING_QUERY]: settingQuery,
  [CREATE_SETTINGS_MUTATION]: createSettingsMutation,
  [EDIT_SETTINGS_MUTATION]: updateSettingsgMutation,
};

export default gql;
