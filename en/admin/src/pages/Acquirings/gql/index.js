import {
  CREATE_ACQUIRING_MUTATION,
  ALL_ACQUIRINGS_QUERY,
  EDIT_ACQUIRING_MUTATION,
  ACQUIRING_QUERY,
  DELETE_ACQUIRING_MUTATION,
  ACQUIRING_QUERY_WITH_RELATIONS,
} from './../../../constants';
import createAcquiringMutation from './createAcquiringMutation';
import allAcquiringsQuery from './allAcquiringsQuery';
import editAcquiringMutation from './editAcquiringMutation';
import acquiringQuery from './acquiringQuery';
import deleteAcquiringMutation from './deleteAcquiringMutation';
import acquiringQueryWithRelations from './acquiringQueryWithRelations';

const gql = {
  [CREATE_ACQUIRING_MUTATION]: createAcquiringMutation,
  [ALL_ACQUIRINGS_QUERY]: allAcquiringsQuery,
  [EDIT_ACQUIRING_MUTATION]: editAcquiringMutation,
  [ACQUIRING_QUERY]: acquiringQuery,
  [DELETE_ACQUIRING_MUTATION]: deleteAcquiringMutation,
  [ACQUIRING_QUERY_WITH_RELATIONS]: acquiringQueryWithRelations,
};

export default gql;
