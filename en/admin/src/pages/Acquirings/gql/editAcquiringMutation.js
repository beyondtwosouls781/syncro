import gql from 'graphql-tag';

export default gql`
  mutation UpdateAcquiringMutation(
      $id: ID!,
      $commission: Float!
      $password: String!
      $shopId: String!
      $title: String!
      $token: String!
    ){
    updateAcquiring(
      id: $id,
      commission: $commission
      password: $password
      shopId: $shopId
      title: $title
      token: $token
    ) {
      id
      createdAt
      updatedAt

      commission
      password
      shopId
      title
      token
      currentAcquiring {
        id
      }
      _paymentsMeta {
        count
      }
    }
  }
`;
