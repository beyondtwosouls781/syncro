import * as R from 'ramda';

import { format } from './../../utils';

const create = [
  {
    id: 'title',
    label: 'Название',
    required: true,
    type: 'multiline',
  },
  {
    id: 'commission',
    label: 'Коммиссия',
    required: true,
    type: 'number-float',
  },
  {
    id: 'password',
    label: 'Пароль',
    required: true,
    type: 'text',
  },
  {
    id: 'token',
    label: 'Токен',
    required: true,
    type: 'text',
  },
  {
    id: 'shopId',
    label: 'ID магазина',
    required: true,
    type: 'text',
  },
];

const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
  // {
  //   id: 'alias_id',
  //   label: 'alias id',
  //   required: true,
  //   disabled: true,
  //   type: 'alias',
  // },
);

const view = update.slice();


const filter = [
  // {
  //   id: 'title_contains',
  //   value: '',
  //   label: 'Название',
  //   type: 'text',
  // },
];

export default {
  create,
  update,
  filter,
  view,
};
