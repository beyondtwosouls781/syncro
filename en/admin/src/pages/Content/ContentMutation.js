import React from 'react';

import {
  CREATE_ABOUTPAGE_MUTATION,
  EDIT_ABOUTPAGE_MUTATION,
  ABOUTPAGE_QUERY,
  ALL_ABOUTPAGE_QUERY,

  CREATE_MAINPAGE_MUTATION,
  EDIT_MAINPAGE_MUTATION,
  MAINPAGE_QUERY,
  ALL_MAINPAGE_QUERY,

  ALL_HOWWORKPAGES_QUERY,
  CREATE_HOWWORKPAGE_MUTATION,
  EDIT_HOWWORKPAGE_MUTATION,
  HOWWORKPAGE_QUERY,
} from './../../constants';

import gqlAboutPage from './AboutPage/gql';
import gqlMainPage from './MainPage/gql';
import gqlHowWorks from './HowWorks/gql';

import MutationMaterial from './../../components/MutationMaterial';

import aboutPageFields from './AboutPage/fields';
import mainPageFields from './MainPage/fields';
import howWorkPageFields from './HowWorks/fields';

const ContentMutation = (props) => {
  const {
    mutationType,
    typeMaterial,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;
  let gql = null;

  if (mutationType === 'create') {
    resulMutatiomtMessageSuccses = 'Создано';
  } else {
    resulMutatiomtMessageSuccses = 'Обновлено';
  }

  switch (typeMaterial) {
    case 'about': {
      gql = gqlAboutPage;
      if (mutationType === 'create') {
        gqlMutationName = CREATE_ABOUTPAGE_MUTATION;
        fields = aboutPageFields.create;
        mutationResultObjectName = 'createAboutPage';
      } else {
        gqlMutationName = EDIT_ABOUTPAGE_MUTATION;
        fields = aboutPageFields.update;
        mutationResultObjectName = 'updateAboutPage';
        gqlQueryName = ABOUTPAGE_QUERY;
        gqlQueryItemName = 'AboutPage';
      }
      break;
    }
    case 'howwork-mainpage': {
      gql = gqlHowWorks;
      if (mutationType === 'create') {
        gqlMutationName = CREATE_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.createHWMainPage;
        mutationResultObjectName = 'createHowWorkPage';
      } else {
        gqlMutationName = EDIT_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.updateHWMainPage;
        mutationResultObjectName = 'updateHowWorkPage';
        gqlQueryName = HOWWORKPAGE_QUERY;
        gqlQueryItemName = 'HowWorkPage';
      }
      break;
    }
    case 'howwork-course': {
      gql = gqlHowWorks;
      if (mutationType === 'create') {
        gqlMutationName = CREATE_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.createHWCourse;
        mutationResultObjectName = 'createHowWorkPage';
      } else {
        gqlMutationName = EDIT_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.updateHWCourse;
        mutationResultObjectName = 'updateHowWorkPage';
        gqlQueryName = HOWWORKPAGE_QUERY;
        gqlQueryItemName = 'HowWorkPage';
      }
      break;
    }
    case 'howwork-lecture': {
      gql = gqlHowWorks;
      if (mutationType === 'create') {
        gqlMutationName = CREATE_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.createHWLecture;
        mutationResultObjectName = 'createHowWorkPage';
      } else {
        gqlMutationName = EDIT_HOWWORKPAGE_MUTATION;
        fields = howWorkPageFields.updateHWLecture;
        mutationResultObjectName = 'updateHowWorkPage';
        gqlQueryName = HOWWORKPAGE_QUERY;
        gqlQueryItemName = 'HowWorkPage';
      }
      break;
    }
    default: {
      gql = gqlMainPage;
      if (mutationType === 'create') {
        gqlMutationName = CREATE_MAINPAGE_MUTATION;
        fields = mainPageFields.create;
        mutationResultObjectName = 'createMainPage';
      } else {
        gqlMutationName = EDIT_MAINPAGE_MUTATION;
        fields = mainPageFields.update;
        mutationResultObjectName = 'updateMainPage';
        gqlQueryName = MAINPAGE_QUERY;
        gqlQueryItemName = 'MainPage';
      }
    }
  }


  return (
    <div>
      <MutationMaterial
        {...props}
        showActions
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default ContentMutation;
