import gql from 'graphql-tag';

export default gql`
  mutation CreateAboutPageMutation(
      $title: String!,
      $subTitle: String!,
      $description: String!,
    ){
    createAboutPage(
      title: $title,
      subTitle: $subTitle,
      description: $description,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      description,
    }
  }
`;
