import gql from 'graphql-tag';

export default gql`
  query AllAboutPageQuery {
    allAboutPages(
      first: 1,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      description,
    }
  }
`;
