import gql from 'graphql-tag';

export default gql`
  mutation UpdateAboutPageMutation(
      $id: ID!
      $title: String,
      $subTitle: String,
      $description: String,
    ){
    updateAboutPage(
      id: $id,
      title: $title,
      subTitle: $subTitle,
      description: $description,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      subTitle,
      description,
    }
  }
`;
