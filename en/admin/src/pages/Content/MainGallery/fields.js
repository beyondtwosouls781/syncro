import * as R from 'ramda';
import { format } from './../../../utils';

const create = [
  {
    id: 'secret',
    label: 'Изображение',
    value: 'no-photo',
    type: 'image',
  },
  {
    id: 'description',
    label: 'Описание',
    type: 'multiline',
  },
  {
    id: 'galleryId',
    value: "cjbl9dfvv5gxd0114fe3tsie9",
    label: 'Описание',
    type: 'multiline',
    required: true,
    disabled: true,
  },
];


const update = create.slice();
update.splice(-1, 1)
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
  {
    id: 'tagsIds',
    label: 'Направления связанные с лектором',
    value: [],
    required: true,
    disabled: true,
    relation: 'tags',
    type: 'relation',
    // format: format.tagsToTitleString,
  },
);

const view = update.slice();

const filter = [
  {
    id: 'description_contains',
    label: 'Описание',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
