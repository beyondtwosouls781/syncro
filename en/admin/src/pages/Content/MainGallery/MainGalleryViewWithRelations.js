import React from 'react';

import gql from './gql';

import {
  IMAGE_QUERY_WITH_RELATIONS,
} from './../../../constants';

import ViewMaterialWithRelations from './../../../components/ViewMaterialWithRelations';

import fields from './fields';

const MainGalleryViewWithRelations = (props) => {

  const gqlQueryWithRelationsName = IMAGE_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Image';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={fields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
        // showActions={false}
      />
    </div>
  );
};

export default MainGalleryViewWithRelations;
