import { format } from './../../../utils';

const columnData = [
  { id: 'secret', numeric: false, disablePadding: false, label: 'Токен' },
  { id: 'description', numeric: false, disablePadding: false, label: 'Описание' },
];
export default columnData;
