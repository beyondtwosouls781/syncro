import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import MainGallery from './MainGallery';
import MainGalleryMutation from './MainGalleryMutation';
import MainGalleryViewWithRelations from './MainGalleryViewWithRelations';

import NotFound from './../../NotFound';
// import withWrapper from './../../../hoc/withWrapper';

const MainGalleriesRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <MainGallery
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <MainGalleryMutation {...props} {...rest} mutationType="create" title={'Новое изображение'} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <MainGalleryMutation {...props} {...rest} mutationType="update" title={'Редактирование изображения'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <MainGalleryViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(MainGalleriesRoutes, { title: 'Фотографии с мероприятий' });
export default MainGalleriesRoutes;
