import * as R from 'ramda';
import { format } from './../../utils';

const create = [
  {
    id: 'firstName',
    label: 'Имя',
    required: true,
  },
  {
    id: 'lastName',
    label: 'Фамилия',
    required: true,
  },
  {
    id: 'anons',
    label: 'Анонс',
    // required: true,
    type: 'multiline',
  },
  {
    id: 'anonsForLecture',
    label: 'Анонс для лекции',
    // required: true,
    type: 'multiline',
  },
  {
    id: 'description',
    label: 'Описание',
    // required: true,
    type: 'multiline',
  },
  {
    id: 'skills',
    label: 'Навыки',
    required: true,
    type: 'multiline',
    helperText: 'Через запятую',
    format: format.arrayToStringComma,
  },
  {
    id: 'specialization',
    value: '',
    label: 'Специализация',
    required: false,
    type: 'multiline',
  },
  {
    id: 'img160x160',
    label: 'Фото 160х160',
    width: 160,
    height: 160,
    required: true,
    disabled: true,
    type: 'image',
    value: 'no-photo',
  },
  {
    id: 'img340x225',
    label: 'Фото 340х225',
    width: 340,
    height: 225,
    required: true,
    disabled: true,
    type: 'image',
    value: 'no-photo',
  },
  {
    id: 'img337x536',
    label: 'Фото 337x536',
    width: 337,
    height: 536,
    required: true,
    disabled: true,
    type: 'image',
    value: 'no-photo',
  },
  {
    id: 'alias',
    label: 'URL alias',
    required: true,
    type: 'alias',
  },
  {
    id: 'tagsIds',
    label: 'Направления',
    value: [],
    required: true,
    disabled: true,
    relation: 'tags',
    type: 'relation',
    // format: format.tagsToTitleString,
  },
];


const update = create.slice();
update.splice(-1, 1)
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
  {
    id: 'tagsIds',
    label: 'Направления связанные с лектором',
    value: [],
    required: true,
    disabled: true,
    relation: 'tags',
    type: 'relation',
    // format: format.tagsToTitleString,
  },
);

const view = update.slice();
view.push(
  {
    id: 'lectures',
    label: 'Лекции связанные с лектором',
    disabled: true,
    relation: 'lectures',
    type: 'relation',
  },
);

const filter = [
  {
    id: 'firstName_contains',
    label: 'Имя',
  },
  {
    id: 'lastName_contains',
    label: 'Фамилия',
  },
  {
    id: 'tagsIds',
    label: 'Направления',
    relation: 'tags-select',
    type: 'relation',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
