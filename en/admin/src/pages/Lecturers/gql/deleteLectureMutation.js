import gql from 'graphql-tag';

export default gql`
  mutation deleteLectureMutation(
      $id: ID!
    ){
    deleteLecture(
      id: $id,
    ) {
      id
    }
  }
`;
