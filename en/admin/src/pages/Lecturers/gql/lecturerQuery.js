import gql from 'graphql-tag';

export default gql`
  query lecturerQuery($id: ID) {
    Lecturer(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,
      anons,
      anonsForLecture,
      description,
      events {
        id,
        lecture {
          id,
        },
      }
      firstName,
      img160x160,
      img337x536,
      img340x225,
      lastName,
      skills,
      specialization,
      tags {
        id,
        title
      }
      alias {
        id,
        alias,
      }
    }
  }
`;
