import gql from 'graphql-tag';

export default gql`
  query certificateProductQuery($id: ID!) {
    CertificateProduct(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      lectures,
      months,
      price,
      type,
      discounts {
        id,
        title,
        rate,
        useCount,
      }
      certificates {
        id,
        title,
        start,
        end,
        user {
          id,

          firstName,
          lastName,
        }
      }
      _certificatesMeta {
        count
      }
      _discountsMeta {
        count
      }
      #
    }
  }
`;
