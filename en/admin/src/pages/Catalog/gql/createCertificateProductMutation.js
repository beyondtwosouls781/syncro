import gql from 'graphql-tag';

export default gql`
  mutation createCertificateProductMutation(
    $lectures: Int!,
    $months: Int!,
    $price: Float!,
    $title: String!,
    $type: SubProductTypes,
    $discountsIds: [ID!],
  ){
    createCertificateProduct(
      lectures: $lectures,
      months: $months,
      price: $price,
      title: $title,
      type: $type,
      discountsIds: $discountsIds,
    ) {
      id
    }
  }
`;
