import gql from 'graphql-tag';

export default gql`
  query lectureQuery($id: ID!) {
    Lecture(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      lectureType,
      title,
      subTitle,
      titleForCourse,
      titleForCycle,
      anons,
      anonsForCourse,
      anonsForCycle,
      public,
      description,
      themes,
      duration,

      img1240x349,
      img340x192,
      img340x340,
      metaDescription,
      metaKeywords,
      #
      author {
        id,

        firstName,
        lastName,
        email,
        phone,
        role,
      },
      #
      alias {
        id,
        alias,
      },
      #
      course {
        id,
        title,
      },
      #
      cycle {
        id,
        title,
      },
      #
      events {
        id,
        date,
        price,
        quantityOfTickets,
        tickets {
          id
        },
        lecture {
          id,
        }
        lecturers {
          id,
          firstName,
          lastName,
        },
        location {
          id,
          title,
          address,
          metro,
        },
        # curator {
        #   id,
        #
        #   firstName,
        #   lastName,
        # }
      },
      #
      reviews {
        id,
        user {
          id,
          firstName,
          lastName,
        },
      },
      #
      discounts {
        id,
        title,
      }
      #
      tags {
        id,
        title,
        color,
        textColor,
      },
      #
      metaTags {
        id,
        metaKeywords,
        metaDescription,
        title,
      },
      #
      recommendedCourses {
        id,
        title,
      },
      #
      recommendedCycles {
        id,
        title,
      },
      #
      recommendedLectures {
        id,
        title,
      },
      #
      inRecommendationsOfCourses {
        id,
        title,
      },
      #
      inRecommendationsOfCycles {
        id,
        title,
      },
      #
      inRecommendationsOfLectures {
        id,
        title,
      },
      #
    }
  }
`;
