const lectures = [
  {
    label: 'Активные',
    id: 'active',
  },
  {
    label: 'Не опубликованные',
    id: 'not_public',
  },
  {
    label: 'Архив',
    id: 'archive',
  },
];

const courses = lectures.slice();
const cycles = lectures.slice();

const subscriptions = null;
const certificates = null;

export default {
  lectures,
  courses,
  cycles,
  subscriptions,
  certificates,
};

export { lectures };
export { courses };
export { cycles };
export { subscriptions };
export { certificates };
