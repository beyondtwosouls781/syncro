import { format } from './../../../utils';

// дате, направлению, лектору, локации.
const filterLectures = [
  {
    id: 'title',
    label: 'Заголовок',
    type: 'text',
  },
  {
    id: 'lecturersIds',
    label: 'Лекторы',
    relation: 'lecturers-select',
    type: 'relation',
  },
  {
    id: 'locationIds',
    label: 'Локация',
    relation: 'locations-select',
    type: 'relation',
  },
  {
    id: 'tagsIds',
    label: 'Направления',
    relation: 'tags-select',
    type: 'relation',
  },
  {
    id: 'date',
    label: 'Даты проведения',
    type: 'date',
    from: '_gte',
    to: '_lte',
    format: format.date,
  },
];

export default filterLectures;
