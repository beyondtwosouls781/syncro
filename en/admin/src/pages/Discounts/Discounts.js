import React from 'react';

import gql from './gql';

import {
  ALL_DISCOUNTS_QUERY,
  DELETE_DISCOUNT_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import discountsFields from './fields';
import discountsActions from './actions';
import discountsColumnData from './columnData';

import SelectAllQueryShort from './../../components/SelectAllQueryShort';

const Discounts = (props) => {

  const gqlAllQueryName = ALL_DISCOUNTS_QUERY;
  const gqlAllQueryItemsName = 'allDiscounts';
  const gqlMutationDelete = DELETE_DISCOUNT_MUTATION;
  const gqlMutationDeleteItemName = 'deleteDiscount';
  const resulMutatiomtMessageSuccses = 'Направление удалено';

  return (
    <div>
      <ListMaterials
        {...props}
        actionsButtons={discountsActions}
        columnData={discountsColumnData}
        fields={discountsFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showTitle={props.showTitle}
        showActions={props.showActions === undefined ? true : props.showActions}
        // filterModifier={filterModifier}
      />
    </div>
  );
};

export default Discounts;
