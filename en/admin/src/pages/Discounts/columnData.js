import { format } from './../../utils';

const columnData = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Название' },
  { id: 'promocode', numeric: false, disablePadding: false, label: 'Промокод' },
  { id: 'rate', numeric: true, disablePadding: false, label: 'Размер скидки'},
  { id: 'validityPeriodFrom', numeric: false, disablePadding: false, label: 'Начало действия'},
  { id: 'validityPeriodTo', numeric: false, disablePadding: false, label: 'Окончание действия'},
  { id: 'useCount', numeric: true, disablePadding: false, label: 'Количество использований'},

];
export default columnData;
