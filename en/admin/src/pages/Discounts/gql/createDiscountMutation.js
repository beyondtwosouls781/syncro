import gql from 'graphql-tag';

export default gql`
  mutation CreateDiscountMutation(
      $allProducts: Boolean,
      $buyingLecturesCount: Int,
      $participantsCount: Int,
      $promocode: String!,
      $rate: Int!,
      $title: String!,
      $useCount: Int,
      $validityPeriodFrom: DateTime!,
      $validityPeriodTo: DateTime!,
      $certificatesIds: [ID!],
      $coursesIds: [ID!],
      $cyclesIds: [ID!],
      $lecturesIds: [ID!],
      $ordersIds: [ID!],
      $subscriptionsIds: [ID!],
      $tagsIds: [ID!],
    ){
    createDiscount(
      allProducts: $allProducts,
      buyingLecturesCount: $buyingLecturesCount,
      participantsCount: $participantsCount,
      promocode: $promocode,
      rate: $rate,
      title: $title,
      useCount: $useCount,
      validityPeriodFrom: $validityPeriodFrom,
      validityPeriodTo: $validityPeriodTo,
      certificatesIds: $certificatesIds,
      coursesIds: $coursesIds,
      cyclesIds: $cyclesIds,
      lecturesIds: $lecturesIds,
      ordersIds: $ordersIds,
      subscriptionsIds: $subscriptionsIds,
      tagsIds: $tagsIds,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      promocode,
      rate,
      allProducts,
      buyingLecturesCount,
      participantsCount,
      useCount,
      validityPeriodFrom,
      validityPeriodTo,
      courses {
        id,
        title,
      }
      cycles {
        id,
        title,
      }
      lectures {
        id,
        title,
      }
      orders {
        id
      }
      tags {
        id,
        title,
      }
      _coursesMeta {
        count
      }
      _cyclesMeta {
        count
      },
      _lecturesMeta {
        count
      },
      _ordersMeta {
        count
      },
      _tagsMeta {
        count
      },
    }
  }
`;
