import gql from 'graphql-tag';

export default gql`
  query discountQuery($id: ID) {
    Discount(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,

      title,
      promocode,
      rate,
      allProducts,
      buyingLecturesCount,
      participantsCount,
      useCount,
      validityPeriodFrom,
      validityPeriodTo,
      courses {
        id,
        title,
      }
      cycles {
        id,
        title,
      }
      lectures {
        id,
        title,
      }
      orders {
        id
      }
      tags {
        id,
        title,
      }
      _coursesMeta {
        count
      }
      _cyclesMeta {
        count
      },
      _lecturesMeta {
        count
      },
      _ordersMeta {
        count
      },
      _tagsMeta {
        count
      },
    }
  }
`;
