import { format } from './../../utils';

const columnData = [
  { id: 'date', numeric: false, disablePadding: false, label: 'Дата' },
  { id: 'lecture', numeric: false, disablePadding: false, label: 'Лекция' },
  { id: 'lecturers', numeric: false, disablePadding: false, label: 'Лекторы' },
  { id: 'location', numeric: false, disablePadding: false, label: 'Локация' },
  { id: 'price', numeric: true, disablePadding: false, label: 'Цена' },
  { id: 'tickets', numeric: true, disablePadding: false, label: 'Количество билетов' },
];

export default columnData;
