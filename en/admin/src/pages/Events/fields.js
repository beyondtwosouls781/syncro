import * as R from 'ramda';
import { format } from './../../utils';

const create = [
  {
    id: 'date',
    label: 'Дата',
    type: 'date-time',
  },
  {
    id: 'price',
    label: 'Цена',
    type: 'number',
  },
  {
    id: 'tickets',
    label: 'Количество билетов',
    required: true,
    type: 'number',
  },
  {
    id: 'lecture',
    label: 'Лекция',
    required: true,
    relation: 'lecture',
    type: 'relation',
  },
  {
    id: 'lecturers',
    label: 'Лекторы',
    required: true,
    relation: 'lecturer',
    type: 'realtion',
  },
  {
    id: 'location',
    label: 'Локация',
    required: true,
    relation: 'location',
    type: 'realtion',
  },
];


const update = create.slice();
update.splice(-1, 1);
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'multiline',
  },
);

const view = update.slice();
view.push(
  {
    id: 'ordersIds',
    label: 'Заказы связанные с мероприятием',
    disabled: true,
    relation: 'orders',
    type: 'relation',
  },
);

const filter = [
  {
    id: 'lecturers',
    label: 'Лектор',
  },
  {
    id: 'location',
    label: 'Локация',
  },
  {
    id: 'price',
    label: 'Цена',
  },
  {
    id: 'rate',
    label: 'Размер скидки',
  },
  {
    id: 'date',
    label: 'Даты проведения',
    type: 'date',
    from: '_gte',
    to: '_lte',
    format: format.date,
  },
];

export default {
  create,
  update,
  filter,
  view,
};
