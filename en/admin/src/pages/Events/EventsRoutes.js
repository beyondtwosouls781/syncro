import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import Events from './Events';
import EventsMutation from './EventsMutation';
import EventViewWithRelations from './EventViewWithRelations';

import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const EventsRoutes = (props) => {
  const { match } = props;
  const { url } = match;

  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${url}`}
          component={rest => (
            <Events
              {...props}
              {...rest}
              linkForUpdateMaterial={`${url}/update`}
              linkForViewMaterial={`${url}/view`}
            />
          )}
        />
        <Route
          exact
          path={`${url}/create`}
          component={rest => (
            <EventsMutation {...props} {...rest} mutationType="create" title={'Новое мероприятие '} />
          )}
        />
        <Route
          exact
          path={`${url}/update/:id`}
          component={rest => (
            <EventsMutation {...props} {...rest} mutationType="update" title={'Редактирование мероприятия'} />
          )}
        />
        <Route
          exact
          path={`${url}/view/:id`}
          component={rest => (
            <EventViewWithRelations
              {...props}
              {...rest}
            />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} title={'Ошибка'} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(EventsRoutes, { title: 'Мероприятия' });
export default EventsRoutes;
