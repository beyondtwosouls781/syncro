import React from 'react';

import gql from './gql';

import {
  EVENT_QUERY_WITH_RELATIONS,
} from './../../constants';

import ViewMaterialWithRelations from './../../components/ViewMaterialWithRelations';

import tagsFields from './fields';

const EventViewWithRelations = (props) => {

  const gqlQueryWithRelationsName = EVENT_QUERY_WITH_RELATIONS;
  const gqlQueryWithRelationsNameItemName = 'Event';

  return (
    <div>
      <ViewMaterialWithRelations
        {...props}
        fields={tagsFields}
        gql={gql}
        gqlQueryWithRelationsName={gqlQueryWithRelationsName}
        gqlQueryWithRelationsNameItemName={gqlQueryWithRelationsNameItemName}
        // showActions={false}
      />
    </div>
  );
};

export default EventViewWithRelations;
