import React from 'react';

import SelectRelatedItems from './../../new_components/SelectRelatedItems';
import Preview from './../../new_components/Preview';

import { formatInputDataArrayForTable } from './../../new_utils/';

import {
  ALL_LECTURERS_QUERY_SHORT,
} from './../../constants';

import gql from './../Lecturers/gql';
import columnData from './columnData';

const gqlQueryName = ALL_LECTURERS_QUERY_SHORT;

const queryResultObjectName = 'allLecturers';

const queryFilter = (filter = {}) => {
  const output = {};
  Object.keys(filter).forEach((key) => {
    output[key] = filter[key];
  });
  return output;
};

const queryVariables = (variables = {}) => {
  const output = {};
  Object.keys(variables).forEach((key) => {
    switch (key) {
      case 'filter': {
        output.filter = queryFilter(variables.filter);
        break;
      }
      default: {
        output[key] = variables[key];
      }
    }
  });
  return output;
};

const graphqlOptions = (props) => {
  const output = {
    variables: queryVariables,
  };
  return output;
};

const tableTitle = 'Лекторы';

const Relation = (props) => {
  return (
    <div>
      <SelectRelatedItems
        {...props}
        columnData={columnData}
        gql={gql}
        gqlQueryName={gqlQueryName}
        queryResultObjectName={queryResultObjectName}
        formatInputData={formatInputDataArrayForTable}
        graphqlOptions={graphqlOptions}
        tableTitle={tableTitle}
      />
      {/* <Preview /> */}
    </div>
  );
};

export default Relation;
