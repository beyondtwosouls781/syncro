import React, { Component } from 'react';
import { compose, graphql } from 'react-apollo';

import gql from './../Lecturers/gql';

import {
  ALL_LECTURERS_QUERY,
  DELETE_LECTURER_MUTATION,
} from './../../constants';

const modifyQueryFilter = () => { return {}; };
const gqlQueryName = ALL_LECTURERS_QUERY;
const queryResultObjectName = 'allLecturers';

class List extends Component {
  handleRefetch = () => {
    this.props[gqlQueryName]({ variables: { filter: modifyQueryFilter() } });
  }
  render() {
    const data = this.props[gqlQueryName];
    if (!data) return <div>No Data</div>;
    const { loading, error } = data;
    if (loading) return <div>Loading</div>;
    if (error) return <h1>ERROR</h1>;
    return (
      <div>
        {
          data[queryResultObjectName].map(item => (
            <div key={item.id}>{item.id}</div>
          ))
        }
        <button onClick={this.handleRefetch}>REFETCH</button>
      </div>
    );
  }
}

export default compose(
  graphql(
    gql[gqlQueryName],
    {
      name: gqlQueryName,
      options: (ownProps) => {
        return {
          variables: {
            filter: modifyQueryFilter(ownProps.allQueryFilter, ownProps.filterType),
          },
        };
      },
    },
  )
)(List);
