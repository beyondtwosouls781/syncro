import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_ORDERS_QUERY,
  DELETE_ORDER_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import ordersFields from './fields';
import ordersActions from './actions';
import ordersColumnData from './columnData';

const Orders = (props) => {

  const gqlAllQueryName = ALL_ORDERS_QUERY;
  const gqlAllQueryItemsName = 'allOrders';
  const gqlMutationDelete = DELETE_ORDER_MUTATION;
  const gqlMutationDeleteItemName = 'deleteOrder';
  const resulMutatiomtMessageSuccses = 'Заказ удален';

  return (
    <div>
      <ListMaterials
        {...props}
        actionsButtons={ordersActions}
        columnData={ordersColumnData}
        fields={ordersFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showActions={props.showActions === undefined ? true : props.showActions}
        // filterModifier={filterModifier}
      />
    </div>
  );
};

export default Orders;
