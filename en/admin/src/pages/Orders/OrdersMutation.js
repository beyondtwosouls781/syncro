import React from 'react';

import { output as outputUtils } from './../../utils';
import gql from './gql';

import {
  CREATE_ORDER_MUTATION,
  EDIT_ORDER_MUTATION,
  ORDER_QUERY,
} from './../../constants';

import MutationMaterial from './../../components/MutationMaterial';

import ordersFields from './fields';
import ordersActions from './actions';


const OrdersMutation = (props) => {
  const {
    mutationType,
  } = props;

  let resulMutatiomtMessageSuccses = '';
  let gqlMutationName = '';
  let mutationResultObjectName = '';
  let fields = null;
  let gqlQueryName = null;
  let gqlQueryItemName = null;

  const outputModifier = outputUtils.outputMutationModifier;
  if (mutationType === 'create') {
    gqlMutationName = CREATE_ORDER_MUTATION;
    fields = ordersFields.create;
    mutationResultObjectName = 'createOrder';
    resulMutatiomtMessageSuccses = 'Заказ создан';
  } else {
    gqlMutationName = EDIT_ORDER_MUTATION;
    fields = ordersFields.update;
    mutationResultObjectName = 'updateOrder';
    resulMutatiomtMessageSuccses = 'Заказ обновлен';
    gqlQueryName = ORDER_QUERY;
    gqlQueryItemName = 'Order';
  }

  return (
    <div>
      <MutationMaterial
        {...props}
        fields={fields}
        gql={gql}
        gqlMutationName={gqlMutationName}
        mutationResultObjectName={mutationResultObjectName}
        gqlQueryName={gqlQueryName}
        gqlQueryItemName={gqlQueryItemName}
        resulMutatiomtMessageSuccses={resulMutatiomtMessageSuccses}
      />
    </div>
  );
};

export default OrdersMutation;
