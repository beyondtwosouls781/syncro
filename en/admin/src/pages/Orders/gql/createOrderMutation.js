import gql from 'graphql-tag';

export default gql`
  mutation CreateOrderMutation(
      $userId: ID,
    ){
    createOrder(
      userId: $userId,
    ) {
      id
    }
  }
`;
