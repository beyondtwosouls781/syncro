import * as R from 'ramda';

import { format } from './../../utils';

const create = [
  {
    id: 'userId',
    label: 'Покупатель',
    // disabled: true,
    required: true,
    type: 'text',
  },
  {
    id: 'discountsIds',
    label: 'Скидка',
    // required: true,
    type: 'text',
  },
  {
    id: 'eventsIds',
    label: 'Мероприятия',
    required: true,
  },
  {
    id: 'paymentId',
    label: 'Оплата',
    required: true,
  },
  {
    id: 'participantsIds',
    label: 'Участники',
    required: true,
    type: 'text',
  },
  {
    id: 'tickets',
    label: 'Количество билетов',
    required: true,
  },
];

const update = create.slice();
update.push(
  {
    id: 'id',
    label: 'id',
    required: true,
    disabled: true,
    type: 'text',
  },
  // {
  //   id: 'alias_id',
  //   label: 'alias id',
  //   required: true,
  //   disabled: true,
  //   type: 'alias',
  // },
);

const view = update.slice();
view.push(
  {
    id: 'lecturers',
    label: 'Лекторы связанные с направлением',
    disabled: true,
    relation: 'lecturers',
    type: 'relation',
  },
  {
    id: 'lectures',
    label: 'Лекции связанные с направлением',
    disabled: true,
    relation: 'lectures',
    type: 'relation',
  },
  {
    id: 'courses',
    label: 'Курсы связанные с направлением',
    disabled: true,
    relation: 'courses',
    type: 'relation',
  },
  {
    id: 'cycles',
    label: 'Циклы связанные с направлением',
    disabled: true,
    relation: 'cycles',
    type: 'relation',
  },
);

const filter = [
  {
    id: 'title_contains',
    value: '',
    label: 'Покупатель',
    type: 'text',
  },
];

export default {
  create,
  update,
  filter,
  view,
};
