import {
  CREATE_USER_MUTATION,
  ALL_USERS_QUERY,
  EDIT_USER_MUTATION,
  USER_QUERY,
} from './../../../constants';
import createUserMutation from './createUserMutation';
import allUsersQuery from './allUsersQuery';
import editUserMutation from './editUserMutation';
import userQuery from './userQuery';

const gql = {
  [CREATE_USER_MUTATION]: createUserMutation,
  [ALL_USERS_QUERY]: allUsersQuery,
  [EDIT_USER_MUTATION]: editUserMutation,
  [USER_QUERY]: userQuery,
};

export default gql;
