import gql from 'graphql-tag';

export default gql`
  mutation UpdateUserMutation(
      $id: ID!,
      $email: String!,
      $firstName: String!,
      $lastName: String!,
      $password: String,
      $phone: String!,
      $role: UserRole,
    ){
    updateUser(
      id: $id,
      email: $email,
      firstName: $firstName,
      lastName: $lastName,
      password: $password,
      phone: $phone,
      role: $role,
    ) {
      id,
      createdAt,
      updatedAt,
      email,
      firstName,
      lastName,
      phone,
      role,
    }
  }
`;
