import gql from 'graphql-tag';

export default gql`
  mutation CreateReviewMutation(
      $userId: ID,
    ){
    createReview(
      userId: $userId,
    ) {
      id
    }
  }
`;
