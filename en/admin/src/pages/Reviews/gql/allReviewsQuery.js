import gql from 'graphql-tag';

export default gql`
  query AllReviewsQuery($filter: ReviewFilter, $first: Int, $skip: Int, $orderBy: ReviewOrderBy) {
    allReviews(
      filter: $filter,
      first: $first,
      skip: $skip,
      orderBy: $orderBy
    ) {
      id,
      createdAt,
      updatedAt,
    }
    _allReviewsMeta {
      count
    }
  }
`;
