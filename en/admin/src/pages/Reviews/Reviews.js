import React, { Component } from 'react';

import gql from './gql';

import {
  ALL_REVIEWS_QUERY,
  DELETE_REVIEW_MUTATION,
} from './../../constants';

import ListMaterials from './../../components/ListMaterials';

import reviewsFields from './fields';
import reviewsActions from './actions';
import reviewsColumnData from './columnData';

import tabs from './tabs';

const Reviews = (props) => {

  const gqlAllQueryName = ALL_REVIEWS_QUERY;
  const gqlAllQueryItemsName = 'allReviews';
  const gqlMutationDelete = DELETE_REVIEW_MUTATION;
  const gqlMutationDeleteItemName = 'deleteReview';
  const resulMutatiomtMessageSuccses = 'Отзыв удален';

  return (
    <div>
      <ListMaterials
        {...props}
        tabs={tabs}
        actionsButtons={reviewsActions}
        columnData={reviewsColumnData}
        fields={reviewsFields}
        gql={gql}
        gqlAllQueryName={gqlAllQueryName}
        gqlAllQueryItemsName={gqlAllQueryItemsName}
        gqlMutationDelete={gqlMutationDelete}
        gqlMutationDeleteItemName={gqlMutationDeleteItemName}
        showActions={props.showActions === undefined ? true : props.showActions}
        // filterModifier={filterModifier}
      />
    </div>
  );
};

export default Reviews;
