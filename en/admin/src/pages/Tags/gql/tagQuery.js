import gql from 'graphql-tag';

export default gql`
  query tagQuery($id: ID!) {
    Tag(
      id: $id
    ) {
      id,
      createdAt,
      updatedAt,
      alias {
        id,
        alias,
      },
      color,
      textColor,
      title,
    }
  }
`;
