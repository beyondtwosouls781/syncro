import React from 'react';
import {
  Switch,
  Route,
} from 'react-router-dom';
import { graphql, compose } from 'react-apollo';

import CreateOther from './CreateOther';
import EditOther from './EditOther';
import Other from './Other';
import List from './List';
import NotFound from './../NotFound';
// import withWrapper from './../../hoc/withWrapper';

const Others = (props) => {
  const { match } = props;
  return (
    <div>
      <Switch>
        <Route
          exact
          path={`${match.url}`}
          component={rest => (
            <List {...props} {...rest} title={''} />
          )}
        />
        <Route
          exact
          path={`${match.url}/view/:id`}
          component={rest => (
            <Other {...props} {...rest} title={''} />
          )}
        />
        <Route
          exact
          path={`${match.url}/create`}
          component={rest => (
            <CreateOther {...props} {...rest} />
          )}
        />
        <Route
          exact
          path={`${match.url}/edit/:id`}
          component={rest => (
            <EditOther {...props} {...rest} />
          )}
        />
        <Route
          component={rest => (
            <NotFound {...props} {...rest} />
          )}
        />
      </Switch>
    </div>
  );
};

// export default withWrapper(Others, { title: 'Материалы' });
export default Others;
