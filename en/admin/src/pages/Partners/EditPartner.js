import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo';
import * as R from 'ramda';

import gql from './gql';
import { edit } from './fields';
import FormBase from './../../components/FormBase';
import withProgress from './../../hoc/withProgress';
import withStateMutation from './../../hoc/withStateMutation';
import withSnackbar from './../../hoc/withSnackbar';
import { EDIT_PARTNER_MUTATION, PARTNER_QUERY } from './../../constants';

const Form = withProgress([EDIT_PARTNER_MUTATION, PARTNER_QUERY])(FormBase);

const editPartnerMutationResult = `${EDIT_PARTNER_MUTATION}Result`;
const editPartnerMutationError = `${EDIT_PARTNER_MUTATION}Error`;
const editPartnerMutationLoading = `${EDIT_PARTNER_MUTATION}Loading`;
const editPartnerMutationReset = `${EDIT_PARTNER_MUTATION}Reset`;

class EditPartner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: null,
      req: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps[editPartnerMutationResult]) {
      const id = nextProps[editPartnerMutationResult].data.updatePartner.id
      this.props.snackbar({ message: `Партнер обновлен - id: ${id}`, name: 'Success' });
      this.setState({
        req: false,
      });
      this.props.reqSuccess(id);
    }
    if (nextProps[editPartnerMutationError]) {
      this.props.snackbar(nextProps[editPartnerMutationError]);
      this.props[editPartnerMutationReset]();
      this.setState({
        req: false,
      }, () => {
        this.props.reqError(nextProps[editPartnerMutationError]);
      });
    }
  }
  confirm = async () => {
    const { fields } = this.state;
    this.props[EDIT_PARTNER_MUTATION]({
      variables: fields,
    });
  }
  handleSubmit(fields) {
    const state = {
      fields: R.clone(fields),
      req: true,
    };
    this.setState(state, () => {
      this.confirm();
    });
  }
  render() {
    const { req } = this.state;
    let fields = null;
    if (this.props[PARTNER_QUERY].Partner) {
      fields = R.clone(edit);
      const partner = this.props[PARTNER_QUERY].Partner;

      Object.keys(fields).forEach((key) => {
        fields[key].value = partner[key];
      });
    }

    return (
      <Form
        formTitle="Редактировать партнера"
        fields={fields || edit}
        type={'edit'}
        disabledAll={false}
        disabledActions={
          {
            save: false,
          }
        }
        showActions={
          {
            save: true,
          }
        }
        onSubmit={this.handleSubmit}
        req={req || this.props[PARTNER_QUERY].loading}
        {...this.props}
      />
    );
  }
}

EditPartner.defaultProps = {
  reqSuccess: () => {},
  reqError: () => {},
};

export default compose(
  graphql(gql[PARTNER_QUERY], {
    name: PARTNER_QUERY,
    options(ownProps) {
      const id = ownProps.match.params.id;
      return (
        {
          variables: {
            id,
          },
        }
      );
    },
  }),
  graphql(gql[EDIT_PARTNER_MUTATION], { name: EDIT_PARTNER_MUTATION }),
  withStateMutation({ name: EDIT_PARTNER_MUTATION }),
  withSnackbar(),
)(EditPartner);
