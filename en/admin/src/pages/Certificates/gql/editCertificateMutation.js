import gql from 'graphql-tag';

export default gql`
  mutation UpdateCertificateMutation(
      $id: ID!,
    ){
    updateCertificate(
      id: $id,
    ) {
      id,
      createdAt,
      updatedAt,
    }
  }
`;
