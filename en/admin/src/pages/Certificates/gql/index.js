import {
  CREATE_CERTIFICATE_MUTATION,
  ALL_CERTIFICATES_QUERY,
  EDIT_CERTIFICATE_MUTATION,
  CERTIFICATE_QUERY,
  DELETE_CERTIFICATE_MUTATION,
  CERTIFICATE_QUERY_WITH_RELATIONS,
} from './../../../constants';
import createCertificateMutation from './createCertificateMutation';
import allCertificatesQuery from './allCertificatesQuery';
import editCertificateMutation from './editCertificateMutation';
import certificateQuery from './certificateQuery';
import deleteCertificateMutation from './deleteCertificateMutation';
import certificateQueryWithRelations from './certificateQueryWithRelations';

const gql = {
  [CREATE_CERTIFICATE_MUTATION]: createCertificateMutation,
  [ALL_CERTIFICATES_QUERY]: allCertificatesQuery,
  [EDIT_CERTIFICATE_MUTATION]: editCertificateMutation,
  [CERTIFICATE_QUERY]: certificateQuery,
  [DELETE_CERTIFICATE_MUTATION]: deleteCertificateMutation,
  [CERTIFICATE_QUERY_WITH_RELATIONS]: certificateQueryWithRelations,
};

export default gql;
