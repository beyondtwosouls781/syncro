import gql from 'graphql-tag';

export default gql`
  query certificateQuery($id: ID!) {
    Certificate(
      id: $id
    ) {
      id,
      createdAt,
      updatedAt,
    }
  }
`;
