import validator from 'validator';
import { USER_ROLES } from './../enum';
import moment from 'moment';

import { GC_USER_ID, GC_USER_ROLE, GC_AUTH_TOKEN } from './../constants';

const hasOwnProperty = (obj, property) => Object.prototype.hasOwnProperty.call(obj, property);

const locale = 'ru-RU';
const userRoles = USER_ROLES.map(role => role.value);

const simpleRelationObjectTitle = (output, id, data) => {
  output[id] = [];
  output[`${id}_title`] = [];
  if (Array.isArray(data)) {
    data.forEach((item) => {
      output[id].push(item.id);
      output[`${id}_title`].push(item.title);
    });
  }
};

const simpleRelationObjectName = (output, id, data) => {
  output[id] = [];
  output[`${id}_title`] = [];
  if (Array.isArray(data)) {
    data.forEach((item) => {
      output[id].push(item.id);
      output[`${id}_title`].push(`${item.firstName} ${item.lastName}`);
    });
  }
};

const validate = (key, value, state) => {
  if (!value) {
    return false;
  }

  if (key === 'email' && (validator.isEmpty(value) || !validator.isEmail(value))) {
    return false;
  }

  if (key === 'password' && (validator.isEmpty(value) || !validator.isLength(value, { min: 8 }))) {
    return false;
  }

  if (key === 'repassword' && (state.password !== value)) {
    return false;
  }

  if (key === 'phone' && (validator.isEmpty(value) || !validator.isMobilePhone(value, locale))) {
    return false;
  }

  if (key === 'firstName' && (validator.isEmpty(value) || !validator.isAlpha(value, locale))) {
    return false;
  }

  if (key === 'lastName' && (validator.isEmpty(value) || !validator.isAlpha(value, locale))) {
    return false;
  }

  if (key === 'role' && (validator.isEmpty(value) || userRoles.indexOf(value) === -1)) {
    return false;
  }

  return true;
};

const generateStateFieldsObjectRelation = (field, data, output) => {
  const {
    relation,
    id,
    defaultValue,
  } = field;

  if (data[id] && data[`${id}_title`]) {
    output[id] = data[id];
    output[`${id}_title`] = data[`${id}_title`];
  } else {
    switch (relation) {
      case 'tags': {
        output[id] = [];
        output[`${id}_title`] = [];

        if (Array.isArray(data.tags)) {
          data.tags.forEach((item) => {
            output[id].push(item.id);
            output[`${id}_title`].push(item.title);
          });
        }
        break;
      }
      case 'events': {
        output.eventsSrc = data ? data.events : [];
        output[id] = [];
        output[`${id}_title`] = [];

        if (data && Array.isArray(data.events)) {
          data.events.forEach((item) => {
            output[id].push(item.id);
            output[`${id}_title`].push(moment(item.date).format('YYYY-MM-DD hh:mm'));
          });
        }
        break;
      }
      case 'lecturers': {
        simpleRelationObjectName(output, id, data.lecturers);
        break;
      }
      case 'lectures': {
        simpleRelationObjectTitle(output, id, data.lectures);
        break;
      }
      case 'courses': {
        simpleRelationObjectTitle(output, id, data.course || data.courses);
        break;
      }
      case 'cycles': {
        simpleRelationObjectTitle(output, id, data.cycle || data.cycles);
        break;
      }
      case 'recommendedCourses': {
        simpleRelationObjectTitle(output, id, data.recommendedCourses);
        break;
      }
      case 'recommendedCycles': {
        simpleRelationObjectTitle(output, id, data.recommendedCycles);
        break;
      }
      case 'recommendedLectures': {
        simpleRelationObjectTitle(output, id, data.recommendedLectures);
        break;
      }
      case 'discounts-select': {
        simpleRelationObjectTitle(output, id, data.discounts);
        break;
      }
      default: {
        output[id] = [];
        output[`${id}_title`] = [];
        if (Array.isArray(data[id])) {
          data[id].forEach((item) => {
            output[id].push(item.id);
            output[`${id}_title`].push(item.title);
          });
        }
      }
    }

    if (Array.isArray(output[id])) {
      const filtered = output[id].filter(item => item !== undefined && item !== null);
      if (filtered.length === 0 || output[id].length === 0) output[id] = data[id] || [];
    } else if (!output[id]) {
      output[id] = data[id] || [];
    }
  }
};

const generateStateFieldsObject = (fields, data = {}) => {

  const output = {
    errors: [],
  };
  fields.forEach((field) => {
    const {
      id,
      type,
      to,
      from,
      value,
    } = field;

    switch (type) {
      case 'checkbox': {
        output[id] = Boolean(data[id]);
        break;
      }
      case 'array': {
        output[id] = Array.isArray(data[id]) ? data[id].join('\n') : value || '';
        break;
      }
      case 'author': {
        if (data.author) {
          output.author = data.author ? `${data.author.firstName} ${data.author.lastName}` : '';
          output.authorId = data.author ? data.author.id : '';
        } else {
          output.authorId = localStorage.getItem(GC_USER_ID) || '';
          output.author = output.authorId;
        }
        break;
      }
      case 'alias': {
        const valueData = data.alias || value || {};
        output.alias = valueData.alias || '';
        output['alias_id'] = valueData.id || '';
        break;
      }
      case 'relation': {
        generateStateFieldsObjectRelation(field, data, output);
        break;
      }
      case 'tagsIds': {
        output[id] = [];
        output.tags = '';
        break;
      }
      case 'date': {
        if (to && from) {
          output[`${id}${from}`] = '';
          output[`${id}${to}`] = '';
        } else {
          output[id] = data ? (data[id] || '') : (value || '');
        }
        break;
      }
      default: {
        output[id] = data[id] ? data[id] : (value || '');
      }
    }
  });

  return output;
};

const validateFields = (fields, state) => {
  const output = [];
  fields.forEach((field) => {
    const { required, forceValidate } = field;
    if (field.required || field.forceValidate) {
      const { id, validValue } = field;
      const value = state[id];
      if (validValue) {
        if (validValue !== value) {
          output.push(id);
        }
      } else if (!validate(id, value, state)) {
        output.push(id);
      }
    }
  });
  return output;
};

const outputFields = (fields) => {
  const output = {};
  Object.keys(fields).forEach((key) => {
    let value = fields[key];
    if (value === null || value === undefined) return undefined;
    // if (typeof value === 'string' && validator.isNumeric(value)) value = parseInt(value, 10);

    switch (key) {
      case 'errors': {
        break;
      }
      case 'alias': {
        output.alias = {
          alias: fields.alias,
        };
        break;
      }
      case 'themes': {
        if (typeof value === 'string') {
          output.themes = value.split('\n');
        } else {
          output.themes = value;
        }
        break;
      }
      case 'duration': {
        output.duration = parseFloat(value);
        break;
      }
      case 'events': {
        output.events = value.map(n => ({
          date: new Date(`${n.date} ${n.time}`),
          quantityOfTickets: parseInt(n.tickets, 10),
          price: parseInt(n.price, 10),
          locationId: n.locationId,
          lecturersIds: n.lecturersIds,
        }));
        break;
      }
      case 'price': {
        output[key] = parseInt(value, 10);
        break;
      }
      case 'quantityOfTickets': {
        output[key] = parseInt(value, 10);
        break;
      }
      case 'rate': {
        output[key] = parseInt(value, 10);
        break;
      }
      case 'time': {
        output.date = new Date(`${fields.date} ${fields.time}`);
        break;
      }
      case 'public': {
        output.public = Boolean(value);
      }
      default: {
        output[key] = value;
      }
    }
  });
  return output;
};

const cloneAndAddField = (fields, field) => {
  const newFields = fields.slice();
  newFields.unshift(field);
  return newFields;
};

export default {
  cloneAndAddField,
  generateStateFieldsObject,
  validateFields,
  outputFields,
};
