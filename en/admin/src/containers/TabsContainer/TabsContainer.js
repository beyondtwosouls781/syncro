// import React from 'react';
// import PropTypes from 'prop-types';
// import { withStyles } from 'material-ui/styles';
// import AppBar from 'material-ui/AppBar';
// import Tabs, { Tab } from 'material-ui/Tabs';
// import PhoneIcon from 'material-ui-icons/Phone';
// import FavoriteIcon from 'material-ui-icons/Favorite';
// import PersonPinIcon from 'material-ui-icons/PersonPin';
// import HelpIcon from 'material-ui-icons/Help';
// import ShoppingBasket from 'material-ui-icons/ShoppingBasket';
// import ThumbDown from 'material-ui-icons/ThumbDown';
// import ThumbUp from 'material-ui-icons/ThumbUp';
//
// import ContentTable from './ContentTable';
//
// function TabContainer(props) {
//   return <div style={{ padding: 8 * 3 }}>{props.children}</div>;
// }
//
// TabContainer.propTypes = {
//   children: PropTypes.node.isRequired,
// };
//
// const styles = theme => ({
//   root: {
//     flexGrow: 1,
//     marginTop: theme.spacing.unit * 3,
//     backgroundColor: theme.palette.background.paper,
//   },
// });
//
// class ScrollableTabsButtonForce extends React.Component {
//   state = {
//     value: 0,
//   };
//
//   handleChange = (event, value) => {
//     this.setState({ value });
//   };
//
//   render() {
//     const { classes } = this.props;
//     const { value } = this.state;
//
//     return (
//       <div className={classes.root}>
//         <AppBar position="static">
//           <Tabs
//             value={value}
//             onChange={this.handleChange}
//             scrollable
//             scrollButtons="on"
//           >
//             <Tab label="Активные" />
//             <Tab label="Не опубликованные" />
//             <Tab label="Архив" />
//           </Tabs>
//         </AppBar>
//         {value === 0 && <TabContainer><ContentTable /></TabContainer>}
//         {value === 1 && <TabContainer><ContentTable /></TabContainer>}
//         {value === 2 && <TabContainer><ContentTable /></TabContainer>}
//       </div>
//     );
//   }
// }
//
// ScrollableTabsButtonForce.propTypes = {
//   classes: PropTypes.object.isRequired,
// };
//
// export default withStyles(styles)(ScrollableTabsButtonForce);

import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import TabContainer from './TabContainer';

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing.unit * 3,
    backgroundColor: theme.palette.background.paper,
  },
});

class TabsContainer extends Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, tabs } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={this.handleChange}
            scrollable
            scrollButtons="on"
          >
            {
              tabs.map((tab, index) => (
                <Tab  key={index} label={tab.label} />
              ))
            }
          </Tabs>
        </AppBar>
        <TabContainer>
          {this.props.children[value]}
        </TabContainer>
      </div>
    );
  }
}

TabsContainer.defaultProps = {
  tabs: [],
};

export default withStyles(styles)(TabsContainer);
