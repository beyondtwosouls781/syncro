import React from 'react';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';
import AppDrawerNavItem from './AppDrawerNavItem';

const AppDrawerNavList = (props, context) => {
  return (
    <List>
      <AppDrawerNavItem
        to={'/schedule'}
        title={'Расписание'}
      />
      <AppDrawerNavItem
        to={'/messages'}
        title={'Уведомления'}
      />
      <Divider />
      <AppDrawerNavItem
        // to={'/catalog'}
        title={'Каталог'}
      >
        <List>
          <AppDrawerNavItem
            to={'/catalog/lectures'}
            title={'Лекции'}
          />
          <AppDrawerNavItem
            to={'/catalog/courses'}
            title={'Курсы'}
          />
          <AppDrawerNavItem
            to={'/catalog/cycles'}
            title={'Циклы'}
          />
          <AppDrawerNavItem
            to={'/catalog/certificates'}
            title={'Сертификаты'}
          />
          <AppDrawerNavItem
            to={'/catalog/subscriptions'}
            title={'Абонементы'}
          />
        </List>
      </AppDrawerNavItem>

      <AppDrawerNavItem
        // to={'/catalog'}
        title={'Содержимое сайта'}
      >
        <List>
          <AppDrawerNavItem
            to={'/content/main'}
            title={'Главная страница'}
          />
          <AppDrawerNavItem
            to={'/content/howwork-mainpage'}
            title={'Как проходит синхронизация - Главная страница'}
          />
          <AppDrawerNavItem
            to={'/content/howwork-course'}
            title={'Как проходит синхронизация - Страница Курс'}
          />
          <AppDrawerNavItem
            to={'/content/howwork-lecture'}
            title={'Как проходит синхронизация - Страница Лекции'}
          />
          <AppDrawerNavItem
            to={'/content/maingallery'}
            title={'Галерея - Фотографии с мероприятий'}
          />
          <AppDrawerNavItem
            to={'/content/about'}
            title={'О проекте'}
          />
          {/* <AppDrawerNavItem
            to={'/pages/courses'}
            title={'Курсы'}
          />
          <AppDrawerNavItem
            to={'/catalog/cycles'}
            title={'Циклы'}
          />
          <AppDrawerNavItem
            to={'/catalog/certificates'}
            title={'Сертификаты'}
          />
          <AppDrawerNavItem
            to={'/catalog/subscriptions'}
            title={'Абонементы'}
          /> */}
        </List>
      </AppDrawerNavItem>

      <AppDrawerNavItem
        to={'/discounts'}
        title={'Скидки'}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/lecturers'}
        title={'Лекторы'}
      />
      <AppDrawerNavItem
        to={'/tags'}
        title={'Направления'}
      />
      <AppDrawerNavItem
        to={'/locations'}
        title={'Локации'}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/orders'}
        title={'Заказы'}
      />
      <AppDrawerNavItem
        to={'/certificates'}
        title={'Сертификаты'}
      />
      <AppDrawerNavItem
        to={'/subscriptions'}
        title={'Абонементы'}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/users'}
        title={'Пользователи'}
      />
      <AppDrawerNavItem
        to={'/reviews'}
        title={'Отзывы'}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/partners'}
        title={'Партнеры'}
      />
      <AppDrawerNavItem
        to={'/galleries'}
        title={'Галереи'}
      />
      <Divider />
      <AppDrawerNavItem
        to={'/users/administrators'}
        title={'Администраторы  '}
      />
      <AppDrawerNavItem
        to={'/users/moderators'}
        title={'Модераторы'}
      />
      <AppDrawerNavItem
        to={'/users/curators'}
        title={'Координаторы'}
      />
      <Divider />
      {/* <AppDrawerNavItem
        to={'/settings'}
        title={'Система'}
      /> */}
      <AppDrawerNavItem
        to={'/acquirings'}
        title={'Эквайринг'}
      />
      <AppDrawerNavItem
        // to={'/catalog'}
        title={'Настройки'}
      >
        <List>
          <AppDrawerNavItem
            to={'/settings/mailchimp'}
            title={'MailChimp API'}
          />
          <AppDrawerNavItem
            to={'/settings/mandrill'}
            title={'Mandrill API'}
          />
          <AppDrawerNavItem
            to={'/settings/smtp'}
            title={'SMTP'}
          />
          <AppDrawerNavItem
            to={'/settings/contacts'}
            title={'Контакты'}
          />
          <AppDrawerNavItem
            to={'/settings/acquiring'}
            title={'Эквайринг'}
          />
        </List>
      </AppDrawerNavItem>
      <AppDrawerNavItem
        // to={'/catalog'}
        title={'Шаблон'}
      >
        <List>
          <AppDrawerNavItem
            to={'/template/scripts'}
            title={'Скрипты'}
          />
          <AppDrawerNavItem
            to={'/template/styles'}
            title={'Стили'}
          />
          <AppDrawerNavItem
            to={'/template/blocks'}
            title={'Блоки'}
          />
        </List>
      </AppDrawerNavItem>
    </List>
  );
};

export default AppDrawerNavList;
