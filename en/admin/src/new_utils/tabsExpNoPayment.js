const tabsExp = [
  {
    label: 'Активные',
    id: 'active',
  },
  {
    label: 'Закончились',
    id: 'expired',
  },
  {
    label: 'Не оплаченные',
    id: 'no_payment',
  },
];

export default tabsExp;
