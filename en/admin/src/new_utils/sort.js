const sort = (data, orderBy, order) => {
  if (orderBy === 'createdAt' || orderBy === 'updatedAt' || orderBy === 'date') {
    return (order === 'desc')
      ? data.sort((a, b) => (b.src[orderBy] < a.src[orderBy] ? -1 : 1))
      : data.sort((a, b) => (a.src[orderBy] < b.src[orderBy] ? -1 : 1));
  }

  return (order === 'desc')
    ? data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
    : data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));
};

export default sort;
