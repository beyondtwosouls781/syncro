export const GC_USER_ID = 'gc-user-id';
export const GC_AUTH_TOKEN = 'gc-auth-token';
export const GC_USER_ROLE = 'gc-user-role';
export const ITEMS_PER_PAGE = 1000;

export const LOGGED_IN_USER_WITH_ROLE = 'loggedInUserWithRole';
export const AUTHENTICATE_USER_MUTATION = 'authenticateUserMutation';
export const CREATE_USER_MUTATION = 'createUserMutation';
export const EDIT_USER_MUTATION = 'editUserMutation';
export const ALL_USERS_QUERY = 'allUsersQuery';
export const USER_QUERY = 'userQuery';
export const USER_CERTIFICATE_AND_SUBSCRIPTION_QUERY = 'userCeriticateAndSubscriptionQuery';
export const DELETE_USER_MUTATION = 'deleteuserMutation';
export const USER_CLIENT_QUERY_WITH_RELATIONS = 'userClientQueryWithRelations';
export const USER_SYSTEM_QUERY_WITH_RELATIONS = 'userSystemQueryWithRelations';
export const CREATE_USER_WITHOUT_PASS_MUTATION = 'createUserWithoutPassMutation';


export const CREATE_TAG_MUTATION = 'createTagMutation';
export const EDIT_TAG_MUTATION = 'editTagMutation';
export const ALL_TAGS_QUERY = 'allTagsQuery';
export const TAG_QUERY = 'tagQuery';
export const DELETE_TAG_MUTATION = 'tagDeleteMutation';
export const TAG_QUERY_WITH_RELATIONS = 'tagQueryWithRelations';

export const CREATE_LOCATION_MUTATION = 'createLocationMutation';
export const EDIT_LOCATION_MUTATION = 'editLocationMutation';
export const ALL_LOCATIONS_QUERY = 'allLocationsQuery';
export const LOCATION_QUERY = 'locationQuery';
export const DELETE_LOCATION_MUTATION = 'deleteLocationMutation';
export const LOCATION_QUERY_WITH_RELATIONS = 'locationQueryWithRelations';

export const CREATE_LECTURER_MUTATION = 'createLecturerMutation';
export const EDIT_LECTURER_MUTATION = 'editLecturerMutation';
export const ALL_LECTURERS_QUERY = 'allLecturersQuery';
export const LECTURER_QUERY = 'lecturerQuery';
export const DELETE_LECTURER_MUTATION = 'deleteLecturerMutation';
export const LECTURER_QUERY_WITH_RELATIONS = 'lecturerQueryWithRelations';

export const CREATE_PARTNER_MUTATION = 'createPartnerMutation';
export const EDIT_PARTNER_MUTATION = 'editPartnerMutation';
export const ALL_PARTNERS_QUERY = 'allPartnersQuery';
export const PARTNER_QUERY = 'partnerQuery';
export const DELETE_PARTNER_MUTATION = 'deletePartnerMutation';
export const PARTNER_RELATIONS_QUERY = 'partnerRelationQuery';
export const PARTNER_QUERY_WITH_RELATIONS = 'partnerRelationQuery';

export const CREATE_OTHER_MUTATION = 'createOtherMutation';
export const EDIT_OTHER_MUTATION = 'editOtherMutation';
export const ALL_OTHERS_QUERY = 'allOthersQuery';
export const OTHER_QUERY = 'otherQuery';
export const DELETE_OTHER_MUTATION = 'deleteOtherMutation';
export const OTHER_RELATIONS_QUERY = 'otherRelationQuery';

export const LECTURE_QUERY = 'lectureQuery'
export const COURSE_QUERY = 'courseQuery';
export const CYCLE_QUERY = 'cycleQuery';

export const ALL_LECTURES_QUERY = 'allLecturesQuery';
export const ALL_COURSE_QUERY = 'allCoursesQuery';
export const ALL_CYCLES_QUERY = 'allCyclesQuery';

export const ALL_LECTURES_QUERY_SHORT = 'allLecturesQueryShort';
export const ALL_COURSES_QUERY_SHORT = 'allCoursesQueryShort';
export const ALL_CYCLES_QUERY_SHORT = 'allCyclesQueryShort';

export const EDIT_LECTURE_MUTATION = 'editLectureMutation';
export const EDIT_COURSE_MUTATION = 'editCourseMutation';
export const EDIT_CYCLE_MUTATION = 'editCycleMutation';

export const CREATE_LECTURE_MUTATION = 'createLectureMutation';
export const CREATE_COURSE_MUTATION = 'createCourseMutation';
export const CREATE_CYCLE_MUTATION = 'createCycleMutation';

export const DELETE_LECTURE_MUTATION = 'deleteLectureMutation';
export const DELETE_COURSE_MUTATION = 'deleteCourseMutation';
export const DELETE_CYCLE_MUTATION = 'deleteCycleMutation';

export const CREATE_DISCOUNT_MUTATION = 'createDiscountMutation';
export const EDIT_DISCOUNT_MUTATION = 'editDiscountMutation';
export const ALL_DISCOUNTS_QUERY = 'allDiscountsQuery';
export const DISCOUNT_QUERY = 'discountQuery';
export const DELETE_DISCOUNT_MUTATION = 'deleteDiscountMutation';
export const DISCOUNT_QUERY_WITH_RELATIONS = 'discountRelationQuery';

export const ALL_DISCOUNTS_OF_CERTIFICATE = 'allDiscountsOfCertificate';
export const ALL_DISCOUNTS_OF_SUBSCRIPTION = 'allDiscountsOfSubscription';
export const ALL_DISCOUNTS_OF_LECTURES = 'allDiscountsOfLectures';

export const ALL_DISCOUNTS_QUERY_SHORT = 'allDiscontsQueryShort';
export const ALL_DISCOUNTS_RELAIONS_QUERY = 'allDiscountsRelationsQuery'
export const ALL_LECTURERS_QUERY_SHORT = 'allLecturersQueryShort';
export const ALL_LOCATIONS_QUERY_SHORT = 'allLocationsQueryShort';
export const ALL_TAGS_QUERY_SHORT = 'allTagsQueryShort';
export const ALL_ACQUIRINGS_QUERY_SHORT = 'allAcquiringsQueryShort';
export const ALL_CURATOR_QUERY_SHORT = 'allCuratorQueryShort';

export const SUBSCRIPTION_QUERY = 'subscriptionQuery';
export const CERTIFICATE_QUERY = 'certificateQuery';
export const ALL_SUBSCRIPTIONS_QUERY = 'allSubscriotionsQuery';
export const ALL_CERTIFICATES_QUERY = 'allCertificatesQuery';
export const ALL_SUBSCRIPTIONS_QUERY_SHORT = 'allSubscriotionsQueryShort';
export const ALL_CERTIFICATES_QUERY_SHORT = 'allCertificatesQueryShort';
export const EDIT_SUBSCRIPTION_MUTATION = 'editSubscriptionMutation';
export const EDIT_CERTIFICATE_MUTATION = 'editCertificateMutation';
export const CREATE_SUBSCRIPTION_MUTATION = 'createSubscriptionMutation';
export const CREATE_CERTIFICATE_MUTATION = 'createCertificateMutation';
export const CREATE_CERTIFICATE_GIFT_MUTATION = 'createCertificateGiftMutation';
export const DELETE_CERTIFICATE_MUTATION = 'deleteCertificateMutation';
export const DELETE_SUBSCRIPTION_MUTATION = 'deleteSubscriptionMutation';

export const SUBSCRIPTION_PRODUCT_QUERY = 'subscriptionProductQuery';
export const CERTIFICATE_PRODUCT_QUERY = 'certificateProductQuery';
export const ALL_SUBSCRIPTION_PRODUCTS_QUERY_SHORT = 'allSubscriotionsProductQuery';
export const ALL_CERTIFICATE_PRODUCTS_QUERY_SHORT = 'allCertificatesProductQuery';
export const ALL_SUBSCRIPTION_PRODUCTS_QUERY = 'allSubscriotionsProductQueryShort';
export const ALL_CERTIFICATES_PRODUCT_QUERY = 'allCertificatesProductQueryShort';
export const EDIT_SUBSCRIPTION_PRODUCT_MUTATION = 'editSubscriptionProductMutation';
export const EDIT_CERTIFICATE_PRODUCT_MUTATION = 'editCertificateProductMutation';
export const CREATE_SUBSCRIPTION_PRODUCT_MUTATION = 'createSubscriptionProductMutation';
export const CREATE_CERTIFICATE_PRODUCT_MUTATION = 'createCertificateProductMutation';
export const DELETE_CERTIFICATE_PRODUCT_MUTATION = 'deleteCertificateProductMutation';
export const DELETE_SUBSCRIPTION_PRODUCT_MUTATION = 'deleteSubscriptionProductMutation';

export const CREATE_EVENT_MUTATION = 'createEventMutation';
export const EDIT_EVENT_MUTATION = 'editEventMutation';
export const ALL_EVENTS_QUERY = 'allEventsQuery';
export const ALL_EVENTS_QUERY_SHORT = 'allEventsQueryShort';
export const EVENT_QUERY = 'eventQuery';
export const DELETE_EVENT_MUTATION = 'deleteEventMUtation';
export const ALL_EVENTS_OF_LECTURE_QUERY = 'allEventsOfLectureQuery';
export const EVENT_REGISTERED_PARTICIPANTS_QUERY = 'eventRegisteredParticipantsQuery';
export const PARTICIPANTS_OF_EVENT_QUERY = 'participantsOfEventQuery';

export const TICKETS_OF_EVENT_QUERY = 'ticketsOfEventQuery';

export const LECTURE_QUERY_WITH_RELATIONS = 'lectureQueryWithRelations';
export const LECTURE_FOR_COURSE_QUERY_WITH_RELATIONS = 'lectureForCourseQueryWithRelations';
export const LECTURE_FOR_CYCLE_QUERY_WITH_RELATIONS = 'lectureForCycleQueryWithRelations';
export const COURSE_QUERY_WITH_RELATIONS = 'courseQueryWithRelations';
export const CYCLE_QUERY_WITH_RELATIONS = 'cycleQueryWithRelations';
export const CERTIFICATE_PRODUCT_QUERY_WITH_RELATIONS = 'certificateProductQueryWithRelations';
export const SUBSCRIPTION_PRODUCT_QUERY_WITH_RELATIONS = 'subscriptionProductQueryWithRelations';

export const CREATE_ORDER_MUTATION = 'createOrderMutation';
export const EDIT_ORDER_MUTATION = 'editOrderMutation';
export const ALL_ORDERS_QUERY = 'allOrdersQuery';
export const ORDER_QUERY = 'orderQuery';
export const ORDER_QUERY_WITH_RELATIONS = 'orderQueryWithRelarions';
export const DELETE_ORDER_MUTATION = 'deleteOrderMUtation';
export const CHECK_ORDER_EVENTS = 'checkOrderEvents';
export const CHECK_ORDER_CERT_AND_SUB = 'checkOrderCertAndSub';
// export const CREATE_CERTIFICATE_MUTATION = 'createCertificateMutation';
// export const EDIT_CERTIFICATE_MUTATION = 'editCertificateMutation';
// export const ALL_CERTIFICATES_QUERY = 'allCertificatesQuery';
// export const CERTIFICATE_QUERY = 'certificateQuery';
// export const DELETE_CERTIFICATE_MUTATION = 'deleteCertificateMUtation';
export const CERTIFICATE_QUERY_WITH_RELATIONS = 'certificateQueryWithRelations';
export const SUBSCRIPTION_QUERY_WITH_RELATIONS = 'subscriptionQueryWithRelations';

export const CREATE_REVIEW_MUTATION = 'createReviewMutation';
export const EDIT_REVIEW_MUTATION = 'editReviewMutation';
export const ALL_REVIEWS_QUERY = 'allReviewsQuery';
export const REVIEW_QUERY = 'reviewQuery';
export const REVIEW_QUERY_WITH_RELATIONS = 'reviewQueryWithRelarions';
export const DELETE_REVIEW_MUTATION = 'deleteReviewMUtation';

export const CREATE_MAINPAGE_MUTATION = 'createMainPageMutation';
export const EDIT_MAINPAGE_MUTATION = 'updateMainPageMUtation';
export const MAINPAGE_QUERY = 'mainPageQuery';
export const ALL_MAINPAGE_QUERY = 'allMainPAgeQuery';

export const ALL_TEMPLATES_QUERY = 'allTemplatesQuery';
export const CREATE_TEMPLATE_MUTATION = 'createTemplateMutation';
export const EDIT_TEMPLATE_MUTATION = 'updateTemplateMutation';
export const TEMPLATE_QUERY = 'templateQuery';

export const ALL_SETTINGS_QUERY = 'allSettingsQuery';
export const SETTING_QUERY = 'settingQuery';
export const CREATE_SETTINGS_MUTATION = 'createSettingsMutation';
export const EDIT_SETTINGS_MUTATION = 'editSettingsMutation';


export const ALL_ACQUIRINGS_QUERY = 'allAcquiringsQuery';
export const ACQUIRING_QUERY = 'acquiringQuery';
export const CREATE_ACQUIRING_MUTATION = 'createAcquiringMutation';
export const EDIT_ACQUIRING_MUTATION = 'updateAcquiringMutation';
export const DELETE_ACQUIRING_MUTATION = 'deleteAcquiringMutation';
export const ACQUIRING_QUERY_WITH_RELATIONS = 'acquiringQueryWithRelations';

// export const CREATE_ACQUIRING_SETTINGS_MUTATION = 'createAcquiringSettingsMutation';
// export const EDIT_ACQUIRING_SETTINGS_MUTATION = 'updateAcquiringMutation';
// export const CREATE_MAILCHIMP_SETTINGS_MUTATION = 'createMailchimpSettingsMutation';
// export const EDIT_MAILCHIMP_SETTINGS_MUTATION = 'updateMailchimpSettingsMutation';
// export const CREATE_MANDRILL_SETTINGS_MUTATION = 'createMandrillSettingsMutation';
// export const EDIT_MANDRILL_SETTINGS_MUTATION = 'updateMandrillSettingsMutation';
// export const CREATE_SMTP_SETTINGS_MUTATION = 'createSmtpSettingsMutation';
// export const EDIT_SMTP_SETTINGS_MUTATION = 'updateSmtpSettingsMutation';
// export const CREATE_CONTACTS_SETTINGS_MUTATION = 'createConatctsSettingsMutation';
// export const EDIT_CONTACTS_SETTINGS_MUTATION = 'updateConatctsSettingsMutation';

export const ALL_ABOUTPAGE_QUERY = 'allAboutPageQuery';
export const CREATE_ABOUTPAGE_MUTATION = 'createAboutPageMutation';
export const EDIT_ABOUTPAGE_MUTATION = 'updateAboutPageMutation';
export const ABOUTPAGE_QUERY = 'aboutPageQuery';

export const ALL_HOWWORKPAGES_QUERY = 'allHowWorkPageQuery';
export const CREATE_HOWWORKPAGE_MUTATION = 'createHowWorkPageMutation';
export const EDIT_HOWWORKPAGE_MUTATION = 'updateHowWorkPageMutation';
export const HOWWORKPAGE_QUERY = 'howWorkQuery';

export const CREATE_GALLERY_MUTATION = 'createGalleryMutation';
export const ALL_GALLERIES_QUERY = 'allGalleriesQuery';
export const EDIT_GALLERY_MUTATION = 'updateGalleryMutation';
export const GALLERY_QUERY = 'galleryQuery';
export const DELETE_GALLERY_MUTATION = 'deleteGalleryMutation';
export const GALLERY_QUERY_WITH_RELATIONS = 'galleryQueryWithRelations';
export const GALLERY_OF_TYPE_QUERY = 'galleryOftypeQuery';

export const ALL_IMAGES_QUERY = 'allImagesQuery';
export const DELETE_IMAGE_MUTATION = 'deleteImageMutation';
export const CREATE_IMAGE_MUTATION = 'createImageMutation';
export const UPDATE_IMAGE_MUTATION = 'updateImageMutation';
export const IMAGE_QUERY = 'imageQuery';
export const IMAGE_QUERY_WITH_RELATIONS = 'imageQueryWithRelqtions';

export const ALL_CERTIFICATES_OF_USER_QUERY = 'allCertificatesOfUserQuery';
export const ALL_SUSCRIPTIONS_OF_USER_QUERY = 'allSubscriptionsOfUserQuery';

export const ALL_MESSAGES_QUERY = 'allMessagesQuery';
export const MESSAGE_QUERY = 'messageQuery';
export const CREATE_MESSAGE_MUTATION = 'createMessageMutation';
export const EDIT_MESSAGE_MUTATION = 'updateMessageMutation';
export const DELETE_MESSAGE_MUTATION = 'deleteMessageMutation';
export const MESSAGE_QUERY_WITH_RELATIONS = 'nessageQueryWithRelations';

export const USER_RESET_PASS_MUTATION = 'userResetPassMutation';
export const USER_RESET_PASS_SEND_EMAIL_MUTATION = 'userResetPassSendEmailMutation';

export const AGREEMENT_QUERY = 'agreementQuery';
export const CREATE_AGREEMENT_MUTATION = 'createagreementMutation';
export const EDIT_AGREEMENT_MUTATION = 'updateagreementMutation';

export const TERM_OF_USE_QUERY = 'termOfUseQuery';
export const CREATE_TERM_OF_USE_MUTATION = 'createTermOfUseMutation';
export const EDIT_TERM_OF_USE_MUTATION = 'updateTermOfUseMutation';
export const EDIT_ALIAS_MUTATION = 'updateALiasMutation';

export const ALL_PAYMENTS_QUERY = 'allPaymentsQuery';
export const ALL_PAYMENTS_QUERY_SHORT = 'allPaymentsQueryShort';
export const PAYMENT_QUERY_WITH_RELATIONS = 'paymentQueryWithRelations';
export const EDIT_PAYMENT_MUTATION = 'updatePaymentMutation';
export const PAYMENT_QUERY = 'paymentQuery';
export const DELETE_PAYMENT_MUTATION = 'deletePaymentMutation';
export const CREATE_PAYMENT_MUTATION = 'createPaymentMutation';
export const ALL_PAYMENTS_RELAIONS_QUERY = 'allPaymentsRelationsQuery';

export const ATTENDED_USER_MUTATION = 'attendedUserMutation';

export const ALL_LECTURES_FOR_RECOMENDATIONS_QUERY_SHORT = 'allLecturesForRecomendationsQuery';
