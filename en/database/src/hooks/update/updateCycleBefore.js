const { fromEvent } = require('graphcool-lib');

const updateLecturesTypeMuration = requre('./updateLecturesTypeMuration');

module.exports = (event) => {
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');
  const { data: { lecturesIds } } = event;

  return updateLecturesTypeMuration(api, lecturesIds, 'LECTURE_FOR_CYCLE')
    .then(() => event)
    .catch((err) => {
      console.error(err);
      return event;
    });
};
