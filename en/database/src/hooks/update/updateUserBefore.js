const bcrypt = require('bcryptjs');

const clearPhone = require('./../../lib/clearPhone');
const upperCaseFirstSymbol = require('./../../lib/upperCaseFirstSymbol');
const config = require('./../../config');

const hashPassword = (password) => {
  const salt = bcrypt.genSaltSync(config.SALT_ROUNDS);
  return bcrypt.hash(password, salt);
};

module.exports = (event) => {
  const { data } = event;
  const { phone, firstName, lastName, secondName, email, password } = data;

  if (phone) event.data.phone = clearPhone(phone);
  if (firstName) event.data.firstName = upperCaseFirstSymbol(firstName);
  if (secondName) event.data.secondName = upperCaseFirstSymbol(secondName);
  if (lastName) event.data.lastName = upperCaseFirstSymbol(lastName);

  if (password) {
    return hashPassword(password)
      .then((hash) => {
        event.data.password = hash;
        return event;
      })
      .catch((error) => {
        console.log(error);
        return { error: 'An unexpected error occured.' };
      });
  }

  return event;
};
