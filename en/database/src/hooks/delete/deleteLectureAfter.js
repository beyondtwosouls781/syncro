const { fromEvent } = require('graphcool-lib');
const deleteAliasOfEntityMutation = require('./deleteAliasOfEntityMutation');

module.exports = (event) => {
  const graphcool = fromEvent(event);
  const api = graphcool.api('simple/v1');
  const { data: { id } } = event;
  return deleteAliasOfEntityMutation(`lecture: { id: "${id}" }`, api);
};
