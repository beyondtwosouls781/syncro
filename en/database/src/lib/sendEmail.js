require('isomorphic-fetch');

const sendEmail = (event, data, path) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(Object.assign(event, { data })),
  };
  return fetch(
    `${process.env.WEBHOOK_HOST}${path}`,
    options,
  );
};

module.exports = sendEmail;
