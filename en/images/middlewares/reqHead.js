const request = require('request');
const {
  FILE_API_ENDPOINT,
  CONTENT_LENGTH,
} = require('./../config');

const parseFilename = (disposition) => {
  let filename = undefined;
  if (disposition) {
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      filename = matches[1].replace(/['"]/g, '');
    }
  }
  return filename;
};

module.exports = (req, res, next) => {
  const { params: { secret } } = req;

  request.head(`${FILE_API_ENDPOINT}/${secret}`, (err, response, body) => {
    if (err) {
      next(err);
      return null;
    }

    const {
      'content-disposition': disposition,
      'content-length': contentLength,
      'content-type': contentType,
    } = response.headers;

    if (contentLength > CONTENT_LENGTH) {
      const error = new Error('File too big');
      error.status = 400;
      next(error);
      return null;
    }

    if (!contentType.includes('image')) {
      const error = new Error('File not an image');
      error.status = 400;
      next(error);
      return null;
    }

    if (
      contentType === 'image/gif' ||
      contentType === 'image/svg+xml'
    ) {
      const error = new Error('This type of image can not be transform');
      error.status = 400;
      next(error);
      return null;
    }

    const filename = parseFilename(disposition);
    if (!filename) {
      const error = new Error('Filename not find');
      error.status = 400;
      next(error);
      return null;
    }
    const {
      crop,
      resize,
    } = res.locals;

    res.locals.contentLength = contentLength;
    res.locals.contentType = contentType;
    res.locals.disposition = disposition;
    res.locals.filename = filename;
    res.locals.filenameForSave = `${crop || 'NO_CROP'}_${resize || 'NO_RESIZE'}_${decodeURIComponent(filename)}`;
    res.locals.headers = response.headers;
    next();
  });
};
