const PROJECT_ID = 'cjd67we9401sw0155segppqze';
const DOMAIN = 'synchronize.ru';

module.exports = {
  SIMPLE_API_ENDPOINT: `http://api-dev.${DOMAIN}/simple/v1/${PROJECT_ID}`,
  FILE_API_ENDPOINT: `http://file-dev.${DOMAIN}/file`,
  FILE_HOST: `http://file-dev.${DOMAIN}/file`,
  IMAGE_HOST: `http://images-dev.${DOMAIN}/images`,
  SUBSCRIPTIONS__API_ENDPOINT: `ws://api-dev.${DOMAIN}/subscriptions/v1/${PROJECT_ID}`,
  REDIS: {
    HOST: '127.0.0.1',
    PORT: '6379',
    PASS: '28b51f7a0b573fc6a71b79de5635c97484e67a8fee8dd430a9b1b60f8475c9f9',
  },
  COOKIE_MAX_AGE: 30 * 24 * 60 * 60 * 1000,
  ROOT_TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTc1OTMzNzgsImNsaWVudElkIjoiY2pkMGRxc3R0MDAwMDAxNTVuaXplcmEyNCIsInByb2plY3RJZCI6ImNqZDY3d2U5NDAxc3cwMTU1c2VncHBxemUiLCJwZXJtYW5lbnRBdXRoVG9rZW5JZCI6ImNqZDY3d3d4aDAybWEwMTU1b3J3MHRyNm4ifQ.RMeeaEj4rw0stsjrSC01ZRVL7rkCim3ICz2eQ-o4ujk',
  // MAIL_HOST: `http://mail-dev.${DOMAIN}`,
  MAIL_HOST: 'http://localhost:60003',
  // sec
  RESET_PASS_TOKEN_EXPIRES: 3600,
  URL: `http://dev.${DOMAIN}`,
  SECRET: '83c7ed9f6234b144483258ea2c58942ebe418f57b027a3355cdae5267c7553fa',
};
