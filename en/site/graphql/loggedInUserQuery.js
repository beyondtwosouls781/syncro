const { graphQlFetch } = require('./graphql');

const loggedInUserQuery = (req) => {
  return graphQlFetch({ req, query: 'loggedInUserQuery' })
    .then(({ data, errors, extensions }) => {
      return {
        data,
        errors,
        extensions,
      };
    });
};

module.exports = loggedInUserQuery;
