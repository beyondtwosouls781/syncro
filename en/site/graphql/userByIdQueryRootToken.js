const { graphQlFetch } = require('./graphql');

const { ROOT_TOKEN } = require('./../config');

const userByIdQueryRootToken = (req, id) => {
  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'userByIdQuery',
    variables: {
      id,
    },
  });
};

module.exports = userByIdQueryRootToken;
