const { graphQlFetch } = require('./../../graphql');
const validator = require('./../../lib/validator');
const redis = require('./../../db/redis');
const { randomBytesAsync } = require('./../../lib/crypto');
const acquiringSettingQuery = require('./../../graphql/acquiringSettingQuery');
const checkAvailabilityOfUserMutation = require('./../../graphql/checkAvailabilityOfUserMutation');
const { checkOrderEventsQuery } = require('./../payment');

const { ROOT_TOKEN } = require('./../../config');

const allEventsByIdsQuery = (eventsIds) => {
  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'allEventsByIdsQuery',
    variables: { ids: eventsIds },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.allEvents;
    });
};

const userProfileByIdQuery = (req, participants) => {
  if (!req.isAuthenticated()) return Array.isArray(participants) ? participants[0] : null;
  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'userProfileByIdQuery',
    variables: {
      id: req.user.id,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.User;
    });
};

module.exports = (req) => {
  const { id } = req.body;
  let userAnOrder = null;
  let order = null;
  let resultOrder = null;
  let discounts = null;
  let promocode = null;
  let lectures = null;
  let draft = null;

  return redis.getAsync(`order-draft:${id}`)
    .then((reply) => {
      if (!reply) return Promise.reject();
      draft = JSON.parse(reply);
      userAnOrder = draft.userAnOrder;
      order = draft.order;
      discounts = draft.discounts;
      promocode = draft.promocode;
      lectures = draft.lectures;
      return Promise.all([
        redis.getAsync('acquiring'),
        checkAvailabilityOfUserMutation(req, draft.participants),
      ]);
    })
    .then((result) => {
      const [reply, userIds] = result;
      const acquiring = JSON.parse(reply);
      const [userAnOrderIds] = userIds.ids;

      let additionalDiscountsIds = null;
      let certificatesInPaymentsIds = null;
      let subscriptionsInPaymentsIds = null;

      if (promocode) {
        if (promocode.__typename === 'Subscription') {
          subscriptionsInPaymentsIds = [promocode.id];
        } else if (promocode.__typename === 'Certificate') {
          certificatesInPaymentsIds = [promocode.id];
        } else {
          additionalDiscountsIds = [promocode.id];
        }
      }

      const payment = {
        acquiringId: acquiring.id,
        commission: acquiring.commission,
        discountsIds: discounts ? discounts.map(({ id }) => id) : [],
        additionalDiscountsIds: additionalDiscountsIds || [],
        certificatesInPaymentsIds: certificatesInPaymentsIds || [],
        subscriptionsInPaymentsIds: subscriptionsInPaymentsIds || [],
        orderPrice: order.orderPrice,
        discountPrice: order.discountsPrice,
        totalPrice: order.totalPrice,
        type: 'INTERNET_ACQUIRING',
        userId: userAnOrderIds,
        status: 'PENDING',
        referer: 'SITE',
        data: draft,
      };

      return graphQlFetch({
        req: { user: { token: ROOT_TOKEN } },
        query: 'createOrderMutation',
        variables: {
          userId: userAnOrderIds,
          draft: false,
          totalPrice: order.totalPrice,
          eventsIds: Object.keys(lectures).map(key => lectures[key].eventId),
          payment,
          participantsIds: userIds.ids,
        },
      });
    })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.createOrder;
    })
    .then((result) => {
      resultOrder = result;
      const { payment: { id: paymentId } } = result;
      draft.paymentId = paymentId;
      const commands = [
        ['set', `payment-data:${result.payment.id}`, JSON.stringify(draft)],
        ['del', `order-draft:${id}`],
      ];
      return redis.batch(commands).execAsync();
    })
    .then(() => {
      resultOrder.data = draft;
      return resultOrder;
    });
};

module.exports.payment = (req) => {
  const { id } = req.params;
  let data = null;
  let acquiring = null;

  return redis.batch([
    ['get', `payment-data:${id}`],
    ['get', 'acquiring'],
  ]).execAsync()
    .then((results) => {
      data = JSON.parse(results[0]);
      acquiring = JSON.parse(results[1]);
      if (!data || !acquiring) return Promise.reject();

      const { lectures } = data;

      const eventsIds = Object.keys(lectures).map((lectureId) => {
        const event = lectures[lectureId];
        return event.eventId;
      });

      return allEventsByIdsQuery(eventsIds);
    })
    .then((allEvents) => {
      return Object.assign(
        {},
        data,
        {
          acquiring: acquiring || {},
          allEvents,
        },
      );
    });
};

module.exports.draft = (req) => {
  const { id } = req.params;
  let data = null;
  let acquiring = null;

  return redis.batch([
    ['get', `order-draft:${id}`],
    ['get', 'acquiring'],
  ]).execAsync()
    .then((results) => {
      if (!results || !results[0] || !results[1]) return Promise.reject(new Error('Not found'));
      data = JSON.parse(results[0]);
      acquiring = JSON.parse(results[1]);

      const { lectures } = data;

      const commands = [];

      const eventsIds = Object.keys(lectures).map((lectureId) => {
        return lectures[lectureId].eventId;
      });

      return allEventsByIdsQuery(eventsIds);
    })
    .then((allEvents) => {
      return Object.assign(
        {},
        data,
        {
          id,
          acquiring: acquiring || {},
          allEvents,
        },
      );
    });
};

module.exports.setDraft = (req) => {
  const order = JSON.parse(req.body.order);
  const { participants } = order;

  let hash = null;

  return checkOrderEventsQuery(order)
    .then((result) => {
      const {
        resultOfChecking,
        resultOfCalcOrder,
      } = result;

      if (resultOfChecking) return { resultOfChecking };
      if (resultOfCalcOrder) return { resultOfCalcOrder };

      return Promise.all([
        userProfileByIdQuery(req, participants),
        randomBytesAsync(12),
      ])
        .then((result) => {
          const [userProfile, buff] = result;
          hash = buff.toString('hex');
          return redis.setAsync(
            `order-draft:${hash}`,
            JSON.stringify(Object.assign(
              {},
              order,
              { userAnOrder: userProfile },
            )),
          );
        })
        .then(reply => ({ hash }));
    });
};

module.exports.cancel = (req) => {
  const { id } = req.params;
  const { id: userId } = req.user;

  return graphQlFetch({
    req: { user: { token: ROOT_TOKEN } },
    query: 'deleteOrderWithPaymentMutation',
    variables: {
      id,
      userId,
    },
  })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data;
    });
};
