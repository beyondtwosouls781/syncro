const { MAIL_HOST } = require('./../../config');
const validator = require('./../../lib/validator');
const fetcPostJson = require('./../../lib/fetcPostJson');

const checkError = (res) => {
  if (res.status !== 200) {
    return res.text()
      .then((text) => {
        const err = new Error(text);
        err.status = res.status;
        return Promise.reject(err);
      });
  }
  return res;
};

module.exports = (req) => {};
module.exports.common = (req) => {
  const { email } = req.body;
  const errors = validator(req.body);
  if (errors.length > 0) return Promise.reject(new Error('Wrong params'));

  return fetcPostJson(`${MAIL_HOST}/subscribe/common`, { email })
    .then(res => checkError(res))
    .then(res => res.json());
};

module.exports.schedule = (req) => {
  const { email } = req.body;
  const errors = validator(req.body);
  if (errors.length > 0) return Promise.reject(new Error('Wrong params'));

  return fetcPostJson(`${MAIL_HOST}/subscribe/schedule`, { email })
    .then(res => checkError(res))
    .then(res => res.json());
};
