const express = require('express');
const router = express.Router();
// User-agent: *
// Disallow: /

router.get('/', (req, res, next) => {
  res.send('');
});

module.exports = router;
