/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 77);
/******/ })
/************************************************************************/
/******/ ({

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var _window = window,
    $ = _window.$,
    utils = _window.utils;


var onScroll = function onScroll(doc) {};

var tagsList = function tagsList() {
  var container = $('.filter-container');
  var items = $('.sortable-items');
  var dataSelector = 'filter-alias';
  var doc = $(document);
  var fnScroll = onScroll.bind(null, doc);
  doc.on('scroll', fnScroll);

  items.each(function (i, e) {
    var itemsActual = $(e);
    var tagsEl = itemsActual.find('.tag');
    tagsEl.on('click', function (eT) {
      var el = $(eT.target);
      var alias = '';
      if (el.hasClass('tag')) {
        alias = el.data('alias');
      } else {
        alias = el.parents('.tag').data('alias');
      }

      var pos = el.offset().top - doc.scrollTop();
      container.eq(i).find('[data-filter-alias=' + alias + ']').trigger('click');
      doc.scrollTop(el.offset().top - pos);
    });
  });

  container.each(function (i, e) {
    var containerActual = $(e);
    var itemsActual = items.eq(i).find('.sortable-item');
    var sortingType = containerActual.data('sorting-type');
    var alias = [];
    containerActual.on('click', function (event) {
      var el = $(event.target);
      if (el.hasClass('filter-item')) {
        event.preventDefault();
        var elAlias = el.data(dataSelector);

        if (elAlias === 'all') {
          alias = [];
          utils.setActive(containerActual, el);
        } else {
          if (sortingType === 'single') {
            alias = [elAlias];
            utils.setActive(containerActual, el, alias, '.filter-item');
          } else {
            var ind = alias.indexOf(elAlias);
            if (ind === -1) {
              alias.push(elAlias);
            } else {
              alias.splice(ind, 1);
            }
            utils.setActiveMany(containerActual, el, alias, '.filter-item');
          }
        }
        if (sortingType === 'single') {
          utils.sortItems(itemsActual, alias, dataSelector);
        } else {
          utils.sortItemsMany(itemsActual, alias, dataSelector);
        }
      }
    });
  });
};

exports.default = tagsList;

/***/ }),

/***/ 23:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _sortable = __webpack_require__(10);

var _sortable2 = _interopRequireDefault(_sortable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _sortable2.default)();

/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(23);


/***/ })

/******/ });