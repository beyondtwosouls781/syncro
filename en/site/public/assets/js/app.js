/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 75);
/******/ })
/************************************************************************/
/******/ ({

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
// import $ from 'jquery';
var $ = window.$;

var utils = {
  remCalc: function remCalc(rootEl) {
    return function (n) {
      return parseFloat(rootEl.css('font-size')) * n / 16;
    };
  },

  show: function show(el, inlineBlock) {
    el.removeClass('hidden');
    if (inlineBlock) {
      el.addClass('visible-inline-block');
    } else {
      el.addClass('visible');
    }
  },
  hide: function hide(el) {
    el.removeClass('visible');
    el.addClass('hidden');
  },
  sortItems: function sortItems(items, alias, dataSelector) {
    var self = this;
    // const visibleElements = [];
    // const hiddenElements = [];
    if (alias === 'all') {
      self.show(items);
    } else {
      items.each(function (i, e) {
        var el = $(e);
        var dateEl = el.parents('.sortable-date-item');
        if (el.data(dataSelector) === alias) {
          self.show(el);
          self.show(dateEl);
          // visibleElements.push(el);
        } else {
          self.hide(el);
          if (dateEl.find('.sortable-item:visible').length === 0) self.hide(dateEl);
          // hiddenElements.push(el);
        }
      });
    }
  },
  sortItemsMany: function sortItemsMany(items, alias, dataSelector) {
    var self = this;
    // const visibleElements = [];
    // const hiddenElements = [];
    if (alias.length === 0) {
      self.show(items);
    } else {
      items.each(function (i, e) {
        var el = $(e);
        var elAlias = el.data(dataSelector);
        var dateEl = el.parents('.sortable-date-item');

        if (alias.indexOf(elAlias) !== -1) {
          self.show(el);
          self.show(dateEl);
          // visibleElements.push(el);
        } else {
          self.hide(el);
          if (dateEl.find('.sortable-item:visible').length === 0) self.hide(dateEl);
          // hiddenElements.push(el);
        }
      });
    }
  },

  setActive: function setActive(container, el) {
    container.find('.active').removeClass('active');
    el.addClass('active');
  },
  setActiveMany: function setActiveMany(container, el, alias, firstEl) {
    if (firstEl) {
      if (alias.length === 0) {
        container.find(firstEl).eq(0).addClass('active');
      } else {
        container.find(firstEl).eq(0).removeClass('active');
      }
    }

    if (el.hasClass('active')) {
      el.removeClass('active');
    } else {
      el.addClass('active');
    }
  },
  checkNoEvents: function checkNoEvents(el, noEvens) {
    if (el.length === 0) {
      this.show(noEvens);
    } else {
      this.hide(noEvens);
    }
  }
};

exports.default = utils;

/***/ }),

/***/ 21:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _simplelightbox = __webpack_require__(54);

var _simplelightbox2 = _interopRequireDefault(_simplelightbox);

var _spin = __webpack_require__(70);

var _spin2 = _interopRequireDefault(_spin);

var _map = __webpack_require__(62);

var _map2 = _interopRequireDefault(_map);

var _subscribe = __webpack_require__(72);

var _subscribe2 = _interopRequireDefault(_subscribe);

var _form = __webpack_require__(59);

var _form2 = _interopRequireDefault(_form);

var _sticky = __webpack_require__(71);

var _sticky2 = _interopRequireDefault(_sticky);

var _showMore = __webpack_require__(67);

var _showMore2 = _interopRequireDefault(_showMore);

var _utils = __webpack_require__(18);

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.Spin = _spin2.default;
window.simpleLightbox = _simplelightbox2.default;
window.utils = _utils2.default;

var _window = window,
    $ = _window.$,
    Foundation = _window.Foundation;


Foundation.Abide.defaults.patterns.alpha_ru = /^[а-яА-Я]+$/;
Foundation.Abide.defaults.patterns.alpha_numeric_ru = /^[а-яА-Я0-9]+$/;
// Foundation.Abide.defaults.patterns.phone = /^[0-9-+()]*$/;
// Foundation.Abide.defaults.patterns.phone = /^((\+?7|8)+([0-9-()]){10})$/;
Foundation.Abide.defaults.patterns.phone = {
  test: function test(text) {
    var t = text.replace(/\+|\(|\)|-|_| |/g, '');
    return (/^(7|8)[0-9]{10}$/.test(t)
    );
  }
};
Foundation.Abide.defaults.patterns.password = {
  test: function test(text) {
    if (/[а-яё"'\s]/i.test(text) || text.length < 8) return false;
    return true;
  }
};

Foundation.Abide.defaults.validators.avatar = function (e) {
  console.log(e[0].files[0]);
  var el = $(e);
  var _e$0$files$ = e[0].files[0],
      size = _e$0$files$.size,
      type = _e$0$files$.type;

  var maxSize = parseInt(el.data('max-size'), 10);
  var accept = el.prop('accept').split(', ');
  console.log(accept);
  if (!maxSize) return false;
  return size < maxSize && accept.includes(type);
};

$(document).foundation();

_utils2.default.remCalcFn = _utils2.default.remCalc($('html')).bind(null);

$(window).ready(function () {
  $('.sticky-container').addClass('sticky-animate');
});

$('.event-dates .more-dates').on('click', function (e) {
  var el = $(e.target);
  var p = el.parent();
  _utils2.default.show(p.find('.hidden'));
  _utils2.default.hide(el);
});

window.MediaQueryCurrent = Foundation.MediaQuery.current;
$(window).on('changed.zf.mediaquery', function (event, newSize, oldSize) {
  window.MediaQueryCurrent = newSize;
  // console.log(newSize);
});

(0, _sticky2.default)();
(0, _form2.default)();
(0, _subscribe2.default)();
(0, _showMore2.default)();

/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
	By André Rinas, www.andrerinas.de
	Available for use under the MIT License
	1.12.0
*/
;(function ($, window, document, undefined) {
	'use strict';

	$.fn.simpleLightbox = function (options) {

		var options = $.extend({
			sourceAttr: 'href',
			overlay: true,
			spinner: true,
			nav: true,
			navText: ['&lsaquo;', '&rsaquo;'],
			captions: true,
			captionDelay: 0,
			captionSelector: 'img',
			captionType: 'attr',
			captionsData: 'title',
			captionPosition: 'bottom',
			captionClass: '',
			close: true,
			closeText: '×',
			swipeClose: true,
			showCounter: true,
			fileExt: 'png|jpg|jpeg|gif',
			animationSlide: true,
			animationSpeed: 250,
			preloading: true,
			enableKeyboard: true,
			loop: true,
			rel: false,
			docClose: true,
			swipeTolerance: 50,
			className: 'simple-lightbox',
			widthRatio: 0.8,
			heightRatio: 0.9,
			disableRightClick: false,
			disableScroll: true,
			alertError: true,
			alertErrorMessage: 'Image not found, next image will be loaded',
			additionalHtml: false,
			history: true
		}, options);

		// global variables
		var touchDevice = 'ontouchstart' in window,
		    pointerEnabled = window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
		    touched = function touched(event) {
			if (touchDevice) return true;
			if (!pointerEnabled || typeof event === 'undefined' || typeof event.pointerType === 'undefined') return false;
			if (typeof event.MSPOINTER_TYPE_MOUSE !== 'undefined') {
				if (event.MSPOINTER_TYPE_MOUSE != event.pointerType) return true;
			} else {
				if (event.pointerType != 'mouse') return true;
			}
			return false;
		},
		    swipeDiff = 0,
		    swipeYDiff = 0,
		    curImg = $(),
		    transPrefix = function transPrefix() {
			var s = document.body || document.documentElement;
			s = s.style;
			if (s.WebkitTransition === '') return '-webkit-';
			if (s.MozTransition === '') return '-moz-';
			if (s.OTransition === '') return '-o-';
			if (s.transition === '') return '';
			return false;
		},
		    opened = false,
		    loaded = [],
		    getRelated = function getRelated(rel, jqObj) {
			var $related = $(jqObj.selector).filter(function () {
				return $(this).attr('rel') === rel;
			});
			return $related;
		},
		    objects = options.rel && options.rel !== false ? getRelated(options.rel, this) : this,
		    transPrefix = transPrefix(),
		    globalScrollbarwidth = 0,
		    canTransisions = transPrefix !== false ? true : false,
		    supportsPushState = 'pushState' in history,
		    historyhasChanged = false,
		    historyUpdateTimeout,
		    winLoc = window.location,
		    getHash = function getHash() {
			return winLoc.hash.substring(1);
		},
		    initialHash = getHash(),
		    updateHash = function updateHash() {
			var hash = getHash(),
			    newHash = 'pid=' + (index + 1);
			var newURL = winLoc.href.split('#')[0] + '#' + newHash;

			if (supportsPushState) {
				history[historyhasChanged ? 'replaceState' : 'pushState']('', document.title, newURL);
			} else {
				if (historyhasChanged) {
					winLoc.replace(newURL);
				} else {
					winLoc.hash = newHash;
				}
			}
			historyhasChanged = true;
		},
		    resetHash = function resetHash() {
			if (supportsPushState) {
				history.pushState('', document.title, winLoc.pathname + winLoc.search);
			} else {
				winLoc.hash = '';
			}
			clearTimeout(historyUpdateTimeout);
		},
		    updateURL = function updateURL() {
			if (!historyhasChanged) {
				updateHash(); // first time
			} else {
				historyUpdateTimeout = setTimeout(updateHash, 800);
			}
		},
		    prefix = 'simplelb',
		    overlay = $('<div>').addClass('sl-overlay'),
		    closeBtn = $('<button>').addClass('sl-close').html(options.closeText),
		    spinner = $('<div>').addClass('sl-spinner').html('<div></div>'),
		    nav = $('<div>').addClass('sl-navigation').html('<button class="sl-prev">' + options.navText[0] + '</button><button class="sl-next">' + options.navText[1] + '</button>'),
		    counter = $('<div>').addClass('sl-counter').html('<span class="sl-current"></span>/<span class="sl-total"></span>'),
		    animating = false,
		    index = 0,
		    caption = $('<div>').addClass('sl-caption ' + options.captionClass + ' pos-' + options.captionPosition),
		    image = $('<div>').addClass('sl-image'),
		    wrapper = $('<div>').addClass('sl-wrapper').addClass(options.className),
		    isValidLink = function isValidLink(element) {
			if (!options.fileExt) return true;
			var filEext = /\.([0-9a-z]+)(?=[?#])|(\.)(?:[\w]+)$/gmi;
			var testExt = $(element).attr(options.sourceAttr).match(filEext);
			return testExt && $(element).prop('tagName').toLowerCase() == 'a' && new RegExp('\.(' + options.fileExt + ')$', 'i').test(testExt);
		},
		    setup = function setup() {
			if (options.close) closeBtn.appendTo(wrapper);
			if (options.showCounter) {
				if (objects.length > 1) {
					counter.appendTo(wrapper);
					counter.find('.sl-total').text(objects.length);
				}
			}
			if (options.nav) nav.appendTo(wrapper);
			if (options.spinner) spinner.appendTo(wrapper);
		},
		    openImage = function openImage(elem) {
			elem.trigger($.Event('show.simplelightbox'));
			if (options.disableScroll) globalScrollbarwidth = handleScrollbar('hide');
			wrapper.appendTo('body');
			image.appendTo(wrapper);
			if (options.overlay) overlay.appendTo($('body'));
			animating = true;
			index = objects.index(elem);
			curImg = $('<img/>').hide().attr('src', elem.attr(options.sourceAttr));
			if (loaded.indexOf(elem.attr(options.sourceAttr)) == -1) {
				loaded.push(elem.attr(options.sourceAttr));
			}
			image.html('').attr('style', '');
			curImg.appendTo(image);
			addEvents();
			overlay.fadeIn('fast');
			$('.sl-close').fadeIn('fast');
			spinner.show();
			nav.fadeIn('fast');
			$('.sl-wrapper .sl-counter .sl-current').text(index + 1);
			counter.fadeIn('fast');
			adjustImage();
			if (options.preloading) preload();
			setTimeout(function () {
				elem.trigger($.Event('shown.simplelightbox'));
			}, options.animationSpeed);
		},
		    adjustImage = function adjustImage(dir) {
			if (!curImg.length) return;
			var tmpImage = new Image(),
			    windowWidth = $(window).width() * options.widthRatio,
			    windowHeight = $(window).height() * options.heightRatio;
			tmpImage.src = curImg.attr('src');

			$(tmpImage).bind('error', function (ev) {
				//no image was found
				objects.eq(index).trigger($.Event('error.simplelightbox'));
				animating = false;
				opened = true;
				spinner.hide();
				if (options.alertError) {
					alert(options.alertErrorMessage);
				}
				if (dir == 1 || dir == -1) {
					loadImage(dir);
				} else {
					loadImage(1);
				}
				return;
			});

			tmpImage.onload = function () {
				if (typeof dir !== 'undefined') {
					objects.eq(index).trigger($.Event('changed.simplelightbox')).trigger($.Event((dir === 1 ? 'nextDone' : 'prevDone') + '.simplelightbox'));
				}

				// history
				if (options.history) {
					updateURL();
				}

				if (loaded.indexOf(curImg.attr('src')) == -1) {
					loaded.push(curImg.attr('src'));
				}
				var imageWidth = tmpImage.width,
				    imageHeight = tmpImage.height;

				if (imageWidth > windowWidth || imageHeight > windowHeight) {
					var ratio = imageWidth / imageHeight > windowWidth / windowHeight ? imageWidth / windowWidth : imageHeight / windowHeight;
					imageWidth /= ratio;
					imageHeight /= ratio;
				}

				$('.sl-image').css({
					'top': ($(window).height() - imageHeight) / 2 + 'px',
					'left': ($(window).width() - imageWidth - globalScrollbarwidth) / 2 + 'px'
				});
				spinner.hide();
				curImg.css({
					'width': imageWidth + 'px',
					'height': imageHeight + 'px'
				}).fadeIn('fast');
				opened = true;
				var cSel = options.captionSelector == 'self' ? objects.eq(index) : objects.eq(index).find(options.captionSelector);
				var captionText;
				if (options.captionType == 'data') {
					captionText = cSel.data(options.captionsData);
				} else if (options.captionType == 'text') {
					captionText = cSel.html();
				} else {
					captionText = cSel.prop(options.captionsData);
				}

				if (!options.loop) {
					if (index === 0) {
						$('.sl-prev').hide();
					}
					if (index >= objects.length - 1) {
						$('.sl-next').hide();
					}
					if (index > 0) {
						$('.sl-prev').show();
					}
					if (index < objects.length - 1) {
						$('.sl-next').show();
					}
				}

				if (objects.length == 1) $('.sl-prev, .sl-next').hide();

				if (dir == 1 || dir == -1) {
					var css = { 'opacity': 1.0 };
					if (options.animationSlide) {
						if (canTransisions) {
							slide(0, 100 * dir + 'px');
							setTimeout(function () {
								slide(options.animationSpeed / 1000, 0 + 'px');
							}, 50);
						} else {
							css.left = parseInt($('.sl-image').css('left')) + 100 * dir + 'px';
						}
					}

					$('.sl-image').animate(css, options.animationSpeed, function () {
						animating = false;
						setCaption(captionText);
					});
				} else {
					animating = false;
					setCaption(captionText);
				}
				if (options.additionalHtml && $('.sl-additional-html').length === 0) {
					$('<div>').html(options.additionalHtml).addClass('sl-additional-html').appendTo($('.sl-image'));
				}
			};
		},
		    setCaption = function setCaption(captiontext) {
			if (captiontext !== '' && typeof captiontext !== "undefined" && options.captions) {
				caption.html(captiontext).hide().appendTo($('.sl-image')).delay(options.captionDelay).fadeIn('fast');
			}
		},
		    slide = function slide(speed, pos) {
			var styles = {};
			styles[transPrefix + 'transform'] = 'translateX(' + pos + ')';
			styles[transPrefix + 'transition'] = transPrefix + 'transform ' + speed + 's linear';
			$('.sl-image').css(styles);
		},
		    addEvents = function addEvents() {
			// resize/responsive
			$(window).on('resize.' + prefix, adjustImage);

			// close lightbox on close btn
			$(document).on('click.' + prefix + ' touchstart.' + prefix, '.sl-close', function (e) {
				e.preventDefault();
				if (opened) {
					close();
				}
			});

			if (options.history) {
				setTimeout(function () {
					$(window).on('hashchange.' + prefix, function () {
						if (opened) {
							if (getHash() === initialHash) {
								close();
								return;
							}
						}
					});
				}, 40);
			}

			// nav-buttons
			nav.on('click.' + prefix, 'button', function (e) {
				e.preventDefault();
				swipeDiff = 0;
				loadImage($(this).hasClass('sl-next') ? 1 : -1);
			});

			// touchcontrols
			var swipeStart = 0,
			    swipeEnd = 0,
			    swipeYStart = 0,
			    swipeYEnd = 0,
			    mousedown = false,
			    imageLeft = 0;

			image.on('touchstart.' + prefix + ' mousedown.' + prefix, function (e) {
				if (mousedown) return true;
				if (canTransisions) imageLeft = parseInt(image.css('left'));
				mousedown = true;
				swipeStart = e.originalEvent.pageX || e.originalEvent.touches[0].pageX;
				swipeYStart = e.originalEvent.pageY || e.originalEvent.touches[0].pageY;
				return false;
			}).on('touchmove.' + prefix + ' mousemove.' + prefix + ' pointermove MSPointerMove', function (e) {
				if (!mousedown) return true;
				e.preventDefault();
				swipeEnd = e.originalEvent.pageX || e.originalEvent.touches[0].pageX;
				swipeYEnd = e.originalEvent.pageY || e.originalEvent.touches[0].pageY;
				swipeDiff = swipeStart - swipeEnd;
				swipeYDiff = swipeYStart - swipeYEnd;
				if (options.animationSlide) {
					if (canTransisions) slide(0, -swipeDiff + 'px');else image.css('left', imageLeft - swipeDiff + 'px');
				}
			}).on('touchend.' + prefix + ' mouseup.' + prefix + ' touchcancel.' + prefix + ' mouseleave.' + prefix + ' pointerup pointercancel MSPointerUp MSPointerCancel', function (e) {
				if (mousedown) {
					mousedown = false;
					var possibleDir = true;
					if (!options.loop) {
						if (index === 0 && swipeDiff < 0) {
							possibleDir = false;
						}
						if (index >= objects.length - 1 && swipeDiff > 0) {
							possibleDir = false;
						}
					}
					if (Math.abs(swipeDiff) > options.swipeTolerance && possibleDir) {
						loadImage(swipeDiff > 0 ? 1 : -1);
					} else if (options.animationSlide) {
						if (canTransisions) slide(options.animationSpeed / 1000, 0 + 'px');else image.animate({ 'left': imageLeft + 'px' }, options.animationSpeed / 2);
					}

					if (options.swipeClose && Math.abs(swipeYDiff) > 50 && Math.abs(swipeDiff) < options.swipeTolerance) {
						close();
					}
				}
			});
		},
		    removeEvents = function removeEvents() {
			nav.off('click', 'button');
			$(document).off('click.' + prefix, '.sl-close');
			$(window).off('resize.' + prefix);
			$(window).off('hashchange.' + prefix);
		},
		    preload = function preload() {
			var next = index + 1 < 0 ? objects.length - 1 : index + 1 >= objects.length - 1 ? 0 : index + 1,
			    prev = index - 1 < 0 ? objects.length - 1 : index - 1 >= objects.length - 1 ? 0 : index - 1;
			$('<img />').attr('src', objects.eq(next).attr(options.sourceAttr)).on('load', function () {
				if (loaded.indexOf($(this).attr('src')) == -1) {
					loaded.push($(this).attr('src'));
				}
				objects.eq(index).trigger($.Event('nextImageLoaded.simplelightbox'));
			});
			$('<img />').attr('src', objects.eq(prev).attr(options.sourceAttr)).on('load', function () {
				if (loaded.indexOf($(this).attr('src')) == -1) {
					loaded.push($(this).attr('src'));
				}
				objects.eq(index).trigger($.Event('prevImageLoaded.simplelightbox'));
			});
		},
		    loadImage = function loadImage(dir) {
			objects.eq(index).trigger($.Event('change.simplelightbox')).trigger($.Event((dir === 1 ? 'next' : 'prev') + '.simplelightbox'));

			var newIndex = index + dir;
			if (animating || (newIndex < 0 || newIndex >= objects.length) && options.loop === false) return;
			index = newIndex < 0 ? objects.length - 1 : newIndex > objects.length - 1 ? 0 : newIndex;
			$('.sl-wrapper .sl-counter .sl-current').text(index + 1);
			var css = { 'opacity': 0 };
			if (options.animationSlide) {
				if (canTransisions) slide(options.animationSpeed / 1000, -100 * dir - swipeDiff + 'px');else css.left = parseInt($('.sl-image').css('left')) + -100 * dir + 'px';
			}

			$('.sl-image').animate(css, options.animationSpeed, function () {
				setTimeout(function () {
					// fadeout old image
					var elem = objects.eq(index);
					curImg.attr('src', elem.attr(options.sourceAttr));
					if (loaded.indexOf(elem.attr(options.sourceAttr)) == -1) {
						spinner.show();
					}
					$('.sl-caption').remove();
					adjustImage(dir);
					if (options.preloading) preload();
				}, 100);
			});
		},
		    close = function close() {
			if (animating) return;
			var elem = objects.eq(index),
			    triggered = false;

			elem.trigger($.Event('close.simplelightbox'));
			if (options.history) {
				resetHash();
			}
			$('.sl-image img, .sl-overlay, .sl-close, .sl-navigation, .sl-image .sl-caption, .sl-counter').fadeOut('fast', function () {
				if (options.disableScroll) handleScrollbar('show');
				$('.sl-wrapper, .sl-overlay').remove();
				removeEvents();
				if (!triggered) elem.trigger($.Event('closed.simplelightbox'));
				triggered = true;
			});
			curImg = $();
			opened = false;
			animating = false;
		},
		    handleScrollbar = function handleScrollbar(type) {
			var scrollbarWidth = 0;
			if (type == 'hide') {
				var fullWindowWidth = window.innerWidth;
				if (!fullWindowWidth) {
					var documentElementRect = document.documentElement.getBoundingClientRect();
					fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left);
				}
				if (document.body.clientWidth < fullWindowWidth) {
					var scrollDiv = document.createElement('div'),
					    padding = parseInt($('body').css('padding-right'), 10);
					scrollDiv.className = 'sl-scrollbar-measure';
					$('body').append(scrollDiv);
					scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
					$(document.body)[0].removeChild(scrollDiv);
					$('body').data('padding', padding);
					if (scrollbarWidth > 0) {
						$('body').addClass('hidden-scroll').css({ 'padding-right': padding + scrollbarWidth });
					}
				}
			} else {
				$('body').removeClass('hidden-scroll').css({ 'padding-right': $('body').data('padding') });
			}
			return scrollbarWidth;
		};

		// events
		setup();

		// open lightbox
		objects.on('click.' + prefix, function (e) {
			if (isValidLink(this)) {
				e.preventDefault();
				if (animating) return false;
				openImage($(this));
			}
		});

		// close on click on doc
		$(document).on('click.' + prefix + ' touchstart.' + prefix, function (e) {
			if (opened) {
				if (options.docClose && $(e.target).closest('.sl-image').length === 0 && $(e.target).closest('.sl-navigation').length === 0) {
					close();
				}
			}
		});

		// disable rightclick
		if (options.disableRightClick) {
			$(document).on('contextmenu', '.sl-image img', function (e) {
				return false;
			});
		}

		// keyboard-control
		if (options.enableKeyboard) {
			$(document).on('keyup.' + prefix, function (e) {
				swipeDiff = 0;
				// keyboard control only if lightbox is open
				if (opened) {
					e.preventDefault();
					var key = e.keyCode;
					if (key == 27) {
						close();
					}
					if (key == 37 || e.keyCode == 39) {
						loadImage(e.keyCode == 39 ? 1 : -1);
					}
				}
			});
		}

		// Public methods
		this.open = function (elem) {
			elem = elem || $(this[0]);
			openImage(elem);
		};

		this.next = function () {
			loadImage(1);
		};

		this.prev = function () {
			loadImage(-1);
		};

		this.close = function () {
			close();
		};

		this.destroy = function () {
			$(document).unbind('click.' + prefix).unbind('keyup.' + prefix);
			close();
			$('.sl-overlay, .sl-wrapper').remove();
			this.off('click');
		};

		this.refresh = function () {
			this.destroy();
			$(this).simpleLightbox(options);
		};

		return this;
	};
})(jQuery, window, document);

/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var _window = window,
    $ = _window.$,
    Foundation = _window.Foundation;

// $('.logoutUser').on('click', (e) => {
//   e.preventDefault();
// });

var bodyElm = $('body');

var formError = function formError(elm, message) {
  elm.text(message || 'Ошибка.');
  Foundation.Motion.animateIn(elm, 'fast fade-in');
  elm.parents('.reveal').one('click', function (e) {
    var elR = $(e.target);
    if ($(e.target).closest('button').length === 0) {
      if (elm.is(':visible')) {
        Foundation.Motion.animateOut(elm, 'slow fade-out', function () {
          elm.text('').attr('class', 'form-error');
        });
      }
    }
  });
};

var checkForm = function checkForm() {
  var form = $('form').not('.not-check');

  form.parents('.reveal-overlay').on('closed.zf.reveal', function (ev) {
    var el = $(ev.target);
    el.find('form').show();
    var formSuccessElm = el.find('.form-success');
    formSuccessElm.text('');
    var formErrorElm = el.find('.form-error');
    formErrorElm.text('').attr('class', 'form-error').hide();
  });

  form.on('formvalid.zf.abide', function (ev, elm) {
    ev.preventDefault();
    var el = $(elm);
    var formErrorElm = el.find('.form-error');
    var formSuccessElm = el.parent().find('.form-success');
    formErrorElm.text('').attr('class', 'form-error').hide();
    var spinFullElm = $('.spin-full');
    var spinLocalElm = el.find('.spin-local');

    var href = el.prop('action');

    if (!href || href === '/') {
      return false;
    }
    // const { hostname } = window.location;

    var dataSerialized = elm.serialize();

    switch (true) {
      case /\/subscribe\/common/.test(href):
        {
          el.find('.elm').show();
          el.find('input').prop('disabled', true).hide();
          el.find('button').prop('disabled', true).hide();
          spinLocalElm.show();
          break;
        }
      default:
        {
          el.find('input').prop('disabled', true);
          el.find('button').prop('disabled', true);
          spinFullElm.show();
          bodyElm.addClass('hide-scroll');
        }
    }

    $.post(href, dataSerialized).done(function (result) {
      var data = result.data,
          error = result.error;


      switch (true) {
        case /\/sign-up$/.test(href):
          {
            if (data && data.user) {
              $('#signUpModal').foundation('close');
              $('.userIsLoggedNav').show();
              $('.userNoLoggedNav').hide();
            } else {
              formError(formErrorElm, error.message);
            }
            break;
          }
        case /\/sign-in$/.test(href):
          {
            if (data && data.user) {
              if (window.location.pathname === '/account') window.location.reload();
              $('#signInModal').foundation('close');
              $('.userIsLoggedNav').show();
              $('.userNoLoggedNav').hide();
            } else {
              formError(formErrorElm, error.message);
            }
            break;
          }
        case /\/reset-pass$/.test(href):
          {
            if (data && data.success) {
              el.hide();
              formSuccessElm.text('На указанный e-mail отправлена инструкция для сброса пароля');
            } else {
              formError(formErrorElm, error.message);
            }
            break;
          }
        case /\/update-pass$/.test(href):
          {
            if (!data || !data.success) formError(formErrorElm, error.message);
            window.location.replace('/');
            break;
          }
        case /\/confirm-email$/.test(href):
          {
            if (data && data.success) {
              el.hide();
              formSuccessElm.text('На указанный e-mail отправлена инструкция для подтверждения e-mail');
            } else {
              formError(formErrorElm, error.message);
            }
            break;
          }
        case /\/certificate\/draft/.test(href):
          {
            if (data) {
              window.location.href = '/certificate/draft/' + data.hash;
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные');
            }
            break;
          }
        case /\/subscription\/draft/.test(href):
          {
            if (data) {
              form.find('input').prop('disabled', true);
              form.find('button').prop('disabled', true);
              window.location.href = '/subscription/draft/' + data.hash;
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные');
            }
            break;
          }
        case /\/subscribe\/common/.test(href) || /\/subscribe\/schedule/.test(href):
          {
            if (data) {
              form.find('input').hide();
              form.find('button').hide();
              formSuccessElm.text('Вы подписаны на рассылку.');
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные');
            }
            break;
          }
        case /\/review$/.test(href):
          {
            if (data) {
              form.find('input').hide();
              form.find('textarea').hide();
              form.find('button').hide();
              formSuccessElm.text('Ваш отзыв отправлен на модерацию.');
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные');
            }
            break;
          }
        case /\/account$/.test(href):
          {
            if (data) {
              formSuccessElm.text('Профель отредактирован.');
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные и размер изображения');
            }
            break;
          }
        default:
          {
            if (data && data.user) {
              $('#signInModal').foundation('close');
              $('#signUpModal').foundation('close');
              $('.userIsLoggedNav').show();
              $('.userNoLoggedNav').hide();
            } else {
              formError(formErrorElm, error.message);
            }
          }
      }
      if (window.getUserProfile && window.getUserProfile instanceof Function) window.getUserProfile();
    }).fail(function () {
      formError(formErrorElm, 'Ошибка сервиса, попробуйте позже');
    }).always(function () {
      setTimeout(function () {
        el.find('input').not('[name="promocode"]').prop('disabled', false);
        el.find('button').prop('disabled', false);
        spinFullElm.hide();
        spinLocalElm.hide();
        bodyElm.removeClass('hide-scroll');
      }, 300);
    });
  });

  form.on('forminvalid.zf.abide', function (ev, elm) {});

  form.submit(function (e) {

    e.preventDefault();
    return false;
    //   const el = $(e.target);
    //   el.foundation('validateForm')
    //   el.on('forminvalid.zf.abide', (ev, elm) => {
    //     console.log('invalid.zf.abide');
    //     console.log(ev);
    //     console.log(elm);
    //   });
    //   el.on('formvalid.zf.abide', (ev, elm) => {
    //     console.log('formvalid.zf.abide');
    //     console.log(ev);
    //     console.log(elm);
    //   });
    //   const href = el.data('href');
    //   // if (href) window.location.href = `${window.location.origin}/${href}`;
  });
};

exports.default = checkForm;

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var init = function init() {
  var myMap = null;
  var myPlacemark = null;
  var _window = window,
      _window$contactDetail = _window.contactDetails,
      _window$contactDetail2 = _window$contactDetail.address,
      address = _window$contactDetail2 === undefined ? '' : _window$contactDetail2,
      _window$contactDetail3 = _window$contactDetail.phone,
      phone = _window$contactDetail3 === undefined ? '' : _window$contactDetail3,
      _window$mapDetails = _window.mapDetails,
      _window$mapDetails$ce = _window$mapDetails.center,
      center = _window$mapDetails$ce === undefined ? '' : _window$mapDetails$ce,
      _window$mapDetails$pl = _window$mapDetails.placemark,
      placemark = _window$mapDetails$pl === undefined ? '' : _window$mapDetails$pl;


  myMap = new ymaps.Map('map', {
    center: center.split(','),
    zoom: 17,
    controls: []
  });

  myPlacemark = new ymaps.Placemark(placemark.split(','), {
    hintContent: 'Синхронизация',
    balloonContent: address + ' <br> ' + phone
  }, {
    // Опции.
    // Необходимо указать данный тип макета.
    iconLayout: 'default#image',
    // Своё изображение иконки метки.
    iconImageHref: '/assets/img/location.png',
    // Размеры метки.
    iconImageSize: [23, 34]
    // Смещение левого верхнего угла иконки относительно
    // её "ножки" (точки привязки).
    // iconImageOffset: [-5, -38]
  });

  myMap.geoObjects.add(myPlacemark);
};

window.mapOnload = function () {
  if ($('#map').length > 0) {
    window.ymaps.ready(init);
  }
};

/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var showMore = function showMore() {
  $(document).ready(function () {
    var elements = $('.button-show-more');
    elements.each(function (i, e) {
      var el = $(e);
      var container = el.parents('.show-more-container');
      var hiddenEl = container.children('.hidden');
      el.on('click', function () {
        hiddenEl.removeClass('hidden');
        el.remove();
      });
    });
  });
};

exports.default = showMore;

/***/ }),

/***/ 70:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _window = window,
    $ = _window.$;

var Spin = function () {
  function Spin() {
    _classCallCheck(this, Spin);

    this.el = $('.spin-glb');
    this.elements = {};
  }

  _createClass(Spin, [{
    key: 'show',
    value: function show(container) {
      if (!container || container.length === 0) return undefined;

      if (this.el.length === 0) this.el = $('.spin-glb');

      var id = Date.now().toString();
      var cEl = this.el.clone();
      this.elements[id] = cEl;
      container.append(cEl);
      cEl.show();
      return id;
    }
  }, {
    key: 'hide',
    value: function hide(id) {
      if (id) {
        this.elements[id].hide();
        this.elements[id].remove();
        delete this.elements[id];
      }
    }
  }]);

  return Spin;
}();

exports.default = Spin;

/***/ }),

/***/ 71:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var _window = window,
    $ = _window.$,
    utils = _window.utils;


var sticky = function sticky() {
  var navMain = $('.st-wrap');
  var navMainSticky = navMain.find('.st-inner');
  var navAnchor = $('.anchors-sticky-container');
  var navAnchorSticky = navAnchor.find('.anchors-sticky-inner');
  var navAnchorStickyLogo = navAnchorSticky.find('.logo-container');
  var navAnchorStickyAnchors = navAnchorSticky.find('.anchors');
  var $window = $(window);
  var prev = 0;
  $window.on('scroll', function () {
    var scrollTop = $window.scrollTop();
    if (navMainSticky.hasClass('is-stuck')) {
      navMain.toggleClass('mobile-hidden', scrollTop > prev && scrollTop > 100);
    }
    if (navAnchorSticky.hasClass('is-stuck')) {
      // const conHeight = navAnchorSticky.outerHeight() - navAnchorStickyAnchors.outerHeight();
      //

      // if (navAnchorSticky.hasClass('mobile-hidden')) {
      //   navAnchorSticky.css({ transform: 'translateY(0)' });
      // } else {
      //   navAnchorSticky.css({ transform: `translateY(-${utils.remCalcFn(conHeight)}px)` });
      // }
      navAnchorSticky.toggleClass('mobile-hidden', scrollTop > prev && scrollTop > 100);
    }
    prev = scrollTop;
  });
};

exports.default = sticky;

/***/ }),

/***/ 72:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var _window = window,
    $ = _window.$;


var subscribe = function subscribe() {
  // $('.subscribe-form').submit((e) => {
  //   const self = $(this);
  //   $.ajax({
  //     url: '/subscribe/add',
  //     type: 'post',
  //     dataType: 'json',
  //     data: $(this).serialize(),
  //     success: (data) => {
  //       if (!data.err.message) {
  //         self.find('.succses').show();
  //       } else {
  //         self.find('.error').show().find('span').text(data.err.message);
  //       }
  //     },
  //   });
  //   e.preventDefault();
  // }).find('.close').on('click', (e) => {
  //   $(e.target).parent().hide();
  // });
};

exports.default = subscribe;

/***/ }),

/***/ 75:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(21);


/***/ })

/******/ });