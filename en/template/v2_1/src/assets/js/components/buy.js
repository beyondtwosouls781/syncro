import Queue from './queue';

const { $, moment, Foundation } = window;

const bodyElm = $('body');
const spinFullElm = $('.spin-full');

// moment.locale('ru');

const maxRateDiscount = (discounts = [], lectures, tickets) => {
  const lecturesIds = Object.keys(lectures);
  let output = null;
  let tmpRate = 0;
  const tmpDiscounts = discounts
    .filter(({ buyingLecturesCount, participantsCount }) => (
      buyingLecturesCount <= lecturesIds.length
      && participantsCount <= tickets
    ));

  tmpDiscounts.forEach((discount) => {
    const { rate } = discount;
    if (rate > tmpRate) {
      output = discount;
      tmpRate = rate;
    }
  });
  return output;
};

const defaultOptions = {
  promocodePath: '/promocode',
  userProfilePath: '/users/profile',
};

class Order {
  static checkTickets(val) {
    const n = parseInt(val, 10);
    if (Number.isNaN(n)) return 1;
    return n;
  }

  constructor(product, options) {
    this.product = Object.assign({}, product);

    this.participants = {};
    this.events = {};
    this.lectures = {};
    this.promocode = null;
    this.discounts = null;
    this.orderPrice = 0;
    this.discountsPrice = 0;
    this.discountsRate = 0;
    this.totalPrice = 0;

    this.tickets = 0;
    this.abidePull = [];
    this.promocodeQueue = new Queue();
    this.options = Object.assign({}, defaultOptions, options);
    const elements = {};
    this.elements = elements;

    const containerEl = $('.buy');
    elements.containerEl = containerEl;
    elements.lectures = containerEl.find('.lecture-item');
    this.isCourse = $('.course-page').length > 0;
    this.isCycle = $('.cycle-page').length > 0;
    this.isLecture = $('.lecture-page').length > 0;

    elements.errorEl = containerEl.find('.fetch-error');
    elements.orderEl = containerEl.find('.order');
    elements.ticketsEl = containerEl.find('.tickets');
    elements.ticketsElInput = elements.ticketsEl.find('input');
    elements.slideContainerEl = containerEl.find('.lecture-dates .slider-container');
    elements.participantsListEl = containerEl.find('.participants-list');
    elements.containerAbideErrorEl = containerEl.find('.order-error');
    elements.containerAbideErrorEl = containerEl.find('.abide-error');
    elements.promocodeEl = containerEl.find('.promocode');
    elements.promocodeInputEl = elements.promocodeEl.find('input');
    elements.promocodeOrderEl = elements.orderEl.find('.order-promocode');
    elements.totalPriceEl = containerEl.find('.total').find('.total-price');
    elements.orderPriceEl = containerEl.find('.total').find('.order-price');

    this.handlerSelectDate = this.handlerSelectDate.bind(this);
    this.handlerClickBuy = this.handlerClickBuy.bind(this);
    this.handlerTickets = this.handlerTickets.bind(this);
    this.handlerTicketsButton = this.handlerTicketsButton.bind(this);
    this.handlerPromocode = this.handlerPromocode.bind(this);
    this.setLectureEvents = this.setLectureEvents.bind(this);
    this.setParticipants = this.setParticipants.bind(this);
    this.renderOrderItems = this.renderOrderItems.bind(this);
    this.renderParticipants = this.renderParticipants.bind(this);

    this.promocodeWorker = this.promocodeWorker.bind(this);
    this.promocodeSuccess = this.promocodeSuccess.bind(this);
    this.promocodeError = this.promocodeError.bind(this);
    this.getUserProfile = this.getUserProfile.bind(this);
    this.setUserToFirstparticipants = this.setUserToFirstparticipants.bind(this);
    this.calcOrder = this.calcOrder.bind(this);
    this.renderOrderPrice = this.renderOrderPrice.bind(this);
    this.renderOrderDiscounts = this.renderOrderDiscounts.bind(this);
    this.showError = this.showError.bind(this);

    elements.slideContainerEl.on('click', this.handlerSelectDate);
    elements.containerEl.find('.process-buy').on('click', this.handlerClickBuy);
    elements.ticketsElInput.on('change', this.handlerTickets);
    elements.ticketsEl.find('.title-r').on('click', this.handlerTicketsButton);

    elements.promocodeInputEl.on('input change', this.handlerPromocode);

    this.promocodeQueue.worker = this.promocodeWorker;
    this.promocodeQueue.success = this.promocodeSuccess;
    this.promocodeQueue.error = this.promocodeError;

    // init
    elements.slideContainerEl.each((i, e) => {
      $(e).find('.slide').eq(0).trigger('click');
    });
    elements.ticketsEl.find('.plus').trigger('click');
    this.getUserProfile();
  }
  calcOrder() {
    const {
      lectures,
      tickets,
      product,
      promocode,
      isCourse,
      isCycle,
    } = this;

    let discounts = null;

    let orderPrice = 0;
    let discountsRate = 0;
    let discountsPrice = 0;

    let isProductBuy = false;
    const lecturesIds = Object.keys(lectures);
    const isBuyAllLectures = lecturesIds.length === product.lectures.length;

    if ((isCourse || isCycle) && isBuyAllLectures) {
      orderPrice = product.price;
      isProductBuy = true;
    } else if (lecturesIds.length === 0) {
      orderPrice = 0;
    } else {
      orderPrice = lecturesIds
        .map(id => lectures[id].price)
        .reduce((prev, curr) => prev + curr);
    }

    orderPrice *= tickets;

    // промокод
    if (promocode) {
      const {
        rate,
        __typename,
        unusedLectures,
        personsCount,
        buyingLecturesCount,
        participantsCount,
      } = promocode;
      // сертификат
      if (__typename === 'Certificate' || __typename === 'Subscription') {
        let cerDiscountPrice = 0;
        if (unusedLectures === -1) {
          // безлимит
          if (isProductBuy) {
            cerDiscountPrice = product.price;
            cerDiscountPrice *= tickets > personsCount ? personsCount : tickets;
          } else {
            cerDiscountPrice = lecturesIds
              .map(id => lectures[id].price)
              .reduce((prev, curr) => prev + curr);

            cerDiscountPrice *= tickets > personsCount ? personsCount : tickets;
          }

        } else if (lecturesIds.length < unusedLectures) {
          if (isProductBuy) {
            const partialPrice = product.p / lecturesIds.length;
            const ost = lecturesIds.length - unusedLectures;
            let np = tickets > personsCount ? personsCount : tickets;
            let nl = unusedLectures;

            while (np > 0 && nl > 0) {
              np -= 1;
              lecturesIds.forEach((id) => {
                nl -= 1;
                cerDiscountPrice += partialPrice;
              });
            }

          } else {
            const ost = lecturesIds.length - unusedLectures;
            let np = tickets > personsCount ? personsCount : tickets;
            let nl = unusedLectures;
            while (np > 0 && nl > 0) {
              np -= 1;
              lecturesIds.forEach((id) => {
                nl -= 1;
                cerDiscountPrice += lectures[id].price;
              });
            }
          }
        } else {
          if (isProductBuy) {
            cerDiscountPrice = (orderPrice / lecturesIds.length) * unusedLectures;
          } else {
            cerDiscountPrice = lecturesIds
              .slice(0, unusedLectures)
              .map(id => lectures[id].price)
              .reduce((prev, curr) => prev + curr);
          }
        }
        discountsRate += (cerDiscountPrice * 100) / orderPrice;
      } else if (lecturesIds.length >= buyingLecturesCount && tickets >= participantsCount) {
        const checkLectures = promocode.lectures.filter(({ id }) => lecturesIds.includes(id));
        const checkCourse = promocode.lectures.filter(({ id }) => product.id === id);
        const checkCycles = promocode.cycles.filter(({ id }) => product.id === id);
        const checkTags = promocode.tags.filter(({ id }) => product.tags.includes(id));
        if (
          checkTags.length > 0
          || (checkLectures.length === 0 && checkCourse.length === 0 && checkCycles.length === 0)
          || (isProductBuy && (checkCourse.length > 0 || checkCycles.length > 0))
        ) {
          discountsRate += rate;
        } else if (checkLectures.length > 0) {

          const cerDiscountPrice = checkLectures
            .map(({ id }) => lectures[id].price)
            .reduce((prev, curr) => prev + curr);

          discountsRate += (((cerDiscountPrice * rate) / 100) * 100) / orderPrice;
        }
      }
    }

    // скидки
    if (product.discounts && product.discounts.length > 0) {
      if ((isCourse || isCycle) && !isBuyAllLectures) {
        const dd = {};
        discounts = lecturesIds
          .map((id) => {
            const discount = maxRateDiscount(product.discounts[id], lectures, tickets);
            dd[id] = discount;
            return discount;
          });

        if (Object.keys(dd).length > 0) {
          discountsRate += lecturesIds
            .map((id) => {
              if (!dd[id]) return 0;
              const { rate } = dd[id];
              const dp = (rate * lectures[id].price) / 100;
              return (100 * dp) / orderPrice;
            })
            .reduce((prev, curr) => prev + curr);
        }
      } else {
        discounts = [maxRateDiscount(product.discounts, lectures, tickets)];

        if (discounts[0]) {
          discountsRate += discounts
            .map(({ rate }) => rate || 0)
            .reduce((prev, curr) => prev + curr);
        }
      }
    }

    discountsRate = discountsRate > 100 ? 100 : discountsRate;
    discountsPrice = (discountsRate * orderPrice) / 100;

    this.orderPrice = orderPrice;
    this.discountsRate = discountsRate;
    this.discountsPrice = discountsPrice > orderPrice ? orderPrice : discountsPrice;
    this.totalPrice = orderPrice - discountsPrice;
    discounts = discounts ? discounts.filter(item => item) : null;
    this.discounts = discounts || [];

    this.renderOrderPrice();
    this.renderOrderDiscounts();
  }
  getUserProfile() {
    const { options: { userProfilePath } } = this;
    $.get(userProfilePath)
      .done((result) => {
        if (result.data && result.data.user) {
          this.user = result.data.user;
          this.setUserToFirstparticipants();
        }
      });
  }
  setUserToFirstparticipants() {
    const { user, elements: { participantsListEl } } = this;
    if (!user) return undefined;
    const {
      email,
      phone = '',
      firstName,
      lastName,
    } = user;
    const participantEl = participantsListEl.find('li').eq(0);
    if (participantEl.length > 0) {
      const inputs = participantEl.find('input');
      const values = inputs.filter((i, e) => $(e).val());
      if (values.length === 0) {
        inputs.each((i, e) => {
          const el = $(e);
          switch (el.prop('name')) {
            case 'email': {
              el.val(email);
              break;
            }
            case 'phone': {
              el.val(phone);
              break;
            }
            case 'firstName': {
              el.val(firstName);
              break;
            }
            case 'lastName': {
              el.val(lastName);
              break;
            }
            default: {

            }
          }
        });
      }
    }
  }
  promocodeSuccess(result) {
    const { promocodeQueue, calcOrder, elements: { promocodeEl, promocodeInputEl, promocodeOrderEl } } = this;
    this.promocode = result;

    promocodeEl.removeClass('error');
    promocodeEl.addClass('success');
    promocodeInputEl.prop('disabled', true).off('input change');
    promocodeOrderEl
      .removeClass('hidden')
      .addClass('visible')
      .find('span')
      .text(result.title);
    calcOrder();
  }
  promocodeError(err) {
    const { promocodeQueue, elements: { promocodeEl } } = this;
    promocodeQueue.delay *= 2;
    promocodeEl.addClass('error');
  }
  promocodeWorker(value, done) {
    const { product, options: { promocodePath }, elements: { promocodeEl, promocodeInputEl } } = this;
    const entityIds = product.lectures ? product.lectures.concat(product.id) : [product.id];
    const body = { promocode: value, entityIds, tagsIds: product.tags };
    $.post({
      url: promocodePath,
      data: { data: JSON.stringify(body) },
      dataType: 'json',
    })
      .done((result) => {
        const { data, error } = result;
        if (error) {
          return done(error);
        }
        if (value !== promocodeInputEl.val()) return done(null, null);
        return done(null, data);
      })
      .always(() => {
        promocodeEl.removeClass('in-process');
      })
      .fail((err) => {
        done(err);
      });
  }
  setLectureEvents(type, lectureId, eventId) {
    const {
      renderOrderItems,
      calcOrder,
      events,
      lectures,
    } = this;
    const lectureEvent = this.product.events[eventId];
    const { date, price } = lectureEvent;
    renderOrderItems(lectureId, date, type);

    if (type === 'add') {
      lectures[lectureId] = { price, date, eventId };
    } else {
      delete lectures[lectureId];
    }
    calcOrder();
  }
  setParticipants(type) {
    if (type === 'add') {

    } else {

    }
  }
  handlerPromocode(e) {
    const { promocodeQueue, elements: { promocodeEl } } = this;
    const el = $(e.target);
    if (promocodeEl.hasClass('error')) {
      promocodeEl.removeClass('error');
    }
    promocodeEl.addClass('in-process');
    this.promocodeQueue.push(el.val());
  }
  handlerSelectDate(e) {
    const { setLectureEvents, elements: { orderEl } } = this;
    const el = $(e.target);

    if (el.hasClass('slider-nav')) return false;

    let slideEl = null;

    if (el.hasClass('slide')) {
      slideEl = el;
    } else {
      slideEl = el.parents('.slide');
    }

    const lectureId = slideEl.data('lecture-id');
    const eventId = slideEl.data('event-id');

    if (product.buyFull) {
      if (!slideEl.hasClass('active')) {
        setLectureEvents('add', lectureId, eventId);
        slideEl.parent().find('.active').removeClass('active');
        slideEl.addClass('active');
      }
    } else if (slideEl.hasClass('active')) {
      setLectureEvents('remove', lectureId, eventId);
      slideEl.removeClass('active');
    } else {
      setLectureEvents('add', lectureId, eventId);
      slideEl.parent().find('.active').removeClass('active');
      slideEl.addClass('active');
    }

    return true;
  }
  handlerClickBuy(e) {
    e.preventDefault();
    const { valid, showError, abidePull, lectures } = this;
    const el = $(e.target);
    const href = el.data('href');

    const orderValid = valid();
    let abideValid = false;

    if (orderValid && abidePull.length > 0) {
      abideValid = abidePull.every(item => item.validateForm());
    }

    if (!orderValid || !abideValid) {
      return showError();
    }
    if (!lectures || Object.keys(lectures).length === 0) {
      return showError();
    }
    const {
      events,
      promocode,
      discounts,
      tickets,
      orderPrice,
      discountsPrice,
      discountsRate,
      totalPrice,
      elements: {
        errorEl,
        participantsListEl,
      },
    } = this;

    const participants = [];
    participantsListEl.find('li').each((i, e) => {
      const el = $(e);
      participants.push({
        firstName: el.find('input[name="firstName"]').val(),
        lastName: el.find('input[name="lastName"]').val(),
        email: el.find('input[name="email"]').val(),
        phone: el.find('input[name="phone"]').val(),
      })
    });

    spinFullElm.show();
    bodyElm.addClass('hide-scroll');

    $.post({
      url: '/order/draft',
      data: {
        order: JSON.stringify({
          product,
          participants,
          events,
          lectures,
          promocode,
          discounts,
          tickets,
          order: {
            orderPrice,
            discountsPrice,
            discountsRate,
            totalPrice,
          },
        }),
      },
      dataType: 'json',
    })
      .done(({ data, error }) => {
        if (data && data.hash) {
          window.location.href = `/order/draft/${data.hash}`;
          return;
        }

        spinFullElm.hide();
        bodyElm.removeClass('hide-scroll');

        switch (true) {
          case error: {
            errorEl.text('Ошибка. Заказ не может быть создан.');
            break;
          }
          case Boolean(data.resultOfChecking): {
            if (data.resultOfChecking.EntityNoPublic) {
              errorEl.text('Ошибка. Материал снят с публикации.');
            }
            if (data.resultOfChecking.allEventsNoTicketsAvailable) {
              errorEl.text(`Ошибка. Нет билетов:
                ${data.resultOfChecking.allEventsNoTicketsAvailable.map(({ date, title }) => `${title} ${date}`).join('\n')}
              .`);
            }
            if (data.resultOfChecking.PromocodeWithErrors) {
              errorEl.text('Ошибка. Промокод не доступен.');
              break;
            }
            if (data.resultOfChecking.DiscountsWithErrors) {
              errorEl.text('Ошибка. Скидки не доступны.');
              break;
            }
            break;
          }
          case Boolean(data.resultOfCalcOrder): {
            errorEl.text('Ошибка. Сумма заказа расчитана не верно.');
            break;
          }
          default: {
            errorEl.text('Ошибка.');
          }
        }
      })
      .fail(() => {
        spinFullElm.hide();
        bodyElm.removeClass('hide-scroll');
        errorEl.text('Ошибка.');
      });
  }
  handlerTickets(e) {
    const { renderParticipants, calcOrder } = this;
    const el = $(e.target);
    this.tickets = Order.checkTickets(el.val());
    renderParticipants();
    calcOrder();
  }
  handlerTicketsButton(e) {
    const { renderParticipants, calcOrder, elements: { ticketsElInput } } = this;
    const el = $(e.target);
    if (el.hasClass('plus') || el.parent().hasClass('plus')) {
      this.tickets += 1;
    }
    if (el.hasClass('minus') || el.parent().hasClass('minus')) {
      if (this.tickets > 0) {
        this.tickets -= 1;
      }
    }
    ticketsElInput.val(this.tickets);
    renderParticipants();
    calcOrder();
  }
  valid() {
    return true;
  }
  renderOrderPrice() {
    const { orderPrice, totalPrice, discountsPrice, elements: { totalPriceEl, orderPriceEl } } = this;
    if (orderPrice !== totalPrice) {
      orderPriceEl.text(orderPrice);
      totalPriceEl.text(totalPrice);
      orderPriceEl.addClass('line-through');
    } else {
      orderPriceEl.text('');
      totalPriceEl.text(totalPrice);
      totalPriceEl.removeClass('line-through');
    }
  }
  renderOrderDiscounts() {
    const { discounts } = this;
  }
  renderOrderItems(id, date, add) {
    const { elements: { orderEl } } = this;
    const orderLectureEl = orderEl.find(`[data-lecture-id="${id}"]`);
    if (add) {
      orderLectureEl.show();
      orderLectureEl.find('.date').show();
      orderLectureEl.find('.date-empty').hide();
      orderLectureEl.find('.l-month').text(moment(date).format('D MMMM'));
      orderLectureEl.find('.l-time').text(moment(date).format('HH:mm'));
    } else {
      orderLectureEl.find('.date-empty').show();
      orderLectureEl.find('.date').hide();
      orderLectureEl.find('.l-month').text('');
      orderLectureEl.find('.l-time').text('');
      orderLectureEl.hide();
    }
  }
  renderParticipants() {
    const { setParticipants, tickets, abidePull, elements: { participantsListEl } } = this;
    const participantsItems = participantsListEl.find('li');
    const l = participantsItems.length;

    if (tickets !== l) {
      if (tickets > l) {
        for (let i = l; i < tickets; i += 1) {
          const els = $(template_participant({ n: i + 1 }));
          participantsListEl.append(els);
          const abide = new Foundation.Abide(els);
          abidePull.push(abide);
          setParticipants('add');
        }
      } else if (tickets < l) {
        const r = l - tickets;
        for (let i = l; i > tickets; i -= 1) {
          participantsItems.eq(i - 1).remove();
          abidePull.splice(i - 1, 1);
          setParticipants('remove');
        }
      }
    }
  }

  showError() {
    const { abidePull, elements: { containerAbideErrorEl } } = this;
    if (!this.valid()) {
      containerAbideErrorEl.show();
    } else {
      containerAbideErrorEl.hide();
    }
    if (abidePull.length === 0) {
      containerAbideErrorEl.show();
    } else {
      containerAbideErrorEl.hide();
    }
  }
}

const buy = () => {
  const { product } = window;
  const order = new Order(product);
  window.getUserProfile = order.getUserProfile.bind(order);
};

export default buy;
