import Stack from './stack';

class Queue {
  constructor() {
    this.queueStack = new Stack();
    this.timeoutDelay = 500;
    this.successCallback = null;
    this.errorCallback = null;
    this.workerFn = null;

    this.lastPush = null;

    this.run = this.run.bind(this);
    this.push = this.push.bind(this);
    this.clear = this.clear.bind(this);
    this.next = this.next.bind(this);
  }

  set stack(stack) {
    this.queueStack = stack;
  }
  set success(success) {
    this.successCallback = success;
  }
  set error(error) {
    this.errorCallback = error;
  }
  set worker(fn) {
    this.workerFn = fn;
  }

  set delay(value) {
    this.timeoutDelay = value;
  }

  get delay() {
    return this.timeoutDelay;
  }

  push(value) {
    const output = this.queueStack.push(value);
    this.lastPush = new Date();
    this.run();
    return output;
  }

  clear() {
    this.queueStack.clear();
  }

  run() {
    const {
      timeoutDelay,
      next,
      queueStack,
      workerFn,
    } = this;
    const t = new Date();

    setTimeout(() => {
      if (this.lastPush > t) return next();
      const value = queueStack.pop();
      queueStack.clear();
      if (!value || !(workerFn instanceof Function)) return undefined;
      return workerFn(value, next);
    }, timeoutDelay);
  }

  next(err, data) {
    const {
      queueStack,
      errorCallback,
      successCallback,
      run,
    } = this;

    if (err) {
      if (errorCallback instanceof Function) return errorCallback(err);
      return undefined;
    }
    if (data) {
      queueStack.clear();
      if (successCallback instanceof Function) return successCallback(data);
      return undefined;
    }
    if (!queueStack || queueStack.isEmpty()) {
      return undefined;
    }

    return run();
  }
}

export default Queue;
