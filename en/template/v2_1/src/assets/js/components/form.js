const { $, Foundation } = window;

// $('.logoutUser').on('click', (e) => {
//   e.preventDefault();
// });
const bodyElm = $('body');

const formError = (elm, message) => {
  elm.text(message || 'Ошибка.');
  Foundation.Motion.animateIn(elm, 'fast fade-in');
  elm.parents('.reveal').one('click', (e) => {
    const elR = $(e.target);
    if ($(e.target).closest('button').length === 0) {
      if (elm.is(':visible')) {
        Foundation.Motion.animateOut(elm, 'slow fade-out', () => {
          elm.text('').attr('class', 'form-error');
        });
      }
    }
  });
};

const checkForm = () => {
  const form = $('form').not('.not-check');

  form.parents('.reveal-overlay').on('closed.zf.reveal', (ev) => {
    const el = $(ev.target);
    el.find('form').show();
    const formSuccessElm = el.find('.form-success');
    formSuccessElm.text('');
    const formErrorElm = el.find('.form-error');
    formErrorElm.text('').attr('class', 'form-error').hide();
  });

  form.on('formvalid.zf.abide', (ev, elm) => {
    ev.preventDefault();
    const el = $(elm);
    const formErrorElm = el.find('.form-error');
    const formSuccessElm = el.parent().find('.form-success');
    formErrorElm.text('').attr('class', 'form-error').hide();
    const spinFullElm = $('.spin-full');
    const spinLocalElm = el.find('.spin-local');

    const href = el.prop('action');

    if (!href || href === '/') {
      return false;
    }
    // const { hostname } = window.location;

    const dataSerialized = elm.serialize();

    switch (true) {
      case /\/subscribe\/common/.test(href): {
        el.find('.elm').show();
        el.find('input').prop('disabled', true).hide();
        el.find('button').prop('disabled', true).hide();
        spinLocalElm.show();
        break;
      }
      default: {
        el.find('input').prop('disabled', true);
        el.find('button').prop('disabled', true);
        spinFullElm.show();
        bodyElm.addClass('hide-scroll');
      }
    }

    $.post(href, dataSerialized)
      .done((result) => {
        const { data, error } = result;

        switch (true) {
          case /\/sign-up$/.test(href): {
            if (data && data.user) {
              $('#signUpModal').foundation('close');
              $('.userIsLoggedNav').show();
              $('.userNoLoggedNav').hide();
            } else {
              formError(formErrorElm, error.message);
            }
            break;
          }
          case /\/sign-in$/.test(href): {
            if (data && data.user) {
              if (window.location.pathname === '/account') window.location.reload();
              $('#signInModal').foundation('close');
              $('.userIsLoggedNav').show();
              $('.userNoLoggedNav').hide();
            } else {
              formError(formErrorElm, error.message);
            }
            break;
          }
          case /\/reset-pass$/.test(href): {
            if (data && data.success) {
              el.hide();
              formSuccessElm.text('На указанный e-mail отправлена инструкция для сброса пароля');
            } else {
              formError(formErrorElm, error.message);
            }
            break;
          }
          case /\/update-pass$/.test(href): {
            if (!data || !data.success) formError(formErrorElm, error.message);
            window.location.replace('/');
            break;
          }
          case /\/confirm-email$/.test(href): {
            if (data && data.success) {
              el.hide();
              formSuccessElm.text('На указанный e-mail отправлена инструкция для подтверждения e-mail');
            } else {
              formError(formErrorElm, error.message);
            }
            break;
          }
          case /\/certificate\/draft/.test(href): {
            if (data) {
              window.location.href = `/certificate/draft/${data.hash}`;
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные');
            }
            break;
          }
          case /\/subscription\/draft/.test(href): {
            if (data) {
              form.find('input').prop('disabled', true);
              form.find('button').prop('disabled', true);
              window.location.href = `/subscription/draft/${data.hash}`;
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные');
            }
            break;
          }
          case (/\/subscribe\/common/.test(href) || /\/subscribe\/schedule/.test(href)): {
            if (data) {
              form.find('input').hide();
              form.find('button').hide();
              formSuccessElm.text('Вы подписаны на рассылку.');
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные');
            }
            break;
          }
          case (/\/review$/.test(href)): {
            if (data) {
              form.find('input').hide();
              form.find('textarea').hide();
              form.find('button').hide();
              formSuccessElm.text('Ваш отзыв отправлен на модерацию.');
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные');
            }
            break;
          }
          case (/\/account$/.test(href)): {
            if (data) {
              formSuccessElm.text('Профель отредактирован.');
            } else {
              formError(formErrorElm, 'Ошибка. Проверьте введеные данные и размер изображения');
            }
            break;
          }
          default: {
            if (data && data.user) {
              $('#signInModal').foundation('close');
              $('#signUpModal').foundation('close');
              $('.userIsLoggedNav').show();
              $('.userNoLoggedNav').hide();
            } else {
              formError(formErrorElm, error.message);
            }
          }
        }
        if (window.getUserProfile && window.getUserProfile instanceof Function) window.getUserProfile();
      })
      .fail(() => {
        formError(formErrorElm, 'Ошибка сервиса, попробуйте позже');
      })
      .always(() => {
        setTimeout(() => {
          el.find('input').not('[name="promocode"]').prop('disabled', false);
          el.find('button').prop('disabled', false);
          spinFullElm.hide();
          spinLocalElm.hide();
          bodyElm.removeClass('hide-scroll');
        }, 300);
      });
  });

  form.on('forminvalid.zf.abide', (ev, elm) => {

  });

  form.submit((e) => {

    e.preventDefault();
    return false;
  });
};

export default checkForm;
