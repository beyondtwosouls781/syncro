const { $, EventEmitter, moment } = window;

// moment.locale('ru');

class Calendar extends EventEmitter {
  static datesWithEvents(dates) {
    const output = [];
    if (!dates || !Array.isArray(dates)) return output;
    dates.forEach((date) => {
      const dateStr = moment(date).startOf('day').toString();
      if (output.indexOf(dateStr) === -1) {
        output.push(dateStr);
      }
    });
    return output;
  }
  constructor(container, calendarEvents, template, startDate) {
    super();
    if (!template) {
      throw new Error('No template');
    }

    this.activeDate = null;
    this.activeMonth = {
      firstDate: null,
      lastDate: null,
    };

    this.calendarEvents = Calendar.datesWithEvents(calendarEvents);
    this.template = template;
    this.container = $(container);
    this.datesEl = this.container.find('.date-wrap');
    this.monthEl = this.container.find('.month');
    this.navEl = this.container.find('.nav-calendar');
    this.handleChangeMonth = this.handleChangeMonth.bind(this);
    this.handleSelectDate = this.handleSelectDate.bind(this);

    this.actions();
    this.setActiveDate(startDate);
    this.setDates();
    this.render();
  }

  setDates(d) {
    const date = d || this.activeDate;

    this.activeMonth = {
      firstDate: moment(date).startOf('month'),
      lastDate: moment(date).endOf('month'),
    };
  }

  setActiveDate(date) {
    this.activeDate = moment(date || new Date());
  }

  handleChangeMonth(e) {
    const el = $(e.target);
    let reRender = false;
    const currentMonth = moment(this.activeMonth.firstDate);
    const anotherMonth = moment(currentMonth);

    if (el.hasClass('next')) {
      this.setDates(anotherMonth.add(1, 'M'));
      this.emit('nextMonth', currentMonth, anotherMonth);
      reRender = true;
    } else if (el.hasClass('prev')) {
      this.setDates(anotherMonth.subtract(1, 'M'));
      this.emit('prevMonth', currentMonth, anotherMonth);
      reRender = true;
    }

    if (reRender) {
      this.render();
    }
  }

  handleSelectDate(e) {
    const el = $(e.target);
    if (el.hasClass('date-anc') && el.parent().hasClass('event')) {
      this.container.find('.active').removeClass('active');
      el.parent().addClass('active');
      const date = el.data('date');
      this.setActiveDate(date);
      this.emit('selectDate', this.activeDate);
    }
  }

  actions() {
    this.navEl.on('click', this.handleChangeMonth);
    this.datesEl.on('click', this.handleSelectDate);
  }

  render() {
    let end = false;
    const date = moment(this.activeMonth.firstDate);
    const lastDate = this.activeMonth.lastDate;
    let dd = date.day();
    const dates = [];
    dd = dd === 0 ? 7 : dd;

    while (dd > 1) {
      dates.push({
        class: 'empty',
      });
      dd -= 1;
    }

    while (!end) {
      const diff = date.diff(lastDate, 'h');
      if (diff >= 0) {
        end = true;
        break;
      }
      const activeDateDiff = date.diff(this.activeDate, 'h');
      let itemClass = '';
      const ed = date.day();
      itemClass += (ed === 6 || ed === 0) ? ' holiday' : '';

      if ((this.calendarEvents.indexOf(date.toString()) !== -1)) {
        itemClass += ' event';
        itemClass += (activeDateDiff > -24 && activeDateDiff < 0) ? ' active' : '';
      } else {
        itemClass += ' empty';
      }

      const item = {
        class: itemClass,
        date: date.toISOString(),
        text: date.format('D'),
        href: `#date-${date.format('MM-DD')}`,
      };

      dates.push(item);
      date.add('1', 'd');
    }

    const html = this.template({ dates });
    this.datesEl.empty();
    this.datesEl.append(html);
    this.monthEl.text(this.activeMonth.firstDate.format('MMMM'));
  }
}

export default Calendar;
