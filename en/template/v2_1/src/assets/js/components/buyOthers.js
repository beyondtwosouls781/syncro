const { $ } = window;

const defaultOptions = {
  promocodePath: '/promocode',
  userProfilePath: '/users/profile',
};

class OrderOther {
  constructor(options) {
    this.user = null;
    this.options = Object.assign({}, defaultOptions, options);
    this.form = $('.buy-others form');
    this.items = this.form.find('.items');
    this.buttons = $('.buy-other-button');
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.getUserProfile = this.getUserProfile.bind(this);
    this.setUserToOrder = this.setUserToOrder.bind(this);
    //
    this.form.find('.items input').on('change', this.handleInputChange);
    this.buttons.on('click', this.handleButtonClick);
    this.getUserProfile();
  }
  handleInputChange(e) {
    const { items } = this;
    const el = $(e.target);
    items.find('.active').removeClass('active');
    el.parent().addClass('active');
  }
  handleButtonClick(e) {
    const { items } = this;
    const el = $(e.target);
    const qnt = el.data('qnt');
    const input = items.find(`#lecturesCount-${qnt}`);
    input.trigger('click');
  }
  setUserToOrder() {
    const { user, form } = this;
    if (!user) return undefined;
    const {
      email,
      phone = '',
      firstName,
      lastName,
    } = user;

    form.find('input').each((i, e) => {
      const el = $(e);
      switch (el.prop('name')) {
        case 'email': {
          el.val(email);
          break;
        }
        case 'phone': {
          el.val(phone);
          break;
        }
        case 'firstName': {
          el.val(firstName);
          break;
        }
        case 'lastName': {
          el.val(lastName);
          break;
        }
        default: {}
      }
    });
  }
  getUserProfile() {
    const { options: { userProfilePath } } = this;
    $.get(userProfilePath)
      .done((result) => {
        if (result.data && result.data.user) {
          this.user = result.data.user;
          this.setUserToOrder();
        }
      });
  }
}

const buyOthers = () => {
  const orderOther = new OrderOther();
  window.getUserProfile = orderOther.getUserProfile.bind(orderOther);
};

export default buyOthers;
