const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  const { locals: { isInitial } } = req.app;
  res.json({ isInitial });
});

module.exports = router;
