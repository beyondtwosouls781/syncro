const express = require('express');
const router = express.Router();

const controllerAsync = require('./../controllers/subscribe');

router.post('/common', (req, res, next) => {
  return controllerAsync.common(req)
    .then((result) => {
      return res.json({ data: result, error: false });
    })
    .catch((err) => {
      if (err.message === 'Member Exists') return res.json({ data: { message: err.message }, error: false });
      return res.json({ data: null, error: true });
    });
});

router.post('/schedule', (req, res, next) => {
  return controllerAsync.schedule(req)
    .then((result) => {
      return res.json({ data: result, error: false });
    })
    .catch((err) => {
      if (err.message === 'Member Exists') return res.json({ data: { message: err.message }, error: false });
      return res.json({ data: null, error: true });
    });
});


module.exports = router;
