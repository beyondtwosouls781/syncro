const express = require('express');
const router = express.Router();
const moment = require('moment');

const redis = require('./../db/redis');
const { graphQlFetch } = require('./../graphql');
// const templateQuery = require('./../graphql/templateQuery');

const allTagsQuery = (req) => {
  return graphQlFetch({ req, query: 'allTagsQuery' })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);

      data.allTags.unshift({
        title: 'Все темы',
        alias: {
          alias: 'all',
        },
      });
      return data;
    });
};

const allTagsWithLectures = (req) => {
  return graphQlFetch({ req, query: 'allTagsWithLectures' })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);

      return {
        allTagsWithLectures: data.allTags,
      };
    });
};

/* GET home page. */
router.get('/', (req, res, next) => {
  Promise.all([
    allTagsWithLectures(req),
    allTagsQuery(req),
    redis.getAsync('template'),
  ])
    .then((result) => {
      const [data1, data2, template] = result;
      const currentPageData = Object.assign(
        {
          user: req.user,
          templateData: JSON.parse(template),
        },
        data1,
        data2,
      );

      res.render('pages/lecturers', { currentPageData });
    })
    .catch((error) => {
      next(error);
    });
});

module.exports = router;
