const fetch = require('node-fetch');

module.exports = (url, data) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify({ data }),
  };
  return fetch(
    url,
    options,
  );
};

module.exports.other = (url, data) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(data),
  };
  return fetch(
    url,
    options,
  );
};
