const { graphQlFetch } = require('./graphql');
const { ROOT_TOKEN } = require('./../config');

const acquiringSettingQuery = (req) => {
  return graphQlFetch({ req: { user: { token: ROOT_TOKEN } }, query: 'acquiringSettingQuery' })
    .then(({ data, errors, error }) => {
      if (errors || error) return Promise.reject(errors || error);
      return data.allSettings[0];
    });
};

module.exports = acquiringSettingQuery;
